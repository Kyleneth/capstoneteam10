﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonBolt : MonoBehaviour {
    public float life = 10;
    public float damage;
    public GameObject poison; //blood squirt particle
    private void Start()
    {
        poison = Resources.Load("PoisonNovaV2", typeof(GameObject)) as GameObject;
    }
    void Update()
    { //destroy after timer
        life -= Time.deltaTime;
        if (life < 0)
        {
            Destroy(this.gameObject);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy") //if it hits an enemy, check if its a boss or normal enemy and then make them take damage. 
        {
            //create the blood particle

            Instantiate(poison, transform.position - new Vector3(0, 0.2f, 0), Quaternion.identity);
            Destroy(gameObject);
        }
        if (other.tag == "Wall" || other.tag == "Floor") //if you hit a wall or floor then throw up the decal and delete it on a timer
        {
            Instantiate(poison, transform.position - new Vector3(0, 0.2f, 0), Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
