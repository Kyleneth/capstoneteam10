﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuBack : MonoBehaviour {

    public GameObject previousScreen;
    public GameObject currentScreen;
    public Button button;
	// Update is called once per frame
	void Update () {
        
        if (Input.GetButtonDown("SecondaryAbility1")
            || Input.GetButtonDown("SecondaryAbility2")
            || Input.GetButtonDown("SecondaryAbility3")
            || Input.GetButtonDown("SecondaryAbility4"))
        {
            if (currentScreen.activeSelf)
            {
                button.onClick.Invoke();
            }
        }
            
        
        
	}
}
