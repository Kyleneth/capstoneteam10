﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CheckInput : MonoBehaviour {
    float timer = 20;
    float maxTimer = 20;
    bool playing;
    public GameObject video;
    MovieTexture movie;
    void Start()
    {
        RawImage r = video.GetComponent<RawImage>();
        movie = (MovieTexture)r.mainTexture;
    }
    // Update is called once per frame
    void Update() {
        if (Input.GetAxisRaw("Vertical" + 1) >= 0.5 || Input.GetAxisRaw("Vertical" + 1) <= -0.5
          || Input.GetAxisRaw("Horizontal" + 1) >= 0.5 || Input.GetAxisRaw("Horizontal" + 1) <= -0.5
          || Input.GetAxisRaw("Vertical" + 2) >= 0.5 || Input.GetAxisRaw("Vertical" + 2) <= -0.5
          || Input.GetAxisRaw("Horizontal" + 2) >= 0.5 || Input.GetAxisRaw("Horizontal" + 2) <= -0.5
          || Input.GetAxisRaw("Vertical" + 3) >= 0.5 || Input.GetAxisRaw("Vertical" + 3) <= -0.5
          || Input.GetAxisRaw("Horizontal" + 3) >= 0.5 || Input.GetAxisRaw("Horizontal" + 3) <= -0.5
          || Input.GetAxisRaw("Vertical" + 4) >= 0.5 || Input.GetAxisRaw("Vertical" + 4) <= -0.5
          || Input.GetAxisRaw("Horizontal" + 4) >= 0.5 || Input.GetAxisRaw("Horizontal" + 4) <= -0.5
          || Input.GetButtonDown("BasicAbility1")
          || Input.GetButtonDown("BasicAbility2")
          || Input.GetButtonDown("BasicAbility3")
          || Input.GetButtonDown("BasicAbility4")
          || Input.GetButtonDown("SecondaryAbility1")
          || Input.GetButtonDown("SecondaryAbility2")
          || Input.GetButtonDown("SecondaryAbility3")
          || Input.GetButtonDown("SecondaryAbility4")
          || Input.GetButtonDown("ThirdAbility1")
          || Input.GetButtonDown("ThirdAbility2")
          || Input.GetButtonDown("ThirdAbility3")
          || Input.GetButtonDown("ThirdAbility4")
          || Input.GetButtonDown("Submit1")
          || Input.GetButtonDown("Submit2")
          || Input.GetButtonDown("Submit3")
          || Input.GetButtonDown("Submit4")
          || Input.GetButtonDown("Pause1")
          || Input.GetButtonDown("Pause2")
          || Input.GetButtonDown("Pause3")
          || Input.GetButtonDown("Pause4")
          || Input.GetButtonDown("Back1")
          || Input.GetButtonDown("Back2")
          || Input.GetButtonDown("Back3")
          || Input.GetButtonDown("Back4"))
        {
            timer = maxTimer;
            video.SetActive(false);
            movie.Stop();
        }
        else
        {
            timer -= Time.deltaTime;
            if (timer <=0 && !movie.isPlaying && !playing)
            {
                playing = true;
                video.SetActive(true);
                movie.Play();
            }
        }
        if (playing && !movie.isPlaying)
        {
            timer = maxTimer;
            playing = false;
            movie.Stop();
            video.SetActive(false);
        }
    }
}
