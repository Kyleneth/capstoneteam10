﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandleAccolades : MonoBehaviour {
    
    public PlayerStats pStats;
    private int numPlayers;

    public GameObject[] accolades;
    public Text[] accoladeText;
    
    // Use this for initialization
    void Start () {
        numPlayers = GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().GetNumPlayers();
        DrawStats();
    }
	
	// Update is called once per frame
	void DrawStats () {
        int worstPlayer = Random.Range(11, 11+numPlayers);
        accolades[worstPlayer].SetActive(true);

        if (numPlayers == 2)
        {
            accolades[0].SetActive(true);
            accoladeText[0].text = "Red Spirit - " + pStats.getNumPossessedEnemiesSpirit1();
            
            if (pStats.getNumKillsSpirit1() > pStats.getNumNecromancerKills())
            {
                accolades[4].SetActive(true);
                accoladeText[4].text = "Red Spirit - " + pStats.getNumKillsSpirit1();
            }
            else
            {
                accolades[3].SetActive(true);
                accoladeText[3].text = "Necromancer - " + pStats.getNumNecromancerKills();
            }

            if (pStats.getSpirit1Damage() > pStats.getNecroDamage())
            {
                accolades[7].SetActive(true);
                accoladeText[7].text = "Red Spirit - " + pStats.getSpirit1Damage();
            }
            else
            {
                accolades[10].SetActive(true);
                accoladeText[10].text = "Necromancer - " + pStats.getNecroDamage();
            }

        }

        if (numPlayers ==3)
        {
            if (pStats.getNumKillsSpirit1() > pStats.getNumNecromancerKills())
            {
                if (pStats.getNumKillsSpirit2() > pStats.getNumKillsSpirit1())
                {
                    accolades[5].SetActive(true);
                    accoladeText[5].text = "Blue Spirit - " + pStats.getNumKillsSpirit2();
                }
                else
                {
                    accolades[4].SetActive(true);
                    accoladeText[4].text = "Red Spirit - " + pStats.getNumKillsSpirit1();
                }
            }
            else
            {
                if (pStats.getNumKillsSpirit2() > pStats.getNumNecromancerKills())
                {
                    accolades[5].SetActive(true);
                    accoladeText[5].text = "Blue Spirit - " + pStats.getNumKillsSpirit2();
                }
                else
                {
                    accolades[3].SetActive(true);
                    accoladeText[3].text = "Necromancer - " + pStats.getNumNecromancerKills();
                }
            }

            if (pStats.getSpirit1Damage() > pStats.getNecroDamage())
            {
                if (pStats.getSpirit2Damage() > pStats.getSpirit1Damage())
                {
                    accolades[8].SetActive(true);
                    accoladeText[8].text = "Blue Spirit - " + pStats.getSpirit2Damage();
                }
                else
                {
                    accolades[7].SetActive(true);
                    accoladeText[7].text = "Red Spirit - " + pStats.getSpirit1Damage();
                }
                
            }
            else
            {
                if (pStats.getSpirit2Damage() > pStats.getNecroDamage())
                {
                    accolades[8].SetActive(true);
                    accoladeText[8].text = "Blue Spirit - " + pStats.getSpirit2Damage();
                }
                else
                {
                    accolades[10].SetActive(true);
                    accoladeText[10].text = "Necromancer - " + pStats.getNecroDamage();
                }
            }

            if (pStats.getNumPossessedEnemiesSpirit1() > pStats.getNumPossessedEnemiesSpirit2())
            {
                accolades[0].SetActive(true);
                accoladeText[0].text = "Red Spirit - " + pStats.getNumPossessedEnemiesSpirit1();
            }
            else
            {
                accolades[1].SetActive(true);
                accoladeText[1].text = "Blue Spirit - " + pStats.getNumPossessedEnemiesSpirit2();
            }
        }

        if (numPlayers == 4)
        {
            if (pStats.getNumKillsSpirit1() > pStats.getNumNecromancerKills())
            {
                if (pStats.getNumKillsSpirit2() > pStats.getNumKillsSpirit1())
                {
                    if (pStats.getNumKillsSpirit3() > pStats.getNumKillsSpirit2())
                    {
                        accolades[6].SetActive(true);
                        accoladeText[6].text = "Green Spirit - " + pStats.getNumKillsSpirit3();
                    }
                    else
                    {
                        accolades[5].SetActive(true);
                        accoladeText[5].text = "Blue Spirit - " + pStats.getNumKillsSpirit2();
                    }
                }
                else
                {
                    if (pStats.getNumKillsSpirit3() > pStats.getNumKillsSpirit1())
                    {
                        accolades[6].SetActive(true);
                        accoladeText[6].text = "Green Spirit - " + pStats.getNumKillsSpirit3();
                    }
                    else
                    {
                        accolades[4].SetActive(true);
                        accoladeText[4].text = "Red Spirit - " + pStats.getNumKillsSpirit1();
                    }
                    
                }
            }
            else
            {
                if (pStats.getNumKillsSpirit2() > pStats.getNumNecromancerKills())
                {
                    if (pStats.getNumKillsSpirit3() > pStats.getNumKillsSpirit2())
                    {
                        accolades[6].SetActive(true);
                        accoladeText[6].text = "Green Spirit - " + pStats.getNumKillsSpirit3();
                    }
                    else
                    {
                        accolades[5].SetActive(true);
                        accoladeText[5].text = "Blue Spirit - " + pStats.getNumKillsSpirit2();
                    }
                }
                else
                {
                    if (pStats.getNumKillsSpirit3() > pStats.getNumNecromancerKills())
                    {
                        accolades[6].SetActive(true);
                        accoladeText[6].text = "Green Spirit - " + pStats.getNumKillsSpirit3();
                    }
                    else
                    {
                        accolades[3].SetActive(true);
                        accoladeText[3].text = "Necromancer - " + pStats.getNumNecromancerKills();
                    }
                }
            }



            if (pStats.getSpirit1Damage() > pStats.getNecroDamage())
            {
                if (pStats.getSpirit2Damage() > pStats.getSpirit1Damage())
                {
                    if (pStats.getSpirit3Damage() > pStats.getSpirit2Damage())
                    {
                        accolades[9].SetActive(true);
                        accoladeText[9].text = "Green Spirit - " + pStats.getSpirit3Damage();
                    }
                    else
                    {
                        accolades[8].SetActive(true);
                        accoladeText[8].text = "Blue Spirit - " + pStats.getSpirit2Damage();
                    }
                    
                }
                else
                {
                    if (pStats.getSpirit3Damage() > pStats.getSpirit1Damage())
                    {
                        accolades[9].SetActive(true);
                        accoladeText[9].text = "Green Spirit - " + pStats.getSpirit3Damage();
                    }
                    else
                    {
                        accolades[7].SetActive(true);
                        accoladeText[7].text = "Red Spirit - " + pStats.getSpirit1Damage();
                    }
                }

            }
            else
            {
                if (pStats.getSpirit2Damage() > pStats.getNecroDamage())
                {
                    if (pStats.getSpirit3Damage() > pStats.getSpirit2Damage())
                    {
                        accolades[9].SetActive(true);
                        accoladeText[9].text = "Green Spirit - " + pStats.getSpirit3Damage();
                    }
                    else
                    {
                        accolades[8].SetActive(true);
                        accoladeText[8].text = "Blue Spirit - " + pStats.getSpirit2Damage();
                    }
                }
                else
                {
                    if (pStats.getSpirit3Damage() > pStats.getNecroDamage())
                    {
                        accolades[9].SetActive(true);
                        accoladeText[9].text = "Green Spirit - " + pStats.getSpirit3Damage();
                    }
                    else
                    {
                        accolades[10].SetActive(true);
                        accoladeText[10].text = "Necromancer - " + pStats.getNecroDamage();
                    }
                }
            }


            if (pStats.getNumPossessedEnemiesSpirit1() > pStats.getNumPossessedEnemiesSpirit2())
            {
                if (pStats.getNumPossessedEnemiesSpirit3() > pStats.getNumPossessedEnemiesSpirit1())
                {
                    accolades[2].SetActive(true);
                    accoladeText[2].text = "Green Spirit - " + pStats.getNumPossessedEnemiesSpirit3();
                }
                else
                {
                    accolades[0].SetActive(true);
                    accoladeText[0].text = "Red Spirit - " + pStats.getNumPossessedEnemiesSpirit1();
                }
            }
            else
            {
                if (pStats.getNumPossessedEnemiesSpirit3() > pStats.getNumPossessedEnemiesSpirit2())
                {
                    accolades[2].SetActive(true);
                    accoladeText[2].text = "Green Spirit - " + pStats.getNumPossessedEnemiesSpirit3();
                }
                else
                {
                    accolades[1].SetActive(true);
                    accoladeText[1].text = "Blue Spirit - " + pStats.getNumPossessedEnemiesSpirit2();
                }
            }
            
        }
    }
}
