﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoviePlayer : MonoBehaviour {
    
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Jump"))
        {
            RawImage r = GetComponent<RawImage>();
            MovieTexture movie = (MovieTexture)r.mainTexture;

            if (movie.isPlaying)
            {
                movie.Pause();
            }
            else {
                movie.Play();
            }
        }
    }
}
