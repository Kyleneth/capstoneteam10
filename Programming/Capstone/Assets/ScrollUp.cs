﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollUp : MonoBehaviour {
    float t = 0.0f;
	// Update is called once per frame
	void FixedUpdate () {
        transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, 2479, transform.position.z), t);
        t += Time.deltaTime / 10000.0f;
	}
}
