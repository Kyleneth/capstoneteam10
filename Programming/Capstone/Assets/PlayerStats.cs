﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour {
    
    public static string whatKilledMe = "";

    public static float timeAlive = 0;
    
    public static int numPages = 0;
    public static int numNecromancerKills = 0;
    public static int necroDamage = 0;
    public static int spiritPowerCollected = 0;

    public static int numPossessedEnemiesSpirit1 = 0;
    public static int numKillsSpirit1 = 0;
    public static float timeSpentPossessingSpirit1 = 0;
    public static int spirit1Damage = 0;
    public static int healingDoneSpirit1 = 0;

    public static int numPossessedEnemiesSpirit2 = 0;
    public static int numKillsSpirit2 = 0;
    public static float timeSpentPossessingSpirit2 = 0;
    public static int spirit2Damage = 0;
    public static int healingDoneSpirit2 = 0;

    public static int numPossessedEnemiesSpirit3 = 0;
    public static int numKillsSpirit3 = 0;
    public static float timeSpentPossessingSpirit3 = 0;
    public static int spirit3Damage = 0;
    public static int healingDoneSpirit3 = 0;
    
    private void Awake()
    {
        if (GameObject.Find("PlayerStats") != gameObject)
        {
            //Destroy(gameObject);
        }
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 0)
        {
            numPages = 0;
            numNecromancerKills = 0;
            necroDamage = 0;
           spiritPowerCollected = 0;

           numPossessedEnemiesSpirit1 = 0;
           numKillsSpirit1 = 0;
           timeSpentPossessingSpirit1 = 0;
           spirit1Damage = 0;
           healingDoneSpirit1 = 0;

           numPossessedEnemiesSpirit2 = 0;
           numKillsSpirit2 = 0;
           timeSpentPossessingSpirit2 = 0;
           spirit2Damage = 0;
           healingDoneSpirit2 = 0;

           numPossessedEnemiesSpirit3 = 0;
           numKillsSpirit3 = 0;
           timeSpentPossessingSpirit3 = 0;
           spirit3Damage = 0;
           healingDoneSpirit3 = 0;
}
    }
    private void Update()
    {
        if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex != 6 && UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex != 0)
            timeAlive += Time.deltaTime;
    }

    public void addPage()
    {
        ++numPages;
    }
    public void addSpiritPower(int sp)
    {
        spiritPowerCollected += sp;
    }
    public void killedBy(string enemy)
    {
        whatKilledMe = enemy;
    }
    public void addNecroKill()
    {
        ++numNecromancerKills;
    }
    public void addPossEnemySpirit(int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                ++numPossessedEnemiesSpirit1;
                break;
            case 2:
                ++numPossessedEnemiesSpirit2;
                break;
            case 3:
                ++numPossessedEnemiesSpirit3;
                break;
            default:
                break;
        }
    }
    public void addHealAmount(int playerNum, int amount)
    {
        switch (playerNum)
        {
            case 1:
                healingDoneSpirit1 += amount;
                break;
            case 2:
                healingDoneSpirit2 += amount;
                break;
            case 3:
                healingDoneSpirit3 += amount;
                break;
            default:
                break;
        }
    }
    public void addDamageAmount(int playerNum, int amount)
    {
        switch (playerNum)
        {
            case 0:
                necroDamage += amount;
                break;
            case 1:
                spirit1Damage += amount;
                break;
            case 2:
                spirit2Damage += amount;
                break;
            case 3:
                spirit3Damage += amount;
                break;
            default:
                break;
        }
    }
    public void addKillSpirit(int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                ++numKillsSpirit1;
                break;
            case 2:
                ++numKillsSpirit2;
                break;
            case 3:
                ++numKillsSpirit3;
                break;
            default:
                break;
        }
    }
    public void addTimePossessing(int playerNum, float dt)
    {
        switch (playerNum)
        {
            case 1:
                timeSpentPossessingSpirit1 += dt;
                break;
            case 2:
                timeSpentPossessingSpirit2 += dt;
                break;
            case 3:
                timeSpentPossessingSpirit3 += dt;
                break;
            default:
                break;
        }
    }



    public string getKilledBy()
    {
        return whatKilledMe;
    }

    public float getTimeAlive()
    {
        return timeAlive;
    }
    public int getNumPages()
    {
        return numPages;
    }
    public int getNumNecromancerKills()
    {
        return numNecromancerKills;
    }
    public int getNecroDamage()
    {
        return necroDamage;
    }
    public int getSpiritPowerCollected()
    {
        return spiritPowerCollected;
    }
    public int getNumPossessedEnemiesSpirit1()
    {
        return numPossessedEnemiesSpirit1;
    }
    public int getNumKillsSpirit1()
    {
        return numKillsSpirit1;
    }
    public float getTimeSpentPossessingSpirit1()
    {
        return timeSpentPossessingSpirit1;
    }
    public int getSpirit1Damage()
    {
        return spirit1Damage;
    }
    public int getHealingDoneSpirit1()
    {
        return healingDoneSpirit1;
    }

    public int getNumPossessedEnemiesSpirit2()
    {
        return numPossessedEnemiesSpirit2;
    }
    public int getNumKillsSpirit2()
    {
        return numKillsSpirit2;
    }
    public float getTimeSpentPossessingSpirit2()
    {
        return timeSpentPossessingSpirit2;
    }
    public int getSpirit2Damage()
    {
        return spirit2Damage;
    }
    public int getHealingDoneSpirit2()
    {
        return healingDoneSpirit2;
    }

    public int getNumPossessedEnemiesSpirit3()
    {
        return numPossessedEnemiesSpirit3;
    }
    public int getNumKillsSpirit3()
    {
        return numKillsSpirit3;
    }
    public float getTimeSpentPossessingSpirit3()
    {
        return timeSpentPossessingSpirit3;
    }
    public int getSpirit3Damage()
    {
        return spirit3Damage;
    }
    public int getHealingDoneSpirit3()
    {
        return healingDoneSpirit3;
    }








}
