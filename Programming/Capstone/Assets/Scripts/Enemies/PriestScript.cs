﻿//Author: Kyle Netherland

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriestScript : BaseEnemyScript {

    public GameObject bolt;
    public GameObject aura;

    void Start()
    {

        navComp = GetComponent<UnityEngine.AI.NavMeshAgent>();
        blood = Resources.Load("BloodSplat", typeof(GameObject)) as GameObject;
        poisonPart = Resources.Load("PosionSplat", typeof(GameObject)) as GameObject;
        SpiritDrop = Resources.Load("SpiritPower", typeof(GameObject)) as GameObject;
        bolt = Resources.Load("EnergyBolt", typeof(GameObject)) as GameObject;

        attackRange = 10f;
        AttackSpeed = 3f;
        if (GameObject.Find("AudioHandler"))
        {
            audioRef = GameObject.Find("AudioHandler").GetComponent<AudioReference>();
        }
       
        pStats = GameObject.Find("PlayerStats");
        if (GameObject.Find("BeepBeep"))
        {
            beepCheatCode = GameObject.Find("BeepBeep");
        }
    }
	// Use this for initialization
	public override void Attack()
    {
        players = GameObject.FindGameObjectsWithTag("Player"); //Get the enemies. (so the players)
        float minDist = 1000;
        int index = 0;
        if (players != null)
        {
            for (int i = 0; i < players.Length; i++)
            {
                Vector3 dir = players[i].transform.position - transform.position;
                if (Mathf.Abs(dir.magnitude) <= attackRange)
                {
                    canAttack = true;
                    if (Mathf.Abs(dir.magnitude) <= minDist)
                    {
                        minDist= Mathf.Abs(dir.magnitude);
                        index = i;
                    }
                }
            }
            if (canAttack)
            {
                audioRef.PlayOneshot(audioRef.priestHB);
                canAttack = false;
                GetComponent<AnimReference>().TriggerAttack();
                Vector3 dir = players[index].transform.position - transform.position;
                transform.rotation = Quaternion.LookRotation(dir);

                GameObject temp = Instantiate(bolt, transform.position + new Vector3(0.0f, 0.0f, -0.5f), Quaternion.LookRotation(dir)) as GameObject;
                temp.GetComponent<Rigidbody>().AddForce(transform.forward * 3, ForceMode.VelocityChange);
            }
        }
       
    }
    public override void AbilityTwo()
    {
       //no need to do anything.
    }
    protected override void ChildUpdate()
    {
    }

}
