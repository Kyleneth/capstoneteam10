﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class BasicSeek : MonoBehaviour {
    private float Speed;
    public float NormalSpeed = 2;
    public GameObject seekRadius;
    public bool canMove = true;
    private UnityEngine.AI.NavMeshAgent navComp;
    private GameObject player;
    private GameObject[] players;

    private Vector3 dest;
    private float easyRotateRange = 2f;
    GameObject necro;
	// Use this for initialization
	void Start () {
        Speed = NormalSpeed;
        navComp = GetComponent<UnityEngine.AI.NavMeshAgent>();
        necro = GameObject.Find("Necromancer");
    }
	
	// Update is called once per frame
	void Update () {
        if (necro)
        {
            if (!necro.GetComponent<NecroScript>().dead)
            {
                if (canMove)
                {
                    Move();
                }
                else
                {
                    navComp.ResetPath();
                    //StartMove();
                }
            }
        }
        else
        {
            necro = GameObject.Find("Necromancer");
        }
        
        
        
        
        
	}
    void Move()
    {
        navComp.speed = Speed;
        players = GameObject.FindGameObjectsWithTag("Player");
        float dist = Vector3.Distance(players[0].transform.position, transform.position);
        GameObject nearestPlayer = players[0];

        for (int i = 0; i < players.Length; i++)
        {
            if (Vector3.Distance(players[i].transform.position, transform.position) < dist)
            {
                dist = Vector3.Distance(players[i].transform.position, transform.position);
                nearestPlayer = players[i];
            }
        }
        if (Vector3.Distance(nearestPlayer.transform.position, transform.position) < easyRotateRange)
        {
            navComp.updateRotation = false;
            Vector3 relPos = nearestPlayer.transform.position - transform.position;
            relPos.y = 0;
            Quaternion rotation = Quaternion.LookRotation(relPos);
            transform.rotation = rotation;
        }
        else
        {
            navComp.updateRotation = true;
        }

        if (Vector3.Distance(nearestPlayer.transform.position,transform.position) <= navComp.stoppingDistance + 0.5f)
        {
            GetComponent<AnimReference>().SetRun(false);
            StopMove();
        }
        else
        {
            navComp.SetDestination(nearestPlayer.transform.position);
            GetComponent<AnimReference>().SetRun(true);
            StartMove();
        }
        
    }
    public void StopMove()
    {
        canMove = false;
    }
    public void StartMove()
    {
        canMove = true;
    }
    public void SlowToggle(float speed)
    {
        Speed = speed;
        //navComp.speed = Speed;
    }
}
