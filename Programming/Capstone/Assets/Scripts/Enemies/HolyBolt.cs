﻿using System.Collections;
using System.Collections.Generic;
//Author: Kyle Netherland

using UnityEngine;

public class HolyBolt : MonoBehaviour
{
    float i = 5; //timer
    public float damage =2;
    public float passes = 2;

    public string enemy = "Player";
    public string ally = "Enemy";
    public int spiritID = 0;
    private GameObject pStats;
    void Start()
    {
        pStats = GameObject.Find("PlayerStats");
    }
    // Update is called once per frame
    void Update ()
    {
        i -= Time.deltaTime;

        if (i < 0 || passes <= 0)
            Destroy(gameObject);
        
    }
    void OnTriggerEnter(Collider other) //if it hits an enemy, slow them and deal damage to them.
    {
        if (other.tag == ally)
        {
            if (other.gameObject.GetComponent<NecroScript>())
            {
                if (pStats)
                {
                    pStats.GetComponent<PlayerStats>().addHealAmount(spiritID, (int)(damage * 0.5f));
                }
                
                other.gameObject.GetComponent<NecroScript>().TakeDamage(-damage * 0.5f,1);
            }
            else if (other.gameObject.GetComponent<PossessedControl>())
            {
                pStats.GetComponent<PlayerStats>().addHealAmount(spiritID, (int)(damage * 0.5f));
                other.gameObject.GetComponent<PossessedControl>().TakeDamage(-damage * 0.5f);
            }
            else if (other.gameObject.GetComponent<PriestControl>())
            {
                if (pStats)
                {
                    pStats.GetComponent<PlayerStats>().addHealAmount(spiritID, (int)(damage * 0.5f));
                }
                
                other.gameObject.GetComponent<PriestControl>().TakeDamage(-damage * 0.5f);
            }
            else if (other.gameObject.GetComponent<CleaveControl>())
            {
                pStats.GetComponent<PlayerStats>().addHealAmount(spiritID, (int)(damage * 0.5f));
                other.gameObject.GetComponent<CleaveControl>().TakeDamage(-damage * 0.5f);
            }
            else if (other.gameObject.GetComponent<BaseEnemyScript>() && !other.gameObject.GetComponent<PlayerScript>())
            {
                other.gameObject.GetComponent<BaseEnemyScript>().TakeDamage(-damage * 0.5f, spiritID);
            }
            --passes;
        }
        if (other.tag == enemy)
        {
            if (other.gameObject.GetComponent<NecroScript>())
                other.gameObject.GetComponent<NecroScript>().TakeDamage(damage,1);
            else if (other.gameObject.GetComponent<PossessedControl>())
                other.gameObject.GetComponent<PossessedControl>().TakeDamage(damage);
            else if (other.gameObject.GetComponent<PriestControl>())
                other.gameObject.GetComponent<PriestControl>().TakeDamage(damage);
            else if (other.gameObject.GetComponent<CleaveControl>())
                other.gameObject.GetComponent<CleaveControl>().TakeDamage(damage);
            else if(other.gameObject.GetComponent<BaseEnemyScript>())
            {
                other.gameObject.GetComponent<BaseEnemyScript>().TakeDamage(damage, spiritID);
            }
               
            --passes;

        }
        if (other.tag == "Wall")
        {
            Destroy(gameObject);
        }
    }
}
