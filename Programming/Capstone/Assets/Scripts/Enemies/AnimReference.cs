﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

//a collection of animations and triggers.
public class AnimReference : MonoBehaviour{

    public Animator anim;

    public void SetRun(bool val)
    {
        anim.SetBool("IsRunning",val);
    }
    public void SetWalk(bool val)
    {
        anim.SetBool("IsWalking", val);
    }
    public void TriggerDeath()
    {
        anim.SetTrigger("Died");
    }
    public void TriggerChannel()
    {
        anim.SetTrigger("StartedChannel");
    }
    public void TriggerAttack()
    {
        anim.SetTrigger("IsAttacking");
    }
    public void SetChannel(bool val)
    {
        anim.SetBool("IsChanneling", val);
    }
    public void TriggerUltimate()
    {
        anim.SetTrigger("Ultimate");
    }
    public void SetAttacking(bool val)
    {
        anim.SetBool("Attacking", val);
    }
}
