﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class EnemyAttackRange : MonoBehaviour {

    public bool canAttack;
    public string enemy = "Player";
    public float range = 3;

    //while there are enemies nearby, they can attack
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == enemy)
        {
            canAttack = true;
        }
    }
    //when the enemies leave, they cannot attack.
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == enemy)
        {
            canAttack = false;
        }
    }
}
