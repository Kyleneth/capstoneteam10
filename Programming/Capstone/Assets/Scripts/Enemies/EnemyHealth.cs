﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyHealth : MonoBehaviour {
    [Header("Health")]
    public float MaxHealth = 10;
    public float Health = 10;
    public float DamageTakenModifier = 1;

    public Slider healthSlider;   // Reference to the enemies health bar.
    
    public void ChangeHealth(float diff)
    {
        // Set the health bar's value to the current health.
        Health -= diff * DamageTakenModifier;
        healthSlider.value = Health;
    }


}
