﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class EventHandler : MonoBehaviour
{
    private Dictionary<string, UnityEvent> events;
    private static EventHandler eventHandler;

    public static EventHandler Instance
    {
        get {
            if (!eventHandler)
            {
                eventHandler = FindObjectOfType(typeof(EventHandler)) as EventHandler;
                if (!eventHandler)
                {
                    
                }
                else
                {
                    eventHandler.Init();
                }
            }
            return eventHandler;
        }
    }

    void Init()
    {
        if (events == null)
        {
            events = new Dictionary<string, UnityEvent>();
        }
    }

    public static void StartListening(string eventName, UnityAction listener)
    {
        //UnityEvent ev = null;
       /* if (Instance.events.TryGetValue(eventName, out ev))
        {
            ev.AddListener(listener);
        }
        else
        {
            ev = new UnityEvent();
            ev.AddListener(listener);
            Instance.events.Add(eventName, ev);
        }*/
    }

    public static void StopListening(string eventName, UnityAction listener)
    {
        if (eventHandler == null) return;
        UnityEvent ev = null;
        if (Instance.events.TryGetValue(eventName, out ev))
        {
            ev.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName)
    {
        UnityEvent ev = null;
        if (Instance.events.TryGetValue(eventName, out ev))
        {
            ev.Invoke();
        }
    }
}