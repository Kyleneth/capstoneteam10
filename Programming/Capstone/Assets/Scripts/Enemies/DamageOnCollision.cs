﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnCollision : MonoBehaviour {
    public float damage = 2.0f;
    public bool ColliderOn = false;
    public bool templar;
    public bool Possessing = false;
    public int spiritID = 0;
	void OnTriggerEnter(Collider other)
    {
        if (ColliderOn)
        {
            if (Possessing)
            {
                if (other.tag == "Enemy")
                {
                    if (other.GetComponent<BaseEnemyScript>())
                    {
                        other.GetComponent<BaseEnemyScript>().TakeDamage(damage, spiritID);
                    }
					if (other.GetComponent<PriestScript>())
					{
						other.GetComponent<PriestScript>().TakeDamage(damage, spiritID);
					}
					if (other.GetComponent<CleaveEnemy>())
					{
						other.GetComponent<CleaveEnemy>().TakeDamage(damage, spiritID);
					}
                }
            }
            else
            {
                if (other.tag == "Player")
                {
                    if (other.GetComponent<NecroScript>())
                    {
                        if (templar)
                        {
                            other.GetComponent<NecroScript>().TakeDamage(damage,0);
                        }
                        else
                        {
                            other.GetComponent<NecroScript>().TakeDamage(damage, 2);
                        }
                        
                    }
                    else if (other.GetComponent<PossessedControl>())
                    {
                        other.GetComponent<PossessedControl>().TakeDamage(damage);
                    }
                    else if (other.GetComponent<PriestControl>())
                    {
                        other.GetComponent<PriestControl>().TakeDamage(damage);
                    }
                    else if (other.GetComponent<CleaveControl>())
                    {
                        other.GetComponent<CleaveControl>().TakeDamage(damage);
                    }
                }
            }
        }
    }
}
