﻿//Author: Kyle Netherland

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleaveEnemy : BaseEnemyScript {
    
    private bool canCharge = false;
    private Vector3 dir;

    public bool spinToWin;
    public bool charging;
    private float NormalSpeed;
    void Start()
    {
        navComp = GetComponent<UnityEngine.AI.NavMeshAgent>();
        blood = Resources.Load("BloodSplat", typeof(GameObject)) as GameObject;
        poisonPart = Resources.Load("PosionSplat", typeof(GameObject)) as GameObject;
        SpiritDrop = Resources.Load("SpiritPower", typeof(GameObject)) as GameObject;
        attackRange = 1.5f;
        attackTimer = 2.0f;
        AttackSpeed = 3.0f;
        a2CoolDown = 1.0f;
        a2MaxCD = 2.0f;
        Damage = 1.0f;
        sword.GetComponent<DamageOnCollision>().damage = Damage;
        sword.GetComponent<DamageOnCollision>().templar = false;
        NormalSpeed = GetComponent<EnemySpeed>().NormalSpeed;
        if (GameObject.Find("AudioHandler"))
        {
            audioRef = GameObject.Find("AudioHandler").GetComponent<AudioReference>();
        }
        pStats = GameObject.Find("PlayerStats");

        if (GameObject.Find("BeepBeep"))
        {
            beepCheatCode = GameObject.Find("BeepBeep");
        }
    }

    public override void Attack()
    {
        canAttack = false;

        players = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < players.Length; i++)
        {
            if ((players[i].transform.position - transform.position).magnitude <= attackRange)
            {
                canAttack = true;
            }
        }
        if (!canAttack)
        {
            if (GetComponent<BasicSeek>())
            {
                GetComponent<BasicSeek>().StartMove();
            }
        }
        if (!GetComponent<BasicWanderSteering>())
        {
            if (GetComponent<BasicSeek>() && !spinToWin)
            {
                charging = true;
                Invoke("Charge",0.2f);
            }
            
        }
        
    }

    public override void AbilityTwo()
    {
        if (!charging)
        {
            if (!GetComponent<BasicWanderSteering>())
            {
                dir = player.transform.position - transform.position;
                if (GetComponent<BasicSeek>() && Mathf.Abs(dir.magnitude) <= attackRange)
                {
                    spinToWin = true;
                    sword.GetComponent<DamageOnCollision>().ColliderOn = true;
                    Invoke("TurnOffSwordCollider", 0.367f * 3.0f);
                    Invoke("turnOffSpin", 0.367f * 3.0f);
                    GetComponent<AnimReference>().SetAttacking(true);
                }
            }
        }
    }
    void turnOffSpin()
    {
        GetComponent<AnimReference>().SetAttacking(false);
        if (GetComponent<BasicSeek>())
            GetComponent<BasicSeek>().StartMove();
        spinToWin = false;
    }
    protected override void ChildUpdate()
    {

        if (canCharge && !spinToWin)
        {
            GetComponent<EnemySpeed>().Speed = NormalSpeed * 1.5f;
            Invoke("FinishCharge",1.0f);
            
        }
    }
   
    private void Charge()
    {
        canCharge = true;
    }
    private void FinishCharge()
    {
        GetComponent<EnemySpeed>().Speed = NormalSpeed * 1.5f;
        charging = false;
        canCharge = false;
    }
}
