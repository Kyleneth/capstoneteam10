﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class BaseEnemyScript : MonoBehaviour {
    
    [Header("Damage")] //Damage is the current damage being done and NormalDamage is the damage it does without any effects on it.
    public float Damage = 1f;
    public float NormalDamage = 1f;
    public GameObject sword;
    [Header("AttackSpeed")] //Attack speed is the current attack speed and Max is maximum Attack speed.
    public float AttackSpeed = 1.5f;
    [Header("Attack Two")]//Attack Two is the stats for the Enemy's second ability.
    public float a2CoolDown = 5.0f;
    public float amountOfDodge = 0.0f;
    public float buffDur = 0.0f;
    public float a2MaxCD = 10.0f;
    
	public Color ogCol = Color.white;
    private Color hurtCol = Color.red;
    private float colTime = 0.0f;
    private bool hurt = false;
    private bool unhurt = false;

    private Color yumCol = Color.green;
    private bool healed = false;
    private bool unhealed = false;
    //The following are the effects of the spirits.
    protected bool isPoisoned; //Are the enemies Poisoned?
    protected float poisonDuration = 0, poisonDamage = 0; //poison damage and poison duration is set when effected.
    protected float attackTimer =0; //Attack speed.
    protected bool dead;//are they dead?

    [Header("ParticleEffects")] //These are the effects that appear on the enemy as they are effected.
    public GameObject poisonPart;//poison particle effect that plays while poisoned
    public GameObject blood; //blood particle effect that plays when you take a hit
    public GameObject Shield1;
    public  GameObject Shield2;
    //Protected variables because every enemy will have a navcomp and list of players.
    protected UnityEngine.AI.NavMeshAgent navComp; //for navigation
    protected GameObject[] players; //for navigation

    [Header("ItemDrops")]
    public GameObject SpiritDrop;
    public int SpiritAmount = 2;

    [Header("Range")]
    public float attackRange = 2.0f; //self explanatory
    protected bool canAttack = true;
    protected bool canA2 = false;
    public float DistFromNecro = 25;
	public GameObject RightBumper;
    public GameObject BloodExplosion; 
    protected AudioReference audioRef;
    protected GameObject player;
    protected GameObject pStats;

    protected GameObject beepCheatCode;
    protected bool isBeep = false;
    protected GameObject beep;
    // Use this for initialization
    void Start () {
        sword.GetComponent<DamageOnCollision>().templar = true;
        navComp = GetComponent<UnityEngine.AI.NavMeshAgent>();
        blood = Resources.Load("BloodSplat", typeof(GameObject)) as GameObject;
        BloodExplosion = Resources.Load("BloodExplosion", typeof(GameObject)) as GameObject;
        poisonPart = Resources.Load("PosionSplat", typeof(GameObject)) as GameObject;
        SpiritDrop = Resources.Load("SpiritPower", typeof(GameObject)) as GameObject;
        Shield2.SetActive(false);
        Shield1.SetActive(false);
        if (GameObject.Find("AudioHandler"))
        {
            audioRef = GameObject.Find("AudioHandler").GetComponent<AudioReference>();
        }
        pStats = GameObject.Find("PlayerStats");

        if (GameObject.Find("BeepBeep"))
        {
            beepCheatCode = GameObject.Find("BeepBeep");
        }
    }

    // Update is called once per frame
    void Update () {
		if (GameObject.Find ("BeepBeep")) {
			if (beepCheatCode.GetComponent<CheatCodeForBeep> ().beepCode && !beep) {
				SkinnedMeshRenderer[] meshes = GetComponentsInChildren<SkinnedMeshRenderer> ();
				for (int i = 0; i < meshes.Length; i++) {
					if (meshes [i].gameObject.name != "cleaverBase2:b_weapon"
					              && meshes [i].gameObject.name != "cleaverBase2:m_weapon"
					              && meshes [i].gameObject.name != "priestess_base:m_staff") {
						meshes [i].enabled = false;
					}
                   
				}
				isBeep = true;
				beep = Instantiate (Resources.Load ("BEEPBEEP", typeof(GameObject)) as GameObject, transform.position, transform.rotation) as GameObject;
				beep.transform.SetParent (gameObject.transform);
			}
		}

        if (player)
        {
            if (!player.GetComponent<NecroScript>().dead)
            {
                if (!dead)//if the enemy isn't dead
                {
                    if (transform.position.y <= -5)
                    {
                        //Not sure if all this needs to be here. but it's deleting the components from the enemy since the enemy can't move anymore and stuff.
                        if (GetComponent<BasicSeek>())
                        {
                            Destroy(GetComponent<BasicSeek>());
                        }
                        else if (GetComponent<BasicWanderSteering>())
                        {
                            Destroy(GetComponent<BasicWanderSteering>());
                        }
                        if (navComp)
                        {
                            Destroy(navComp);
                        }
                        Destroy(GetComponent<Rigidbody>());
                        Destroy(GetComponent<CapsuleCollider>());
                        
                        Invoke("Kill", 2f); //this just gives it enough time for the animation to play
                    }
                    if ((gameObject.transform.position - player.transform.position).magnitude >= DistFromNecro)
                    {
                        Invoke("Kill", 2f); //this just gives it enough time for the animation to play
                    }

                    //if the enemy is poisoned 
                    if (isPoisoned)
                    {
                        TakeDamage(poisonDamage * Time.deltaTime, 0); //deal damage to the enemy based on time.
                        poisonDuration -= Time.deltaTime; //update timer
                        if (poisonDuration <= 0) //check if timer is done
                        {
                            isPoisoned = false; //set poison off and duration to 1 second 
                            poisonDuration = 1;
                        }
                    }

                    attackTimer -= Time.deltaTime; //then start counting down
                    a2CoolDown -= Time.deltaTime;

                    if (attackTimer <= 0) //when its time to attack...
                    {

                        Attack(); //attack here
                        attackTimer = AttackSpeed; //then update the timer again.
                    }
                    if (a2CoolDown <= 0)
                    {
                        //canA2 = false;
                        AbilityTwo();
                        a2CoolDown = a2MaxCD;
                    }
                    if (hurt)
                    {
                        foreach (Renderer r in GetComponentsInChildren<Renderer>())
                        {
                            r.material.color = Color.Lerp(ogCol, hurtCol, colTime);
                        }
                        colTime += Time.deltaTime * 3.0f;
                        if (colTime >= 1.0f)
                        {
                            colTime = 0.0f;
                            unhurt = true;
                            hurt = false;
                        }
                    }
                    if (unhurt)
                    {
                        foreach (Renderer r in GetComponentsInChildren<Renderer>())
                        {
                            r.material.color = Color.Lerp(hurtCol, ogCol, colTime);
                        }
                        colTime += Time.deltaTime * 3.0f;
                        if (colTime >= 1.0f)
                        {
                            colTime = 0.0f;
                            unhurt = false;
                        }
                    }
                    if (healed)
                    {
                        foreach (Renderer r in GetComponentsInChildren<Renderer>())
                        {
                            r.material.color = Color.Lerp(ogCol, yumCol, colTime);
                        }
                        colTime += Time.deltaTime * 3.0f;
                        if (colTime >= 1.0f)
                        {
                            colTime = 0.0f;
                            unhealed = true;
                            healed = false;
                        }
                    }
                    if (unhealed)
                    {
                        foreach (Renderer r in GetComponentsInChildren<Renderer>())
                        {
                            r.material.color = Color.Lerp(yumCol, ogCol, colTime);
                        }
                        colTime += Time.deltaTime * 3.0f;
                        if (colTime >= 1.0f)
                        {
                            colTime = 0.0f;
                            unhealed = false;
                        }
                    }
                    ChildUpdate();
                }
            }
        }
        else
        {
            player = GameObject.Find("Necromancer");
        }
        
	}
    protected virtual void ChildUpdate()
    {

        if (buffDur >= 0)
        {
            buffDur -= Time.deltaTime;
        }
        if (buffDur <= 0)
        {
            Shield2.SetActive(false);
            Shield1.SetActive(false);
            
            amountOfDodge = 0;
        }
        
    }
    //function for taking damage. It's calculated here and updated in the EnemyHealth
    public void TakeDamage(float dmg, int spiritID)
    {
        if (dmg >= 0) {

            hurt = true;

			if (amountOfDodge <= 0 && !isPoisoned)
            {
                if (GetComponent<PriestScript>())
                    audioRef.PlayOneshot(audioRef.priestDamage);
                else if(GetComponent<CleaveEnemy>())
                    audioRef.PlayOneshot(audioRef.necroGrunt1);
                else
                    audioRef.PlayOneshot(audioRef.necroGrunt2);

                if (pStats)
                    pStats.GetComponent<PlayerStats>().addDamageAmount(spiritID,(int)dmg);
                GetComponent<EnemyHealth>().ChangeHealth (dmg);
			}
            else if (amountOfDodge <= 0 && isPoisoned)
            {
                GetComponent<EnemyHealth>().ChangeHealth(dmg);
            }
			if (amountOfDodge >= 1)
            {
                audioRef.PlayOneshot(audioRef.templarShieldDeplete);
                Shield1.SetActive(false);
                amountOfDodge--;
			}
			if (amountOfDodge == 0) 
			{
                if(Shield2 !=null)
                    Shield2.SetActive(false);
            }


		}
        if(dmg>=300)
        {
            if (BloodExplosion)
            {
                GameObject temp = Instantiate(BloodExplosion, transform.position, transform.rotation) as GameObject;
                Destroy(temp, 1.3f);
            }
        }

		if (dmg <= 0)
		{
            if ((GetComponent<EnemyHealth>().Health - dmg) <= GetComponent<EnemyHealth>().MaxHealth)
            {
                healed = true;
                GetComponent<EnemyHealth>().ChangeHealth(dmg);
            }
            else
            {
                GetComponent<EnemyHealth>().Health = GetComponent<EnemyHealth>().MaxHealth;
            }
			
		}
        if (isPoisoned) //if poisoned, also release a poison particle.
		{
			GameObject temp2 = Instantiate (poisonPart, transform.position, transform.rotation) as GameObject;
			Destroy (temp2, .1f);
		}
        

        if (GetComponent<EnemyHealth>().Health <= 0 && !dead) //if enemy isn't dead and they should be then 
        {
            if (spiritID == 0)
            {
				if(pStats)
                    pStats.GetComponent<PlayerStats>().addNecroKill();
            }
            else
            {
				if(pStats)
                    pStats.GetComponent<PlayerStats>().addKillSpirit(spiritID);
            }
            GameObject temp = Instantiate(blood, transform.position, transform.rotation) as GameObject; //make them bleed.
            Destroy(temp, .4f); //this destroys the particle after a time so that it doesn't stay.
            
            //Not sure if all this needs to be here. but it's deleting the components from the enemy since the enemy can't move anymore and stuff.
            if (GetComponent<BasicSeek>())
            {
                Destroy(GetComponent<BasicSeek>());
            }
            else if (GetComponent<BasicWanderSteering>())
            {
                Destroy(GetComponent<BasicWanderSteering>());
            }
            if (navComp)
            {
                //navComp.ResetPath();
                Destroy(navComp);
            }
            
            
            Destroy(GetComponent<Rigidbody>());
            Destroy(GetComponent<CapsuleCollider>());

            //this plays the death animation
            GetComponent<AnimReference>().TriggerDeath();

            dead = true; //it seems obvious, but this stops updates from happening
            Invoke("Kill",2f); //this just gives it enough time for the animation to play
        }
    }

    void Kill()
	{
		if(GameObject.Find("EnemySpawning"))
        	GameObject.Find("EnemySpawning").GetComponent<NewEnemySpwnScript>().currEnemies--;
        if (dead)
        {
            GameObject temp = Instantiate(SpiritDrop, transform.position, transform.rotation) as GameObject;
            temp.GetComponent<SpiritPowerPickUp>().value = SpiritAmount;
        }
        Destroy(gameObject); //bye bye
    }

    public virtual void Attack()
    {
        canAttack = false;
        players = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < players.Length; i++)
        {
            if ((players[i].transform.position - transform.position).magnitude <= attackRange)
            {
                canAttack = true;
            }
        }
        if (!canAttack)
        {
            if (GetComponent<BasicSeek>())
            {
                GetComponent<BasicSeek>().StartMove();
            }
        }
        
        if (canAttack)
        {
            audioRef.PlayOneshot(audioRef.templarBasic);
            //trigger attack anim
            sword.GetComponent<DamageOnCollision>().ColliderOn = true;
            Invoke("TurnOffSwordCollider",1.0f);

            GetComponent<AnimReference>().TriggerAttack();
        }
        
    }

    protected void TurnOffSwordCollider()
    {
        sword.GetComponent<DamageOnCollision>().ColliderOn = false;
    }

    public virtual void AbilityTwo()
    {
		buffDur = 4;
		amountOfDodge = 2;
        if (audioRef)
        {
            audioRef.PlayOneshot(audioRef.templarShields);
        }

        Shield1.SetActive(true);
        Shield2.SetActive(true);			
    }
    public void Poison(float dmg, float duration)//called somewhere else. sets the damage, duration and sets poison to true.
    {
        isPoisoned = true;
        poisonDamage = dmg;
        poisonDuration = duration;
    }
	public void TurnOnRB()
	{
		RightBumper.SetActive (true);
	}
	public void TurnOffRB()
	{
		RightBumper.SetActive (false);
	}
    
}
