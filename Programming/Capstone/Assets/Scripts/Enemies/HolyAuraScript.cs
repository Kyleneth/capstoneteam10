﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolyAuraScript : MonoBehaviour {

    public bool possessing = false;
    public float healAmount = 1.0f;
    private float timer = 2.0f;
    private bool canHeal = false;
    public int spiritID = 0;
    private GameObject pStats;
    void Start()
    {
        pStats = GameObject.Find("PlayerStats");
    }
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            canHeal = true;
        }
    }
	void OnTriggerStay(Collider other)
    {
        if (canHeal)
        {
            if (possessing)
            {
                if (other.tag == "Player")
                {
                    pStats.GetComponent<PlayerStats>().addHealAmount(spiritID, (int)healAmount);
                    if (other.GetComponent<NecroScript>())
                    {
                        other.GetComponent<NecroScript>().TakeDamage(-healAmount,1);
                    }
                    else if (other.GetComponent<PossessedControl>())
                    {
                        other.GetComponent<PossessedControl>().TakeDamage(-healAmount);
                    }
                    else if (other.GetComponent<PriestControl>())
                    {
                        other.GetComponent<PriestControl>().TakeDamage(-healAmount);
                    }
                    else if (other.GetComponent<CleaveControl>())
                    {
                        other.GetComponent<CleaveControl>().TakeDamage(-healAmount);
                    }
                    canHeal = false;
                    timer = 2.0f;
                }
            }
            else
            {
                if (other.tag == "Enemy")
                {

                    if (other.GetComponent<BaseEnemyScript>())
                    {
                        other.GetComponent<BaseEnemyScript>().TakeDamage(-healAmount,spiritID);
                    }
                    canHeal = false;
                    timer = 2.0f;
                }
            }
        }
    }
}
