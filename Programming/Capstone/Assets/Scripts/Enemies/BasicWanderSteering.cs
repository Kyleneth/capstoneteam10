﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class BasicWanderSteering : MonoBehaviour {
    private float Speed;
    public float NormalSpeed = 1;
    private bool canMove = true;
    private UnityEngine.AI.NavMeshAgent navComp;

    //for wandering
    private Vector3 steeringDirection = new Vector3();
    private float circleOffset = 5;
    private float circleRadius = 2;
    private float maxJitter = 0.5f;
    //timer for switching between wanders
    private float newWanderTimer = 0;
    private float switchWander = 7;
    public GameObject shouldSeek; //this is attached to the enemy and is a range object for them.
    public GameObject necro;
    void Start () {
        //sets all the needed variables for navigating
        Speed = NormalSpeed;
        navComp = GetComponent<UnityEngine.AI.NavMeshAgent>();
        
        navComp.speed = Speed;
        necro = GameObject.Find("Necromancer");
    }
	
	void Update () {
        if (necro)
        {
            if (!GameObject.Find("Necromancer").GetComponent<NecroScript>().dead)
            {
                if (canMove)//if it can move
                {
                    if (Vector3.Distance(steeringDirection, transform.position) < navComp.stoppingDistance)//check if its within its stop range, if it is then reset its path.
                    {
                        navComp.ResetPath();
                    }
                    //update timers.. newWanderTimer is the time used to find the new path, and switch wander is used to switch between wander methods.
                    newWanderTimer -= Time.deltaTime;
                    switchWander -= Time.deltaTime;

                    if (Vector3.Distance(steeringDirection, transform.position) <= navComp.stoppingDistance + 0.5f)
                    {
                        GetComponent<AnimReference>().SetWalk(false); //if its close stop animating
                    }
                    else
                    {
                        GetComponent<AnimReference>().SetWalk(true); //else keep animating
                    }
                    //if you dont have a path and you time is up, start finding another one
                    if (!navComp.hasPath && newWanderTimer <= 0)
                    {
                        //if its time to switch between wanders calculate different wander path and set timer
                        if (switchWander <= 0)
                        {
                            switchWander = 10;
                            WanderState();
                        }
                        else
                        {
                            StationaryWander();//else go ahead and use our normal wander
                        }
                        //randomize timer for resetting path
                        newWanderTimer = Random.Range(4, 6);
                        //if the path isnt valid, reset and reset timers.
                        if (!navComp.SetDestination(steeringDirection))
                        {
                            navComp.ResetPath();
                            newWanderTimer = 0;
                        }

                    }
                    //if it has a path, look at the next position of that path. This is to keep the enemies rotation looking smart. Instead of looking at the final point. 
                    if (navComp.hasPath)
                    {
                        transform.LookAt(new Vector3(navComp.nextPosition.x, transform.position.y, navComp.nextPosition.z));
                        GetComponent<AnimReference>().SetWalk(true);//set animation
                    }
                    else
                    {
                        GetComponent<AnimReference>().SetWalk(false);//turn off animation if it doesn't have a path.
                    }

                    if (shouldSeek.GetComponent<SeekRadius>().seek) //if they are in range to seek, then swap their steerings.
                    {
                        if (gameObject.name != "PossessEnemy") //this is a special enemy that should not have steering at all.
                        {
                            GetComponent<AnimReference>().SetWalk(false);
                            gameObject.AddComponent<BasicSeek>();
                            Destroy(gameObject.GetComponent<BasicWanderSteering>());
                        }
                    }
                }
            }
        }
        else
        {
            necro = GameObject.Find("Necromancer");
        }
    }

    void WanderState()
    {
        //creates new positions to travel to. extended by a radius, then sets the steering direction to that.
            steeringDirection = new Vector3();
            Vector3 targetPosition = steeringDirection - transform.forward * circleOffset;
            targetPosition = new Vector3(targetPosition.x + Random.Range(-maxJitter, maxJitter), transform.position.y, targetPosition.z + Random.Range(-maxJitter, maxJitter));
            targetPosition = Vector3.Normalize(targetPosition);
            targetPosition *= circleRadius;
            steeringDirection = targetPosition + transform.forward * circleOffset;
        
       
       
        //have character find a random place to go to.
    }
    void StationaryWander()
    {
        //walk a unit forward in  random direction.
            float heading = Random.Range(0, 360);
            transform.eulerAngles = new Vector3(0, heading, 0);
            steeringDirection = transform.position + transform.forward;
        
    }
   //these are for effects such as root and slow
    public void StopMove()
    {
        canMove = false;
    }
    public void StartMove()
    {
        canMove = true;
    }
    public void SlowToggle(float speed)
    {
        Speed = speed;
        navComp.speed = Speed;
    }
}
