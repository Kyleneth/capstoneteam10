﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class EnemySpeed : MonoBehaviour {
    [Header("Speed")]
    public float Speed;
    public float NormalSpeed;
    public float stopRange;
    [Header("Particles")]
    public GameObject BoneLock; //This is for the rooting particles.


    private bool isSlowed; //Are the enemies suffering from these effects?
    private float slowDuration = 0;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        //if slowed
        if (isSlowed)
        {
            slowDuration -= Time.deltaTime;//update slow timer
            if (slowDuration <= 0)//if its over
            {
                //set slow to false, set speed to normal, set duration to 1 for standin
                isSlowed = false;
                Speed = NormalSpeed;
                slowDuration = 1;

                //checks to see which steering it has then unslows that
                if (GetComponent<BasicSeek>())
                {
                    GetComponent<BasicSeek>().SlowToggle(Speed);
                }
                else if (GetComponent<BasicWanderSteering>())
                {
                    GetComponent<BasicWanderSteering>().SlowToggle(Speed);
                }
            }
        }
    }

    public void Slow(float slow, float duration) //when called, set slow status, set speed to slowed down speed, set duration based on passed in value
    {
        isSlowed = true;
        Speed = NormalSpeed - slow;
        slowDuration = duration;
        //check which steering and slow it
        if (GetComponent<BasicSeek>())
        {
            GetComponent<BasicSeek>().SlowToggle(Speed);
        }
        else if (GetComponent<BasicWanderSteering>())
        {
            GetComponent<BasicWanderSteering>().SlowToggle(Speed);
        }
    }
}
