﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class SeekRadius : MonoBehaviour {

    public bool seek;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //If there is a player in the radius, turn to true, which has another script switch to seek.
            seek = true;
        }
    }

}
