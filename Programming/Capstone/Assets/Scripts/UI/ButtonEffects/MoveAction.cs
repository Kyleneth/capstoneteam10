﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Direction
{
    Horizontal,
    Vertical
}

public class MoveAction : MonoBehaviour {

    [Tooltip("What kind of easing the button should have.")]
    public EaseType easeType = EaseType.CubeInOut;

    [Tooltip("What direction should the ease move in.")]
    public Direction direction = Direction.Horizontal;

    [Tooltip("Duration of the ease.")]
    public float duration = 1.2f;

    [Tooltip("Target to ease towards.")]
    public float distance;

    // The button this script is attached to
    private Button button;

    // The current target being eased toward
    private Vector2 targetPosition;

    // The current target being eased toward
    private float targetPositionScalar;

    // The rect transform of our button
    private RectTransform rectTransform;

    //private float timer = 0.0f;

	// Use this for initialization
	void Start ()
    {
        button = GetComponent<Button>();
        rectTransform = button.GetComponent<RectTransform>();

        targetPosition.x = rectTransform.anchoredPosition.x + distance;
        targetPosition.y = rectTransform.anchoredPosition.y;

        StartCoroutine(Execute(rectTransform.anchoredPosition, targetPosition, () =>
        {
        }));
    }

    void Update()
    {
    }

    IEnumerator Execute(Vector3 start, Vector3 end, System.Action done)
    {
        float t = 0.0f;

        while (t <= 1.0)
        {
            // normalize the time
            t += Time.deltaTime / duration;
            t = t <= 0.5f ? t * t * 2.0f : 1.0f - (--t) * t * 2.0f;

            button.gameObject.transform.localPosition = Vector3.Lerp(start, end, t);

            yield return null;
        }

        done();
    }
}
