﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMotionTest : MonoBehaviour
{
    private float duration = 2.0f;

    private CoroutineData data;

    // Use this for initialization
    void Start ()
    {
        data.target = gameObject;
        data.easeType = EaseType.ExpoInOut;
        data.duration = duration;

        Vector3 end = new Vector3(5.0f, transform.position.y, transform.position.z);

        StartCoroutine(Coroutines.Move(transform.position, end, data));
    }
}
