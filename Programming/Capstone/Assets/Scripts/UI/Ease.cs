﻿using UnityEngine;
using System;
using System.Collections.Generic;

[System.Serializable]
public enum EaseType
{
    Linear,
    QuadIn,
    QuadOut,
    QuadInOut,
    CubeIn,
    CubeOut,
    CubeInOut,
    BackIn,
    BackOut,
    BackInOut,
    ExpoIn,
    ExpoOut,
    ExpoInOut
}

public static class Ease
{
    private static Dictionary<EaseType, Func<float, float>> easeMethods = new Dictionary<EaseType, Func<float, float>>()
    {
        { EaseType.Linear,    Linear    },
        { EaseType.QuadIn,    QuadIn    },
        { EaseType.QuadOut,   QuadOut   },
        { EaseType.QuadInOut, QuadInOut },
        { EaseType.CubeIn,    CubeIn    },
        { EaseType.CubeOut,   CubeOut   },
        { EaseType.CubeInOut, CubeInOut },
        { EaseType.BackIn,    BackIn    },
        { EaseType.BackOut,   BackOut   },
        { EaseType.BackInOut, BackInOut },
        { EaseType.ExpoIn,    ExpoIn    },
        { EaseType.ExpoOut,   ExpoOut   },
        { EaseType.ExpoInOut, ExpoInOut }
    };

    public static Func<float, float> GetEasingFunc(EaseType type)
    {
        return easeMethods[type];
    }

    public static float Linear(float t)
    {
        return t;
    }

    public static float QuadIn(float t)
    {
        return t * t;
    }

    public static float QuadOut(float t)
    {
        return 1 - Ease.QuadIn(1 - t);
    }

    public static float QuadInOut(float t)
    {
        return (t <= 0.5f) ? Ease.QuadIn(t * 2) / 2 : Ease.QuadOut(t * 2 - 1) / 2 + 0.5f;
    }

    public static float CubeIn(float t)
    {
        return t * t * t;
    }

    public static float CubeOut(float t)
    {
        return 1 - Ease.CubeIn(1 - t);
    }

    public static float CubeInOut(float t)
    {
        return (t <= 0.5f) ? Ease.CubeIn(t * 2) / 2 : Ease.CubeOut(t * 2 - 1) / 2 + 0.5f;
    }

    public static float BackIn(float t)
    {
        return t * t * (2.70158f * t - 1.70158f);
    }

    public static float BackOut(float t)
    {
        return 1 - Ease.BackIn(1 - t);
    }

    public static float BackInOut(float t)
    {
        return (t <= 0.5f) ? Ease.BackIn(t * 2) / 2 : Ease.BackOut(t * 2 - 1) / 2 + 0.5f;
    }

    public static float ExpoIn(float t)
    {
        return Mathf.Pow(2, 10 * (t - 1));
    }

    public static float ExpoOut(float t)
    {
        return 1 - Ease.ExpoIn(1 - t);
    }

    public static float ExpoInOut(float t)
    {
        return (t <= 0.5f) ? Ease.ExpoIn(t * 2) / 2 : Ease.ExpoOut(t * 2 - 1) / 2 + 0.5f;
    }
}