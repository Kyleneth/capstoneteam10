﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimNeededThis : MonoBehaviour {
	public GameObject turnThisOne;
	public GameObject KillThisTwo;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "Player") 
		{
			turnThisOne.SetActive (true);
			if (KillThisTwo != null) 
			{
				Destroy (KillThisTwo);
			}
			Destroy (this.gameObject);
		}
	}
}
