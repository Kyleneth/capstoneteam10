﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SmoothTextWrite : MonoBehaviour {

    public Text textField;
    public string textToWrite;
    [Tooltip("0-1 for best use")]
    public float timePerLetter;
	public bool end=false;
	public float startTimer;
    private float timer;
    private char[] text;

    public GameObject nextTutorial;

    static int index = 0;
    void Start()
    {
		index = 0;
		timer = startTimer;
        text = textToWrite.ToCharArray();
       
    }
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            if (index < text.Length)
            {
                textField.text += text[index];
                ++index;
                timer = timePerLetter;
            }
        }
	}
    public void TutorialFinished()
    {
        nextTutorial.SetActive(true);
        Destroy(gameObject);
    }
}
