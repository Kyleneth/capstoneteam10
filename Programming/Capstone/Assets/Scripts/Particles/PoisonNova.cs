﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonNova : MonoBehaviour
{
    private ParticleSystem skullEmitter;

    void Awake()
    {
        Transform child = transform.Find("PoisonSkulls");
        skullEmitter = child.GetComponent<ParticleSystem>();
    }

    // Use this for initialization
    void Start ()
    {
    }

    // Update is called once per frame
    void Update ()
    {
        if (skullEmitter.isPlaying)
        {
            // @Refactor - this method of increasing radius is terrile.
            var r = skullEmitter.shape;
            r.radius += (Time.deltaTime / 1.2f);

            GetComponent<SphereCollider>().radius += (Time.deltaTime / 1.2f);
        }
    }
}
