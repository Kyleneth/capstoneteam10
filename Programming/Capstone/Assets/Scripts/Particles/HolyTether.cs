﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(LineRenderer))]
public class HolyTether : MonoBehaviour
{
    public LineRenderer lineRenderer;

    private int curveCount = 0;
    private int layerOrder = 0;
    private int segmentCount = 50;
    
    private const int numPoints = 4;
    private const int maxPlayers = 4;

    private List<GameObject> players = new List<GameObject>();
    private List<LineRenderer> lines = new List<LineRenderer>();
    private List<Vector3[]> points = new List<Vector3[]>()
    {
        new Vector3[numPoints],
        new Vector3[numPoints],
        new Vector3[numPoints]
    };

    private Transform priestess;

    public void FindPlayers(GameObject[] players, Transform origin)
    {
        priestess = origin;
        // Loop through and ignore the priest
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].transform == priestess) continue;
            else this.players.Add(players[i]);
        }

        for (int i = 0; i < this.players.Count; i++)
        {
            lines.Add(Instantiate(lineRenderer) as LineRenderer);

            // Generate bezier to that player
            Vector3 between = priestess.position - players[i].transform.position;
            Vector3 divided = between / numPoints;

            Vector3 previous = priestess.position;

            for (int j = 0; j < numPoints; j++)
            {
                Vector3 toAdd = previous - divided;
                toAdd.y = priestess.position.y;
                points[i][j] = toAdd;
                

                previous = toAdd;
            }

            lines[i].sortingLayerID = layerOrder;
            curveCount = numPoints / 3;

            lines[i].numPositions = numPoints;
            lines[i].SetPositions(points[i]);

            for (int j = 0; j < curveCount; j++)
            {
                for (int k = 1; k <= segmentCount; k++)
                {
                    float t = k / (float)segmentCount;
                    int nodeIndex = j * 3;
                    Vector3 pixel = CalculateBezierPoint(t, points[i][nodeIndex],
                                                         points[i][nodeIndex + 1], points[i][nodeIndex + 2],
                                                         points[i][nodeIndex + 3]);

                    lines[i].numPositions = (((j * segmentCount) + k));
                    lines[i].SetPosition((j * segmentCount) + (k - 1), pixel);
                }
            }
        }
    }

    public void UpdateOrigin(Transform origin)
    {
        priestess = origin;
    }

    void Update()
    {
        for (int i = 0; i < players.Count; i++)
        {
            GameObject player = players[i];
            LineRenderer line = lines[i];

            Vector3 between = priestess.position - players[i].transform.position;
            Vector3 divided = between / numPoints;

            Vector3 previous = priestess.position;

            for (int j = 0; j < numPoints; j++)
            {
                Vector3 toAdd = previous - divided;
                toAdd.y = player.transform.position.y;
                points[i][j] = toAdd;

                previous = toAdd;
            }

            points[i][0] = priestess.position;
            points[i][0].y = player.transform.position.y;
            points[i][3] = player.transform.position;

            // Grab the cross of the line renderer and the y axis
            Vector3 cross = Vector3.Cross(between, Vector3.up);
            Vector3.Normalize(cross);
            float magnitude1 = Mathf.Sin(Time.time / 7f) / 1.2f;
            cross *= magnitude1;
            points[i][1] += cross;

            Vector3.Normalize(cross);
            float magnitude2 = Mathf.Cos(Time.time / 7f) / 1.2f;
            cross *= magnitude2;
            points[i][2] += cross;

            //lines[i].numPositions = numPoints;
            //lines[i].SetPositions(points[i]);

            for (int j = 0; j < curveCount; j++)
            {
                for (int k = 1; k <= segmentCount; k++)
                {
                    float t = k / (float)segmentCount;
                    int nodeIndex = j * 3;
                    Vector3 pixel = CalculateBezierPoint(t, points[i][nodeIndex],
                                                         points[i][nodeIndex + 1], points[i][nodeIndex + 2],
                                                         points[i][nodeIndex + 3]);

                    line.numPositions = (((j * segmentCount) + k));
                    line.SetPosition((j * segmentCount) + (k - 1), pixel);
                }
            }
        }
    }

    Vector3 CalculateBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0;
        p += 3 * uu * t * p1;
        p += 3 * u * tt * p2;
        p += ttt * p3;

        return p;
    }

    private void OnDestroy()
    {
        for (int i = 0; i < players.Count; i++)
        {
            lines[i].numPositions = 0;
        }
    }

    public void Clear()
    {
        for (int i = 0; i < players.Count; i++)
        {
            lines[i].numPositions = 0;
        }
    }
}