﻿//AUTHOR: Kyle Netherland

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonePushAbility : MonoBehaviour {
    [Header("BonePush")]
    public float BonePushSpeed = 0.25f;
    public float BonePushTimeUntilDestroy = 0.1f;
    public float BonePushStartDistance = 0.5f;
    public int BonePushCost = 3;

    [Header("Prefabs")]
    public GameObject BonePush;

    [Header("Known Abilities")]
    public bool knowsBonePush = true;

    private int playerID;
    private NecroScript necro;
    private float globalCD = 0.25f;
    private int level = 1;
    private int maxLevel = 2;
    Image img;
    AudioReference audioRef;

    void Start () {
        playerID = gameObject.GetComponent<PlayerScript>().playerID;
        necro = GetComponent<NecroScript>();
        GameObject cdImg = GameObject.Find("BonePushCD");
        if (cdImg)
        {
            img = cdImg.GetComponent<Image>();
        }

        if (GameObject.Find("AudioHandler"))
        {
            audioRef = GameObject.Find("AudioHandler").GetComponent<AudioReference>();
        }
    }
	
	// Update is called once per frame
	void Update () {
        globalCD -= Time.deltaTime;

        if (img)
        {
            img.fillAmount = globalCD / 0.25f;
        }
        

        if (globalCD <= 0 && !necro.upgrading)
        {
            if (Input.GetButtonDown("RBumper" + playerID) && necro.GetSpiritPower() >= BonePushCost && knowsBonePush)
            {
                audioRef.PlayOneshot(audioRef.necroBP1);
                audioRef.PlayOneshot(audioRef.necroBP2);
                switch (level)
                {
                    case 1:
                        {
                            necro.staff.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.yellow);
                            Invoke("changeBack", 1);
                            GetComponent<NecroScript>().anim.SetTrigger("IsAttacking");
                            GameObject bones = Instantiate(BonePush, transform.position + (transform.forward * BonePushStartDistance), transform.rotation) as GameObject;
                            bones.GetComponentInChildren<Rigidbody>().AddForce(transform.forward * BonePushSpeed, ForceMode.VelocityChange);
                            bones.GetComponent<BonePush>().life = BonePushTimeUntilDestroy;
                            necro.SpiritGain(-BonePushCost);
                            globalCD = 0.25f;
                        }
                        break;
                    case 2:
                        {
                            necro.staff.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
                            Invoke("changeBack", 1);
                            GetComponent<NecroScript>().anim.SetTrigger("IsAttacking");
                            GameObject bones = Instantiate(BonePush, transform.position + (transform.forward * BonePushStartDistance), transform.rotation) as GameObject;
                            bones.GetComponentInChildren<Rigidbody>().AddForce(transform.forward * (BonePushSpeed+2.0f), ForceMode.VelocityChange);
                            bones.GetComponent<BonePush>().life = BonePushTimeUntilDestroy*2.0f;
                            necro.SpiritGain(-BonePushCost);
                            globalCD = 0.25f;
                        }
                        break;
                    case 3:
                        {

                        }
                        break;
                    case 4:
                        {

                        }
                        break;
                    default:
                        break;
                }
                

            }
        }
        if (necro.upgrading)
        {
            if (Input.GetButtonDown("RBumper" + playerID))
            {
                if (level < maxLevel)
                {
                    ++level;
                    necro.upgrading = false;
                }
            }
        }
    }

    void changeBack()
    {
        necro.staff.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.white);
    }
}
