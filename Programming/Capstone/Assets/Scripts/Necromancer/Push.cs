﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class Push : MonoBehaviour {
    //pushes enemies or coffins.
	void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponent<Rigidbody>().AddForce(transform.forward * 7, ForceMode.Impulse);
        }
		if (other.gameObject.tag == "Coffin")
		{
			other.gameObject.GetComponent<Rigidbody>().AddForce(transform.forward * 7, ForceMode.Impulse);
		}
    }
}
