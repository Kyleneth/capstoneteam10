﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class BloodBolt : MonoBehaviour {
    public float life = 10;
    public float damage;
    public GameObject blood; //blood squirt particle
    public GameObject bloodStamp;//blood decal
	
	void Update () { //destroy after timer
        life -= Time.deltaTime;
        if (life < 0)
        {
            Destroy(this.gameObject);
        }
	}
    void OnTriggerEnter(Collider other) 
    {
        if (other.tag == "Enemy") //if it hits an enemy, check if its a boss or normal enemy and then make them take damage. 
        {
            if (other.GetComponent<BaseEnemyScript>())
            {
                other.GetComponent<BaseEnemyScript>().TakeDamage(damage,0);
            }
            //create the blood particle
            GameObject temp = Instantiate(blood, transform.position, transform.rotation) as GameObject;
            Destroy(temp, .1f);
            Destroy(gameObject);
        }
        if (other.tag == "Wall" || other.tag == "Floor") //if you hit a wall or floor then throw up the decal and delete it on a timer
        {
            GameObject temp = Instantiate(blood, transform.position, transform.rotation) as GameObject;
            Destroy(temp, .1f);
            Instantiate(bloodStamp, transform.position, Quaternion.Inverse(transform.rotation));
            Destroy(gameObject);
        }
    }
}
