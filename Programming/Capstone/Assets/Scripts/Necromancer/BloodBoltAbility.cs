﻿//AUTHOR: Kyle Netherland

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BloodBoltAbility : MonoBehaviour {
    [Header("BloodBolt")]
    public float BloodBoltDamage = 2;//one time damage
    public float BloodBoltSpeed = 3;
    public float BloodBoltTimeUntilDestroy = 4f;
    [Header("Prefabs")]
    public GameObject BloodBolt;

    [Header("Known Abilities")]
    public bool knowsBloodBolt = true;

    private int playerID;
    private NecroScript necro;
    private float bbCD = 0.5f;
    private int level = 1;
    private int maxLevel = 2;
    Image img;

    AudioReference audioRef;
    void Start () {
        playerID = gameObject.GetComponent<PlayerScript>().playerID;
        necro = GetComponent<NecroScript>();
        GameObject cdImg = GameObject.Find("BloodCD");
        if (cdImg)
        {
            img = cdImg.GetComponent<Image>();
        }
        if (GameObject.Find("AudioHandler"))
        {
            audioRef = GameObject.Find("AudioHandler").GetComponent<AudioReference>();
        }
        
    }

	void Update () {

        bbCD -= Time.deltaTime;
        if (img)
        {
            img.fillAmount = bbCD / 0.5f;
        }
        

        if (bbCD <= 0 && !necro.upgrading)
        {
            if (Input.GetAxis("BasicAbilityAlt" + playerID) < 0.0 && knowsBloodBolt) 
            {
                audioRef.PlayOneshot(audioRef.necroBB);

                switch (level)
                {
                    case 1:
                        {
                            necro.staff.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.red);
                            Invoke("changeBack", 1);
                            GetComponent<NecroScript>().anim.SetTrigger("IsAttacking");
                            GameObject bolt = Instantiate(BloodBolt, transform.position, transform.rotation) as GameObject;
                            bolt.GetComponent<Rigidbody>().AddForce(transform.forward * BloodBoltSpeed, ForceMode.VelocityChange);
                            bolt.GetComponent<BloodBolt>().life = BloodBoltTimeUntilDestroy;
                            bolt.GetComponent<BloodBolt>().damage = BloodBoltDamage;
                            bbCD = 0.5f;
                        }
                        break;
                    case 2:
                        {
                            necro.staff.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.magenta);
                            Invoke("changeBack", 1);
                            GetComponent<NecroScript>().anim.SetTrigger("IsAttacking");
                            GameObject bolt = Instantiate(BloodBolt, transform.position, transform.rotation) as GameObject;
                            bolt.GetComponent<Rigidbody>().AddForce(transform.forward * BloodBoltSpeed, ForceMode.VelocityChange);
                            bolt.GetComponent<BloodBolt>().life = BloodBoltTimeUntilDestroy;
                            bolt.GetComponent<BloodBolt>().damage = BloodBoltDamage;

                            GameObject bolt2 = Instantiate(BloodBolt, transform.position, Quaternion.LookRotation(transform.right,transform.up)) as GameObject;
                            bolt2.GetComponent<Rigidbody>().AddForce(bolt2.transform.forward * BloodBoltSpeed, ForceMode.VelocityChange);
                            bolt2.GetComponent<BloodBolt>().life = BloodBoltTimeUntilDestroy;
                            bolt2.GetComponent<BloodBolt>().damage = BloodBoltDamage;
                            bbCD = 0.5f;
                        }
                        break;
                    case 3:
                        {

                        }
                        break;
                    case 4:
                        {

                        }
                        break;
                    default:
                        break;
                }
                
            }
        }
        if (necro.upgrading)
        {
            if (Input.GetAxis("BasicAbilityAlt" + playerID) < 0.0)
            {
                if (level < maxLevel)
                {
                    ++level;
                    necro.upgrading = false;
                }
            }
        }

    }
    void changeBack()
    {
        necro.staff.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.white);
    }
}
