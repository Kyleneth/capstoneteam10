﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NecroHealth : MonoBehaviour {

    public float currentHealth;   
    public Slider healthSlider;                                 
    NecroScript necro;

    public float currentPower;
    public Slider spiritSlider;
	public GameObject Hit;

    void Awake()
    {
        necro = GetComponent<NecroScript>();
        // Set the initial health of the player.
        currentHealth = necro.MaxHealth;
        currentPower = necro.MaxSpiritPower;
    }
    public void ChangeHealth(float currHP)
    {
		StartCoroutine (Hitme ());
        // Set the health bar's value to the current health.
        if (healthSlider)
        {
            healthSlider.value = currHP;
        }
    }
    public void changeSpirit(float currSpirit)
    {
		if (spiritSlider)
		{
			spiritSlider.value = currSpirit;
		}
    }
	IEnumerator Hitme()
	{
		//Hit.SetActive (true);
		yield return new WaitForSeconds (.5f);
		//Hit.SetActive (false);
	}

}
