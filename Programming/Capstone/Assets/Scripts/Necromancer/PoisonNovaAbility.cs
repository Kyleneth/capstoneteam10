﻿//AUTHOR: Kyle Netherland

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PoisonNovaAbility : MonoBehaviour {

    [Header("PoisonNova")]
    public float PoisonNovaDamage = 1;//damage per second
    public float PoisonDuration = 3;
    public float PoisonNovaTimeUntilDestroy = 2f;
    public int PoisonNovaCost = 2;

    [Header("Prefabs")]
    public GameObject PoisonNova;

    [Header("Known Abilities")]
    public bool knowsPoisonNova = true;

    private int playerID;
    private NecroScript necro;
    private float globalCD = 0.25f;
    private int level = 1;
    private int maxLevel = 2;
    Image img;
    AudioReference audioRef;
    void Start () {
        playerID = gameObject.GetComponent<PlayerScript>().playerID;
        necro = GetComponent<NecroScript>();
        GameObject cdImg = GameObject.Find("PoisonNovaCD");
        if (cdImg)
        {
            img = cdImg.GetComponent<Image>();
        }

        if (GameObject.Find("AudioHandler"))
        {
            audioRef = GameObject.Find("AudioHandler").GetComponent<AudioReference>();
        }
    }
	
	
	void Update () {
        globalCD -= Time.deltaTime;

        if (img)
        {
            img.fillAmount = globalCD / 0.25f;
        }
        

        if (globalCD <= 0 && !necro.upgrading)
        {

            if (Input.GetButtonDown("LBumper" + playerID) && necro.GetSpiritPower() >= PoisonNovaCost) //SecondaryAbility //ThirdAbility
            {
                audioRef.PlayOneshot(audioRef.necroPN);
                switch (level)
                {
                    case 1:
                        {
                            necro.staff.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.green);
                            Invoke("changeBack", 1);
                            GetComponent<NecroScript>().anim.SetTrigger("IsAttacking");
                            GameObject poison = Instantiate(PoisonNova, transform.position - new Vector3(0,0.2f,0), Quaternion.identity) as GameObject;
                            poison.GetComponent<Rigidbody>().AddForce(transform.forward * 8, ForceMode.VelocityChange);
                            poison.GetComponent<PoisonBolt>().life = PoisonNovaTimeUntilDestroy;
                            necro.SpiritGain(-PoisonNovaCost);
                            globalCD = 0.25f;
                        }
                        break;
                    case 2:
                        {
                            necro.staff.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.cyan);
                            Invoke("changeBack", 1);
                            GetComponent<NecroScript>().anim.SetTrigger("IsAttacking");
                            Instantiate(PoisonNova, transform.position - new Vector3(0, 0.2f, 0), Quaternion.identity);

                            necro.SpiritGain(-PoisonNovaCost);
                            globalCD = 0.25f;
                        }
                        break;
                    case 3:
                        {

                        }
                        break;
                    case 4:
                        {

                        }
                        break;
                    default:
                        break;
                }
                
            }

        }
        if (necro.upgrading)
        {
            if (Input.GetButtonDown("LBumper" + playerID)) 
            {
                if (level < maxLevel)
                {
                    ++level;
                    necro.upgrading = false;
                }
            }
        }
    }

    void changeBack()
    {
        necro.staff.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.white);
    }
}
