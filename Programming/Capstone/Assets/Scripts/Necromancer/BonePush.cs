﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class BonePush : MonoBehaviour {
    public float life;
    //destroy after a time limit
    void Update()
    {
        life -= Time.deltaTime;
        if (life < 0)
        {
            GetComponentInChildren<Rigidbody>().velocity = Vector3.zero;
            Destroy(gameObject, 1.0f);
        }
    }
}
