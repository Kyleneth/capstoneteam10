﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class PoisonNovaScript : MonoBehaviour {
    public float life = 6;
    public float damage,duration;

    void Update()
    {
        life -= Time.deltaTime;//destroy after time
        if (life < 0)
        {
            Destroy(gameObject);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            other.GetComponent<BaseEnemyScript>().Poison(damage,duration); //poison them
        }
    }
}
