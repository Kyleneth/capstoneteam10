﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeSkill : MonoBehaviour {

	void OnTriggerEnter(Collider other)
    {
        if (other.tag =="Player")
        {
            if (other.GetComponent<NecroScript>())
            {
                other.GetComponent<NecroScript>().UpgradeSkills();
				Destroy (this.gameObject);
            }
        }
    }
}
