﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class NecroScript : MonoBehaviour
{
    [Header("SpiritPower")]
    public float MaxSpiritPower = 10;
    private float currSpiritPower;

    [Header("Health")]
    public float MaxHealth;
    private float currHealth = 10;
    
    [Header("Effects")]
    public GameObject blood;

    [Header("Staff")]
    public GameObject staff;

    public Animator anim;
    public bool upgrading;

    public bool invincible = false;

    private Color ogCol = Color.white;
    private Color hurtCol = Color.red;
    private float colTime = 0.0f;
    private bool hurt = false;
    private bool unhurt = false;

    private Color yumCol = Color.green;
    private bool healed = false;
    private bool unhealed = false;
    AudioReference audioRef;
    GameObject pStats;
    public bool dead;

    void Start()
    {
        currSpiritPower = MaxSpiritPower;
        if (GameObject.Find("AudioHandler"))
        {
            audioRef = GameObject.Find("AudioHandler").GetComponent<AudioReference>();
        }
        pStats = GameObject.Find("PlayerStats");
    }
    public float GetSpiritPower()
    {
        return currSpiritPower;
    }
    public void SpiritGain(int gains)
    {
        pStats.GetComponent<PlayerStats>().addSpiritPower(gains);
        currSpiritPower += gains;
        if (currSpiritPower >= MaxSpiritPower)
        {
            currSpiritPower = MaxSpiritPower;
        }

        GetComponent<NecroHealth>().changeSpirit(currSpiritPower);
    }
    public void UpgradeSkills()
    {
        upgrading = true;
        //stop necromancer from using abilities, increase the level of whatever next ability he uses. 
    }
    void Update()
    {
        if(!dead)
        {
            if (invincible)
            {
                ogCol = Color.yellow;
            }
            else
            {
                ogCol = Color.white;
            }

            if (hurt)
            {
                foreach (Renderer r in GetComponentsInChildren<Renderer>())
                {
                    r.material.color = Color.Lerp(ogCol, hurtCol, colTime);
                }
                colTime += Time.deltaTime * 3.0f;
                if (colTime >= 1.0f)
                {
                    colTime = 0.0f;
                    unhurt = true;
                    hurt = false;
                }
            }
            if (unhurt)
            {
                foreach (Renderer r in GetComponentsInChildren<Renderer>())
                {
                    r.material.color = Color.Lerp(hurtCol, ogCol, colTime);
                }
                colTime += Time.deltaTime * 3.0f;
                if (colTime >= 1.0f)
                {
                    colTime = 0.0f;
                    unhurt = false;
                }
            }

            if (healed)
            {
                foreach (Renderer r in GetComponentsInChildren<Renderer>())
                {
                    r.material.color = Color.Lerp(ogCol, yumCol, colTime);
                }
                colTime += Time.deltaTime * 3.0f;
                if (colTime >= 1.0f)
                {
                    colTime = 0.0f;
                    unhealed = true;
                    healed = false;
                }
            }
            if (unhealed)
            {
                foreach (Renderer r in GetComponentsInChildren<Renderer>())
                {
                    r.material.color = Color.Lerp(yumCol, ogCol, colTime);
                }
                colTime += Time.deltaTime * 3.0f;
                if (colTime >= 1.0f)
                {
                    colTime = 0.0f;
                    unhealed = false;
                }
            }
        }
        
    }
    public void TakeDamage(float loss, int enemyID)
    {
        if (loss <= 0)
        {
            if (currHealth - loss <= MaxHealth)
            {
                healed = true;
                currHealth -= loss;
                GetComponent<NecroHealth>().ChangeHealth(currHealth);
            }
            else
            {
                currHealth = MaxHealth;
                GetComponent<NecroHealth>().ChangeHealth(currHealth);
            }
        }
        else
        {
            if (!invincible)
            {
                hurt = true;
                audioRef.PlayOneshot(audioRef.necroGrunt4);
                currHealth -= loss;
                GameObject temp2 = Instantiate(blood, transform.position, transform.rotation) as GameObject;
                Destroy(temp2, .1f);
                GetComponent<NecroHealth>().ChangeHealth(currHealth);
            }
        }
        if (currHealth <= MaxHealth * 0.5f)
        {
            audioRef.PlayOneshot(audioRef.almostDead);
        }
        if (currHealth <= 0 && !dead)
        {
            dead = true;
            anim.SetTrigger("Dying");
            
            if (enemyID == 0)
                pStats.GetComponent<PlayerStats>().killedBy("Templar");
            else if (enemyID == 1)
                pStats.GetComponent<PlayerStats>().killedBy("Priestess");
            else if (enemyID == 2)
                pStats.GetComponent<PlayerStats>().killedBy("Cleaver");

            Invoke("DeathTime", 1.6f);
        }

    }

    void DeathTime()
    {
        Time.timeScale = 0;
        pStats.GetComponent<Death>().Died();
    }
}
