﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PlayerHandler : MonoBehaviour {

    int numPlayers;
    public int tracker = 0;
    int[] playerIDs;//0 is first controller ..
    char[] characterID; //n for necro //s for spirit
    private GameObject eventSystem;
    private EventSystem localSystem;
    public GameObject inputResetObject;
    private StandaloneInputModule pauseController;
    public bool hasNecro;
    
    void Start () {
        //create players and characters
        playerIDs = new int[4];
        characterID = new char[4];
        eventSystem = GameObject.Find("EventSystem");
        localSystem = eventSystem.GetComponent<EventSystem>();
       
        pauseController = eventSystem.GetComponent<StandaloneInputModule>(); //returns an array of 4
    }

    public void Reset() //resets values.
    {
        tracker = 0;
        numPlayers = 0;
        playerIDs = new int[4];
        characterID = new char[4];
    }
    public void ChooseNecromancer()
    {
        //set the current tracker to the necromancer. 
        if (tracker < numPlayers)
        {
            tracker++;
            if (tracker > 4)
            {
                tracker = 4;
            }
            playerIDs[tracker - 1] = tracker;
            characterID[tracker - 1] = 'n';
            if (tracker < numPlayers)
            {
                //update the input system.
                pauseController.horizontalAxis = "Horizontal" + (tracker + 1);
                pauseController.verticalAxis = "Vertical" + (tracker + 1);
                pauseController.submitButton = "Submit" + (tracker + 1);
            }
            inputResetObject = GameObject.Find("StartButton4");
            localSystem.SetSelectedGameObject(inputResetObject);
            hasNecro = true;
        }
        
    }
    //same thing but with an s
    public void ChooseSpirit()
    {
        if (tracker < numPlayers)
        {
            tracker++;
            if (tracker > 4)
            {
                tracker = 4;
            }
            playerIDs[tracker - 1] = tracker;
            characterID[tracker - 1] = 's';

            if (tracker < numPlayers)
            {
                pauseController.horizontalAxis = "Horizontal" + (tracker + 1);
                pauseController.verticalAxis = "Vertical" + (tracker + 1);
                pauseController.submitButton = "Submit" + (tracker + 1);
            }
            inputResetObject = GameObject.Find("StartButton4");
            localSystem.SetSelectedGameObject(inputResetObject);
        }
        

    }
    public bool DoneSelecting()
    {
        return tracker == numPlayers;
    }
    public void SetPlayers(int num)
    {
        numPlayers = num;
    }
    public int GetNumPlayers()
    {
        return numPlayers;
    }
    public int[] GetPlayerIDs()
    {
        return playerIDs;
    }
    public char[] GetCharacterIDs()
    {
        return characterID;
    }
}
