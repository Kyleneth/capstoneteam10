﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

using UnityEngine.EventSystems;

//Similar to select on input, however this is for in game when you hit pause. It updates with who pauses it. 
public class PauseSelectOnInput : MonoBehaviour {

    public EventSystem eventSystem;
    public GameObject selectedObject;
    private PauseGame pauseHandler;

    private bool buttonSelected;
    void Start()
    {
        pauseHandler = GameObject.Find("PauseHandler").GetComponent<PauseGame>();
    }
    
    void Update()
    {
        if (pauseHandler.playerID > 0)
        {
            if (Input.GetAxisRaw("Vertical" + pauseHandler.playerID) != 0 && !buttonSelected || Input.GetAxisRaw("Horizontal" + pauseHandler.playerID) != 0 && !buttonSelected)
            {
                eventSystem.SetSelectedGameObject(selectedObject);
                buttonSelected = true;
            }
        }
        
    }

    private void OnDisable()
    {
        buttonSelected = false;
    }
}
