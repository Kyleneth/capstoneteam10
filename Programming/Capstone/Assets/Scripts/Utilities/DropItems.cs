﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItems : MonoBehaviour {
    [Header("Drop Type: Choose One")]
    public bool SpiritPowerDrop;
    public bool TimeDrop;
    public bool PageDrop;

    public float SpiritPer;
    public float TimePer;
    
    public int SpiritDropAmount;
    public int TimeDropAmount;
    bool quitting;
    // Update is called once per frame
    void Start()
    {
    }
    void OnApplicationQuit()
    {
        quitting = true;
    }
    void OnDestroy()
    {
        if (!quitting)
        {
            if (SpiritPowerDrop)
            {
                if (Random.Range(1, 100) <= SpiritPer)
                {
                    GameObject spiritPower = Resources.Load("SpiritPower", typeof(GameObject)) as GameObject;
                    GameObject temp = Instantiate(spiritPower, new Vector3(transform.position.x,transform.position.y+.5f,transform.position.z), transform.rotation) as GameObject;
                    temp.GetComponent<SpiritPowerPickUp>().value = SpiritDropAmount;
                }
            }
            else if (TimeDrop)
            {
                if (Random.Range(1, 100) <= TimePer)
                {
                    GameObject time = Resources.Load("TimerPickUp", typeof(GameObject)) as GameObject;
                    GameObject temp = Instantiate(time, new Vector3(transform.position.x, transform.position.y + .5f, transform.position.z), transform.rotation) as GameObject;
                    temp.GetComponent<SpiritPowerPickUp>().value = TimeDropAmount;
                }
            }
            else if (PageDrop)
            {
                GameObject page = Resources.Load("PagePickup", typeof(GameObject)) as GameObject;
                Instantiate(page, new Vector3(transform.position.x, 1f, transform.position.z), transform.rotation);
            }
            
        }

    }

}
