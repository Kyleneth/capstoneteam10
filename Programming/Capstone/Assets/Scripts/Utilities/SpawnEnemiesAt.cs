﻿//AUTHOR: Kyle Netherland, Tim White

using UnityEngine;
using System.Collections;

public class SpawnEnemiesAt : MonoBehaviour {
    public GameObject EnemyType; //What enemy
    public bool spawnNow; //tests
	public bool spawnNow2;
	public float timer; //timers
	public float timer2;
	public float maxTimer=3; //max timers
	public float maxTimer2=4;

    //pretty much reduce timer and play particles 
	void Update () {

		if (spawnNow == true) 
		{
			timer += Time.deltaTime;
		}
		if (spawnNow2 == true) 
		{
			timer2 += Time.deltaTime;
		}
		if (timer >= maxTimer) 
		{
			spawnNow = false;
			timer = 0;
			Instantiate (EnemyType, this.transform.position,Quaternion.Euler (0, 0, 0));
        }
		if (timer2 >= maxTimer2) 
		{
			spawnNow2 = false;
			timer2 = 0;

			GameObject[] Particles;
			Particles = GameObject.FindGameObjectsWithTag ("Particles");
			foreach (GameObject particle in Particles) 
			{
				Destroy (particle);
			}
			Destroy (this.gameObject);
		}
	}
}
