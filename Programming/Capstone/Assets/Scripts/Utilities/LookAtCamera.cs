﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class LookAtCamera : MonoBehaviour {

    void Update()
    {
        //Super simple, find the camera then look at it.
        Vector3 v = GameObject.Find("Main Camera").transform.position - transform.position;
        v.x = v.z = 0.0f;
        transform.LookAt(GameObject.Find("Main Camera").transform.position - v);
        transform.Rotate(0, 180, 0);
    }
}
