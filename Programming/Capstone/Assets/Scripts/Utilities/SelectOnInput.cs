﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SelectOnInput : MonoBehaviour
{

    public EventSystem eventSystem;
    public GameObject selectedObject;

    private bool buttonSelected;

    void Start()
    {
        eventSystem.SetSelectedGameObject(selectedObject);
        buttonSelected = true;
    }

    // This script allows players to use the joystick to navigate menus.
    void Update()
    {
       
        if (Input.GetAxisRaw("Vertical" + 1) >= 0.5 && !buttonSelected || Input.GetAxisRaw("Vertical" + 1) <= -0.5 && !buttonSelected
        || Input.GetAxisRaw("Horizontal" + 1) >= 0.5 && !buttonSelected || Input.GetAxisRaw("Horizontal" + 1) <= -0.5 && !buttonSelected
        || Input.GetAxisRaw("Vertical" + 2) >= 0.5 && !buttonSelected || Input.GetAxisRaw("Vertical" + 2) <= -0.5 && !buttonSelected
        || Input.GetAxisRaw("Horizontal" + 2) >= 0.5 && !buttonSelected || Input.GetAxisRaw("Horizontal" + 2) <= -0.5 && !buttonSelected
        || Input.GetAxisRaw("Vertical" + 3) >= 0.5 && !buttonSelected || Input.GetAxisRaw("Vertical" + 3) <= -0.5 && !buttonSelected
        || Input.GetAxisRaw("Horizontal" + 3) >= 0.5 && !buttonSelected || Input.GetAxisRaw("Horizontal" + 3) <= -0.5 && !buttonSelected
        || Input.GetAxisRaw("Vertical" + 4) >= 0.5 && !buttonSelected || Input.GetAxisRaw("Vertical" + 4) <= -0.5 && !buttonSelected
        || Input.GetAxisRaw("Horizontal" + 4) >= 0.5 && !buttonSelected || Input.GetAxisRaw("Horizontal" + 4) <= -0.5 && !buttonSelected)
        {
            eventSystem.SetSelectedGameObject(selectedObject);
            buttonSelected = true;
        }
        
        
    }

    private void OnDisable()
    {
        buttonSelected = false;
    }

}
