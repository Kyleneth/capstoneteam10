﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class FadeIntoLevel : MonoBehaviour
{
    float musicFade = 0;
    //Pretty easy to understand. just find the mask then fade it in
    public void FadeToLevelOne()
    {
        for (int i = 0; i < GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().GetNumPlayers(); i++)
        {
            if (GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().GetNumPlayers() == GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().tracker)
            {
                GameObject.Find("Mask").GetComponent<ScreenFader>().setFadeTime(2.0f);
                GameObject.Find("Mask").GetComponent<ScreenFader>().fadeIn = false;
                Invoke("advanceScene", 3.0f);
                musicFade += Time.deltaTime;

     			if (GameObject.Find("CharacterSelectPanel")) {
					GameObject.Find("CharacterSelectPanel").SetActive(false);
				}
                
            }
        }
        
    }
    private void advanceScene()
    {
        GameObject.Find("Mask").GetComponent<ScreenFader>().fadeIn = true;
        SceneManager.LoadScene(1);
    }
}
