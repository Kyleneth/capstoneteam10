﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterHandler : MonoBehaviour {
    private PlayerHandler pHandler;
    public Transform[] spawns;
    public GameObject necro;
    public GameObject[] spirits;
    private int uiTracker = 1;
    private int numSpirits = 1;
	void Start () {
        if (GameObject.Find("PlayerHandler"))
        {
            pHandler = GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>(); //first find the player handler

            //then loop through number of players playing.

            for (int i = 0; i < pHandler.GetNumPlayers(); i++)
            {
                //if they chose to be the necromancer then
                if (pHandler.GetCharacterIDs()[i] == 'n')
                {
                    //create the necromancer at the corresponding spawn point. set their id and the Name
                    GameObject temp = Instantiate(necro, spawns[i].position, Quaternion.identity) as GameObject;
                    temp.GetComponent<PlayerScript>().playerID = pHandler.GetPlayerIDs()[i];
                    temp.name = "Necromancer";

                    //change to .Start for testing and .StartFollow for game
                    //Then set the camera to follow the necromancer
                    GameObject.Find("CameraTarget").GetComponent<CameraFollow>().StartFollow();

                    //set the references the UI when created
                    temp.GetComponent<NecroHealth>().healthSlider = GameObject.Find("NecromancerHealth").GetComponent<Slider>();
                    temp.GetComponent<NecroHealth>().spiritSlider = GameObject.Find("NecromancerSpiritPower").GetComponent<Slider>();
                    temp.GetComponent<NecroHealth>().Hit = GameObject.Find("Hit");
                    //temp.GetComponent<NecroHealth> ().Hit.SetActive (false);

                }
                else if (pHandler.GetCharacterIDs()[i] == 's')
                {
                    //else if they are a spirit then create the spiit, assign the spawn and player ID and assign the name
                    GameObject temp = Instantiate(spirits[uiTracker - 1], spawns[i].position, Quaternion.identity) as GameObject;
                    temp.GetComponent<PlayerScript>().playerID = pHandler.GetPlayerIDs()[i];
                    temp.GetComponent<PlayerScript>().spiritID = numSpirits;
                    numSpirits++;
                    temp.name = "Spirit" + temp.GetComponent<PlayerScript>().playerID;

                    //set the ID and attach the two together then set the lights
                    switch (uiTracker)
                    {
                        case 1:
                            temp.GetComponentInChildren<Light>().color = Color.red;
                            break;
                        case 2:
                            temp.GetComponentInChildren<Light>().color = Color.blue;
                            break;
                        case 3:
                            temp.GetComponentInChildren<Light>().color = Color.green;
                            break;
                        default:
                            break;
                    }
                    uiTracker++;
                }
            }
            //deactivate UI that is not in use
            for (int j = uiTracker; j < 4; j++)
            {
                GameObject.Find("SpiritUI" + j).SetActive(false);
            }


        }
        
    }
}
