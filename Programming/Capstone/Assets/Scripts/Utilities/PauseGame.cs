﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


//messy way of setting the input for who pauses it.
public class PauseGame : MonoBehaviour {

    public bool gameOn = true;
    public EventSystem eventSystem;
    public GameObject background, pause,controls,necroControls,spiritControls,credits,settings;
    public int playerID = 0;
    private StandaloneInputModule pauseController;
    // Update is called once per frame
    void Start()
    {
        pauseController = eventSystem.GetComponent<StandaloneInputModule>(); //returns an array of 4
    }
    void Update () {
        
        if (Input.GetButtonDown("Pause1") && gameOn && playerID == 0)
        {
            Pause(1);
        }
        else if(Input.GetButtonDown("Pause1") && !gameOn && playerID == 1)
        {
            Resume();
        }
        if(Input.GetButtonDown("Pause2") && gameOn && playerID == 0)
        {
            Pause(2);
        }
        else if (Input.GetButtonDown("Pause2") && !gameOn && playerID == 2)
        {
            Resume();
        }
        else if (Input.GetButtonDown("Pause3") && gameOn && playerID == 0)
        {
            Pause(3);
        }
        else if (Input.GetButtonDown("Pause3") && !gameOn && playerID == 3)
        {
            Resume();
        }
        else if (Input.GetButtonDown("Pause4") && gameOn && playerID == 0)
        {
            Pause(4);
        }
        else if (Input.GetButtonDown("Pause4") && !gameOn && playerID == 4)
        {
            Resume();
        }
    }
    public void Resume()
    {
        playerID = 0;
        background.SetActive(false);
        pause.SetActive(false);
        controls.SetActive(false);
        necroControls.SetActive(false);
        spiritControls.SetActive(false);
        credits.SetActive(false);
        settings.SetActive(false);
        Time.timeScale = 1;
        gameOn = true;
    }
    public void Pause(int player)
    {
        playerID = player;
        Time.timeScale = 0;
        gameOn = false;
        background.SetActive(true);
        pause.SetActive(true);
        pauseController.horizontalAxis = "Horizontal" + player;
        pauseController.verticalAxis = "Vertical" + player;
        pauseController.submitButton = "Submit" + player;
    }
}
