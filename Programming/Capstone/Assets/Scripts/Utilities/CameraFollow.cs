﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;
using System;

public enum ZoomState
{
    In,
    Out
}

public class CameraFollow : MonoBehaviour
{
    #region MOVEMENT
    private GameObject mainPlayer;
    private Func<float, float> EasingFunction = Ease.GetEasingFunc(EaseType.QuadInOut);

    // Factor of how fast the camera eases to the player
    private float speed = 75.0f;
    #endregion

    #region ZOOMING
    private float startSize = 4f;
    private float endSize = 5.5f;

    private float spiritRadius = 7f;

    private EaseType type = EaseType.CubeOut;
    private CoroutineData data;

    [HideInInspector]
    public ZoomState zoomState = ZoomState.In;
    [HideInInspector]
    public bool transitioning = false;

    private System.Action callback;
    #endregion

    //change to Start for testing StartFollow for game
    public void Start()
    {
        if(GameObject.Find("Necromancer"))
            mainPlayer = GameObject.Find("Necromancer");

        data.duration = 1.2f;
        data.target = gameObject;
        data.easeType = type;

        callback = () => { transitioning = false; };
    }

    public void StartFollow()
    {
        mainPlayer = GameObject.Find("Necromancer");
    }

    void Update ()
    {
        float t = Time.deltaTime;

        t = EasingFunction(t);
        transform.position = Vector3.Lerp(transform.position, mainPlayer.transform.position, t * speed);

        // Check for zooming
        GameObject[] spirits = GameObject.FindGameObjectsWithTag("Spirit");

        float highestDst = 0.0f;
        foreach (GameObject obj in spirits)
        {
            float dst = Vector3.Distance(obj.transform.position, transform.position);
            if (dst > highestDst) highestDst = dst;
        }

        if (highestDst > spiritRadius && !transitioning && zoomState == ZoomState.In)
        {
            transitioning = true;
            StartCoroutine(Coroutines.Zoom(startSize, endSize, data, callback));
            zoomState = ZoomState.Out;
        }
        else if (highestDst < spiritRadius && !transitioning && zoomState == ZoomState.Out)
        {
            transitioning = true;
            StartCoroutine(Coroutines.Zoom(endSize, startSize, data, callback));
            zoomState = ZoomState.In;
        }
    }

    void ZoomCallback()
    {
        transitioning = false;
    }
}
