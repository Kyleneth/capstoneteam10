﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class UnderstoodBoss : MonoBehaviour {
    public EventSystem eventSystem;
    public GameObject selectedObject;

    private bool buttonSelected;

    void Update()
    {
        //This was for old pop ups, it's how to select the 'okay' button that pops up with them. 
        if (Input.GetAxisRaw("Horizontal" + 1) != 0 && !buttonSelected)
        {
            eventSystem.SetSelectedGameObject(selectedObject);
            buttonSelected = true;
        }

    }

    private void OnDisable()
    {
        buttonSelected = false;
    }

}
