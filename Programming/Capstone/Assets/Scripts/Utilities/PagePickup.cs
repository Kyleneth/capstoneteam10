﻿//AUTHOR: Tim White

using UnityEngine;
using System.Collections;

public class PagePickup : MonoBehaviour {

	// Use this for initialization
	public GameObject Page;
	public bool goTime=false;
	public RectTransform rt;
	public Vector2 targetScale;
	public Transform targetPosition; 

	public float timer;
	public float speed;
	void Start () 
	{
		rt = (RectTransform)Page.transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (goTime) 
		{
			timer += Time.deltaTime;
			if (timer >= 3) 
			{
				rt.sizeDelta = Vector2.Lerp (rt.sizeDelta, targetScale, speed * Time.deltaTime);
				rt.position = Vector3.Lerp (rt.position, targetPosition.position, speed * Time.deltaTime);
			}
			if (timer >= 4) 
			{
				Page.gameObject.SetActive (false);
				Destroy (this.gameObject);
			}
		}
	
	}
	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player")
		{
			Page.gameObject.SetActive (true);
			goTime = true;
		}
	}
}
