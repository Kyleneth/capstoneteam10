﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class IsoMovement : MonoBehaviour
{

    public float speed;
    private Quaternion newForward = Quaternion.identity;
    private float smoothingSpeed = 4.0f;

    float currSpeed = 0.0f;
    float t = 0.0f;
    float t2 = 0.0f;
    float angleT = 0.0f;

    GameObject player;
    //Movement and rotation for necromancer and spirits

    private void Start()
    {
        if (gameObject.tag == "Spirit")
        {
            smoothingSpeed = 2.0f;
        }
        player = GameObject.Find("Necromancer");
    }
    void Update()
    {
        if (player)
        {
            if (!player.GetComponent<NecroScript>().dead)
            {
                Vector3 temp1 = new Vector3(), temp2 = new Vector3();

                float x = Input.GetAxis("Horizontal" + gameObject.GetComponent<PlayerScript>().playerID);
                float y = 0;
                float z = Input.GetAxis("Vertical" + gameObject.GetComponent<PlayerScript>().playerID);

                if (newForward != Quaternion.identity)
                {
                    if (gameObject.tag == "Spirit")
                    {
                        transform.rotation = Quaternion.Slerp(transform.rotation, newForward, angleT);
                        angleT += smoothingSpeed * Time.deltaTime;
                        if (angleT > 1.0f)
                        {
                            angleT = 1.0f;
                        }
                    }
                    else
                    {
                        transform.rotation = Quaternion.Slerp(transform.rotation, newForward, angleT);
                        angleT += smoothingSpeed * Time.deltaTime;
                        if (angleT > 1.0f)
                        {
                            angleT = 1.0f;
                        }
                    }
                }


                if (new Vector3(x, y, z).magnitude > 0)
                {
                    t2 = 0.0f;
                    currSpeed = Mathf.Lerp(0.0f, speed, t);
                    t += smoothingSpeed * Time.deltaTime;
                    if (t > 1.0f)
                    {
                        t = 1.0f;
                    }
                    float heading = Mathf.Atan2(x, z);

                    if (Time.timeScale > 0)
                    {
                        newForward = Quaternion.Euler(0f, heading * Mathf.Rad2Deg + 45.0f, 0f);
                        temp1 = newForward * Vector3.forward;
                    }
                    if (gameObject.name == "Necromancer")
                    {
                        if (!GetComponent<NecroScript>().anim.GetBool("IsMoving"))
                        {
                            GetComponent<NecroScript>().anim.SetBool("IsMoving", true);
                        }
                    }

                    //is spirit
                    if (gameObject.tag == "Spirit")
                    {
                        if (!GetComponent<SpiritScript>().anim.GetBool("IsMoving"))
                        {
                            GetComponent<SpiritScript>().anim.SetBool("IsMoving", true);
                        }

                    }
                    //if enemy
                    if (GetComponent<AnimReference>() && !GetComponent<AnimReference>().anim.GetBool("IsRunning"))
                    {
                        GetComponent<AnimReference>().SetRun(true);

                    }
                }
                else
                {
                    currSpeed = Mathf.Lerp(speed, 0.0f, t2);
                    t2 += (smoothingSpeed / 2.0f) * Time.deltaTime;
                    if (t2 > 1.0f)
                    {
                        t2 = 1.0f;
                    }
                    t = 0.0f;

                    if (gameObject.name == "Necromancer" && (GetComponent<NecroScript>().anim.GetBool("IsMoving") || GetComponent<NecroScript>().anim.GetBool("IsBackPedaling")))
                    {
                        GetComponent<NecroScript>().anim.SetBool("IsMoving", false);
                        //GetComponent<NecroScript>().anim.SetBool("IsBackPedaling", false);
                    }
                    if (gameObject.tag == "Spirit" && GetComponent<SpiritScript>().anim.GetBool("IsMoving"))
                    {
                        GetComponent<SpiritScript>().anim.SetBool("IsMoving", false);
                    }

                    if (GetComponent<AnimReference>() && GetComponent<AnimReference>().anim.GetBool("IsRunning"))
                    {
                        GetComponent<AnimReference>().SetRun(false);
                    }
                }
                if (GetComponent<PossessedControl>())
                {
                    transform.position += temp1 * currSpeed * Time.deltaTime;
                }
                else if (GetComponent<CleaveControl>())
                {
                    if (!GetComponent<CleaveControl>().spinToWin)
                    {
                        transform.position += temp1 * currSpeed * Time.deltaTime;
                    }
                    else
                    {
                        transform.position += temp1 * (currSpeed / 2.0f) * Time.deltaTime;
                    }
                }
                else if (GetComponent<PriestControl>())
                {
                    if (GetComponent<PriestControl>().canMove)
                    {
                        transform.position += temp1 * currSpeed * Time.deltaTime;
                    }
                }
                else
                {
                    transform.position += temp1 * currSpeed * Time.deltaTime;
                }
                
                if (gameObject.tag == "Spirit")
                {
                    RaycastHit hit;
                    if (Physics.Raycast(transform.position, Vector3.up, out hit) || Physics.Raycast(transform.position, -Vector3.up, out hit))
                    {
                        if (hit.collider.tag == "Floor")
                        {
                            gameObject.transform.position = new Vector3(transform.position.x, hit.transform.position.y + 0.01f, transform.position.z);
                        }
                    }
                }

                //the following should allow looking around while not moving.
                float x2 = Input.GetAxis("HorizontalLook" + gameObject.GetComponent<PlayerScript>().playerID);
                float y2 = 0;
                float z2 = Input.GetAxis("VerticalLook" + gameObject.GetComponent<PlayerScript>().playerID);

                if (new Vector3(x2, y2, z2).magnitude > 0)
                {
                    float heading = Mathf.Atan2(x2, z2);
                    if (Time.timeScale > 0)
                    {
                        newForward = Quaternion.Euler(0f, heading * Mathf.Rad2Deg + 45.0f, 0f);
                    }

                    temp2 = transform.forward;
                    if (gameObject.name == "Necromancer")
                    {
                        temp2 = Vector3.Normalize(temp2);
                        temp1 = Vector3.Normalize(temp1);
                    }
                }
                else if (new Vector3(x2, y2, z2).magnitude <= 0 && new Vector3(x, y, z).magnitude <= 0)
                {
                    newForward = Quaternion.identity;
                    angleT = 0.0f;
                }


            }

        }
        else
        {
            player = GameObject.Find("Necromancer");
        }
        


    }
}
