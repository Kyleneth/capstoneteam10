﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour {
    //So here we set the timer Text, start timer, current timer(IE: what the time is after it starts ticking down) and seconds and minutes
    public Text timer;
    float startTime = 420;
    public static float currTime = 420;
    int seconds;
    int minutes;
    public float timePerLevel= 60;
    private void Awake()
    {
        if (GameObject.Find("Timer") != gameObject)
        {
            GameObject.Find("Timer");
            Destroy(gameObject);
        }
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 0)
        {
            Destroy(gameObject);
        }
        else if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 1 || UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 2)
        {
            currTime = startTime;
        }
        else if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex > 2)
        {
            currTime += timePerLevel;
        }
    }
    
    void Start()
    {
        //Here I get the number of minutes and seconds left. Minutes is simple, and by casting as int I can ensure it stays a whole number.
        
        minutes = (int)currTime / 60;
        seconds = (int)(currTime % 60); //seconds is also easy, as 60%60 = 0 but 59%60 = 59 
        if (seconds == 0)
        {
            timer.text = minutes + ":" + seconds + "0"; //convert to text
        }
        else
        {
            timer.text = minutes + ":" + seconds;
        }
    }
	// Update is called once per frame
	void Update () {
        //reduce time limit and redraw text
        currTime -= Time.deltaTime;
        minutes = (int)currTime / 60;
        seconds = (int)(currTime % 60);
        if (seconds == 0)
        {
            timer.text = minutes + ":" + seconds + "0";
        }
        else if(seconds < 10)
        {
            timer.text = minutes + ":0" + seconds;
        }
        else
        {
            timer.text = minutes + ":" + seconds;
        }

        if (currTime <= 0)
        {
            Time.timeScale = 0;
            GameObject.Find("PlayerStats").GetComponent<Death>().Died();
        }
    }
    public void AddTime(int time)
    {
        currTime += time;
    }
}
