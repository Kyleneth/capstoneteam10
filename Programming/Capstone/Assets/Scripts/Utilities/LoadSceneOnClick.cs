﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {

	public void LoadByIndex(int sceneIndex)
    {
        //Simply makes sure everyone is ready to go then starts the game
        if (GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().DoneSelecting() && GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().hasNecro)
        {
            Time.timeScale = 1;
            SceneManager.LoadSceneAsync(sceneIndex);
        }
    }
}
