﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerPickUp : MonoBehaviour {
    public int time = 20;
	void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameObject.Find("Timer").GetComponent<TimerScript>().AddTime(time);
            Destroy(gameObject);
        }
    }
}
