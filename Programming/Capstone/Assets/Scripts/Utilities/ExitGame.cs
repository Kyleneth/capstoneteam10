﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ExitGame : MonoBehaviour {

	public void Exit()
    {
        Time.timeScale = 1;
        SceneManager.LoadSceneAsync(0); //Exit to main menu
    }
}
