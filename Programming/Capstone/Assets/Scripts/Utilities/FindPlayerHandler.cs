﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//this acts like the bridge between playerHandler and menu
public class FindPlayerHandler : MonoBehaviour {

    public void ChooseNecro()
    {
        GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().ChooseNecromancer();
        
    }
    public void ChooseSpirit()
    {
        GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().ChooseSpirit();
    }
    public void SetPlayers(int num)
    {
        GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().SetPlayers(num);
    }
    public void Reset()
    {
        GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().Reset();
    }
}
