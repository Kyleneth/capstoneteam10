﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class StartTutorialInfo : MonoBehaviour {
    //No longer in the game i believe.. We may add it back, but it just sets an intro to true and pauses the game, which shows you a little snippit of info.
    public GameObject intro;
	// Use this for initialization
	void Awake () {
        Time.timeScale = 0;
        intro.SetActive(true);
        Destroy(gameObject);
	}
}
