﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class QuitOnClick : MonoBehaviour {

	public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false; //code that says if you quit in editor, it unplays, but if its in the app then it quits.
#else
        Application.Quit();
#endif
    }
}
