﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

//this class holds a public function to be called during menu navigation
public class FindAudioHandler : MonoBehaviour {

    void Start()
    {
        GameObject.Find("AudioHandler").GetComponent<AdjustVolume>().OnLoad();
    }
    public void GetMusic()
    {
        GameObject.Find("AudioHandler").GetComponent<AdjustVolume>().SetMusicVolume();
    }
    public void GetSFX()
    {
        GameObject.Find("AudioHandler").GetComponent<AudioReference>().SetSFXVolume();
    }
}
