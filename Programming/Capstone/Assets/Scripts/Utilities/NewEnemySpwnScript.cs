﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewEnemySpwnScript : MonoBehaviour {
	public GameObject[] Enemies;
	public GameObject spawnPart; //particle
	public Transform[] TempspawnPoints; //spawnpoints
	public int NumPoints;

    public float timer = 10;
    public float MaxTimer = 6;

    public int MAX_ENEMIES = 10;
    public int currEnemies = 0;
    public int numEnemiesPerSpawn = 3;
    // Use this for initialization
    GameObject player;
    void Start()
    {
        player = GameObject.Find("Necromancer");
    }
	// Update is called once per frame
	void Update () 
	{
        if (player)
        {
            if (TempspawnPoints.Length > 0)
            {
                timer -= Time.deltaTime;

                if (timer <= 0)
                {
                    if (currEnemies < MAX_ENEMIES)
                    {
                        float min = 100000;
                        int ind = 0;
                        for (int i = 0; i < TempspawnPoints.Length; i++)
                        {
                            float dist = (player.transform.position - TempspawnPoints[i].position).magnitude;
                            if (dist < min)
                            {
                                min = dist;
                                ind = i;
                            }
                        }
                        
                        for (int i = 0; i < numEnemiesPerSpawn; i++)
                        {
                            currEnemies++;
                            Instantiate(Enemies[Random.Range(0, 3)], TempspawnPoints[ind].position, Quaternion.identity);
                            timer = MaxTimer;
                        }
                        
                    }
                }
            }
        }
        else
        {
            player = GameObject.Find("Necromancer");
        }
		
	}
}
