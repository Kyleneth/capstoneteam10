﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropSpiritPower : MonoBehaviour {
    public GameObject SpiritDrop;
    public int SpiritAmount;
    bool quitting;
	// Update is called once per frame
    void OnApplicationQuit()
    {
        quitting = true;
    }
	void OnDestroy () {
        if (!quitting)
        {
            SpiritDrop = Resources.Load("SpiritPower", typeof(GameObject)) as GameObject;
            GameObject temp = Instantiate(SpiritDrop, transform.position, transform.rotation) as GameObject;
            temp.GetComponent<SpiritPowerPickUp>().value = SpiritAmount;
        }
        
    }
}
