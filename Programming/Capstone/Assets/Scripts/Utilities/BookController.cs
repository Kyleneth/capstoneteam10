﻿//AUTHOR: Tim White
//This is used to control the book that has the controls and information on elements

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BookController : MonoBehaviour {

	public bool gameOn = true;
	public EventSystem eventSystem;
	public GameObject background, book,controls,settings;
	public int playerID = 0;

	public Vector2 targetScale, baseScale;
	public Vector3 targetPosition, basePosition;

	public RectTransform rt;
	public float speed; 

	public int PageNum=1, PageMax=1;

	public Sprite Page1, Page2, Page3, Page41, Page42, Page5;

	private StandaloneInputModule bookController;
	// Update is called once per frame
	void Start()
	{
		bookController = eventSystem.GetComponent<StandaloneInputModule>(); //returns an array of 4
		rt = (RectTransform)book.transform;
		baseScale = rt.sizeDelta;
		basePosition = book.transform.position;
		targetPosition = new Vector3 (Screen.width / 2f, Screen.height / 2f, 0);
	}
	void Update () {

		if (Input.GetButtonDown("Back1") && gameOn && playerID == 0)
		{
			Pause(1);
		}
		else if(Input.GetButtonDown("Back1") && !gameOn && playerID == 1)
		{
			Resume();
		}
		if(Input.GetButtonDown("Back2") && gameOn && playerID == 0)
		{
			Pause(2);
		}
		else if (Input.GetButtonDown("Back2") && !gameOn && playerID == 2)
		{
			Resume();
		}
		else if (Input.GetButtonDown("Back3") && gameOn && playerID == 0)
		{
			Pause(3);
		}
		else if (Input.GetButtonDown("Back3") && !gameOn && playerID == 3)
		{
			Resume();
		}
		else if (Input.GetButtonDown("Back4") && gameOn && playerID == 0)
		{
			Pause(4);
		}
		else if (Input.GetButtonDown("Back4") && !gameOn && playerID == 4)
		{
			Resume();
		}
		if (!gameOn) 
		{
			rt.sizeDelta = Vector2.Lerp (rt.sizeDelta, targetScale, speed * Time.deltaTime);
			rt.position = Vector3.Lerp (rt.position, targetPosition, speed * Time.deltaTime);
			if (rt.sizeDelta.x>=999.8f) 
			{
				Time.timeScale = 0;
			}
			if (PageNum == 1) {
				Image image = book.GetComponent<Image> ();
				image.sprite = Page1;
			}
			if (PageNum == 2) {
				Image image = book.GetComponent<Image> ();
				image.sprite = Page2;
			}
			if (GameObject.Find ("Necromancer").GetComponent<BonePushAbility> ().knowsBonePush == true) {
				PageMax = 3;
				if (PageNum == 3) {
					Image image = book.GetComponent<Image> ();
					image.sprite = Page3;
				}
				if (PageNum == 4) {
					Image image = book.GetComponent<Image> ();
					image.sprite = Page41;
					if (GameObject.Find ("Necromancer").GetComponent<PoisonNovaAbility> ().knowsPoisonNova == true) 
					{
						Image image2= book.GetComponent<Image> ();
						image2.sprite = Page42;
					}
				}
			}
			if (GameObject.Find ("Necromancer").GetComponent<PoisonNovaAbility> ().knowsPoisonNova == true) {
				PageMax = 4;
				if (PageNum == 5) {
					Image image = book.GetComponent<Image> ();
					image.sprite = Page5;
				}
			}
			if (Input.GetButtonDown ("RBumper1") && playerID == 1) 
			{
				if (PageNum <= PageMax) {
					PageNum++;
				}
			}
			if (Input.GetButtonDown ("LBumper1") && playerID == 1) 
			{
				if (PageNum >= 2) {
					PageNum--;
				}
			}
			if (Input.GetButtonDown ("RBumper2") && playerID == 2) 
			{
				if (PageNum <= PageMax) {
					PageNum++;
				}
			}
			if (Input.GetButtonDown ("LBumper2") && playerID == 2) 
			{
				if (PageNum >= 2) {
					PageNum--;
				}
			}
			if (Input.GetButtonDown ("RBumper3") && playerID == 3) 
			{
				if (PageNum <= PageMax) {
					PageNum++;
				}
			}
			if (Input.GetButtonDown ("LBumper3") && playerID == 3) 
			{
				if (PageNum >= 2) {
					PageNum--;
				}
			}
			if (Input.GetButtonDown ("RBumper4") && playerID == 4) 
			{
				if (PageNum <= PageMax) {
					PageNum++;
				}
			}
			if (Input.GetButtonDown ("LBumper4") && playerID == 4) 
			{
				if (PageNum >= 2) {
					PageNum--;
				}
			}
		}
		if (gameOn) 
		{
			rt.sizeDelta = Vector2.Lerp (rt.sizeDelta, baseScale, speed * Time.deltaTime);
			rt.position = Vector3.Lerp (rt.position, basePosition, speed * Time.deltaTime);
		}
	}
	public void Resume()
	{
		PageNum = 1;
		playerID = 0;
		background.SetActive(false);
		//pause.SetActive(false);
		controls.SetActive(false);
		settings.SetActive(false);
		Time.timeScale = 1;
		gameOn = true;
	}
	public void Pause(int player)
	{
		playerID = player;
		//Time.timeScale = 0;
		gameOn = false;
		background.SetActive(true);
		//pause.SetActive(true);
		bookController.horizontalAxis = "Horizontal" + player;
		bookController.verticalAxis = "Vertical" + player;
		bookController.submitButton = "Submit" + player;
	}
}
