﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CheatCodesForEva : MonoBehaviour {
    void Awake()
    {
        if (GameObject.Find("ForEva")!=gameObject)
        {
            Destroy(gameObject);
        }
    }
   
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.A))
        {
            SceneManager.LoadSceneAsync(0);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            SceneManager.LoadSceneAsync(1);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            SceneManager.LoadSceneAsync(2);
        }

    }
}
