﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiritPowerPickUp : MonoBehaviour {

    public int value;
    void OnTriggerEnter(Collider other)
    {
        
        
        if (other.gameObject.tag == "Player")
        {
            if (other.GetComponent<NecroScript>())
            {
                other.GetComponent<NecroScript>().SpiritGain(value);
                Instantiate(Resources.Load("SpiritPowerPickUpParticles") as GameObject, transform.position, Quaternion.Euler(-90, 0, 0));
                Destroy(gameObject);
            }
        }
        
        
        
    }
}
