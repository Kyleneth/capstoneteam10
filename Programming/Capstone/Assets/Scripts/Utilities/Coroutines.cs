﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct CoroutineData
{
    public EaseType easeType;
    public GameObject target;
    public float duration;
}

public class Coroutines
{
    public static IEnumerator Move(Vector3 start, Vector3 end, CoroutineData data, System.Action callback = null)
    {
        float curTime = 0.0f;

        while (curTime < data.duration)
        {
            curTime += Time.deltaTime;
            float t = curTime / data.duration;

            var func = Ease.GetEasingFunc(data.easeType);
            t = func(t);

            data.target.transform.position = Vector3.Lerp(start, end, t);

            yield return null;
        }

        data.target.transform.position = end;
        if (callback != null) callback();
    }

    public static IEnumerator Zoom(float start, float end, CoroutineData data, System.Action callback = null)
    {
        float curTime = 0.0f;

        while (curTime < data.duration)
        {
            curTime += Time.deltaTime;
            float t = curTime / data.duration;

            var func = Ease.GetEasingFunc(data.easeType);
            t = func(t);
            
            Camera.main.orthographicSize = Mathf.Lerp(start, end, t);

            yield return null;
        }

        Camera.main.orthographicSize = end;
        if (callback != null) callback();
    }
}