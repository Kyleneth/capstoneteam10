﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AdjustVolume : MonoBehaviour {

    public float  musicVolume = 0.1f;
    public AudioSource music;
    public AudioSource ambience;
    public AudioClip GameMusic;
    public AudioClip AmbientMusic;
    
    void Start()
    {
        music = GetComponents<AudioSource>()[1]; //find audio source
        ambience = GetComponents<AudioSource>()[2];
        ambience.volume = musicVolume + .1f;
    }
    public void SetMusicVolume()
    {
        //set value of volume to the value of the slider as its changed.
        float val = GameObject.Find("MusicSlider").GetComponent<Slider>().value;
        musicVolume = val;
        music.volume = musicVolume;
        ambience.volume = musicVolume + .1f;
    }
    public void OnLoad()
    {
       
        music.volume = musicVolume;
        ambience.volume = musicVolume + .1f;
        //on load, get the current volume and set the sliders value to it.
        GameObject.Find("MusicSlider").GetComponent<Slider>().value = musicVolume;
    }

    public void OnSceneLoad()
    {
        if (music)
        {
            if (SceneManager.GetActiveScene().buildIndex != 0)
            {
                if (!music.isPlaying)
                {
                    music.Play();
                }
            }
            else
            {
                if (music.isPlaying)
                {
                    music.Stop();
                }
            }
        }
        
    }
}
