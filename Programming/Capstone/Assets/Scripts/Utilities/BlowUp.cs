﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class BlowUp : MonoBehaviour {
    
    [Header("Drop Type: Choose One")]
    public bool SpiritPowerDrop;
    public bool TimeDrop;
    public bool PageDrop;

    public float SpiritPer;
    public float TimePer;

    public int SpiritDropAmount;
    public int TimeDropAmount;
    bool quitting;

    public GameObject blowUpParticles;

       void OnTriggerEnter(Collider other)
    {
        if (other.tag == "BloodBolt" || other.tag == "Bone" || other.GetComponent<DamageOnCollision>() || other.GetComponent<HolyBolt>())
        {
            GameObject.Find("AudioHandler").GetComponent<AudioReference>().PlayOneshot(GameObject.Find("AudioHandler").GetComponent<AudioReference>().blowUp);
            GameObject temp = Instantiate(blowUpParticles, transform.position, Quaternion.identity) as GameObject; //blows up if somethings hit it
            Destroy(temp, 1f);
            Destroy(gameObject);

            if (SpiritPowerDrop)
            {
                if (Random.Range(1, 100) <= SpiritPer)
                {
                    GameObject spiritPower = Resources.Load("SpiritPower", typeof(GameObject)) as GameObject;
                    GameObject temp2 = Instantiate(spiritPower, new Vector3(transform.position.x, transform.position.y + .5f, transform.position.z), transform.rotation) as GameObject;
                    temp2.GetComponent<SpiritPowerPickUp>().value = SpiritDropAmount;
                }
            } 
            if (TimeDrop)
            {
                if (Random.Range(1, 100) <= TimePer)
                {
                    GameObject time = Resources.Load("TimerPickUp", typeof(GameObject)) as GameObject;
                    GameObject temp3 = Instantiate(time, new Vector3(transform.position.x, transform.position.y + .5f, transform.position.z), transform.rotation) as GameObject;
                    temp3.GetComponent<TimerPickUp>().time = TimeDropAmount;
                }
            }
            if (PageDrop)
            {
                GameObject page = Resources.Load("PagePickup", typeof(GameObject)) as GameObject;
                Instantiate(page, new Vector3(transform.position.x, 1f, transform.position.z), transform.rotation);
            }
        }
    }
} 
