﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class ResumeGame : MonoBehaviour {

	public void Resume()
    {
        GameObject.Find("PauseHandler").GetComponent<PauseGame>().Resume(); //pretty self explanatory
    }
}
