﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour
{
    int necro;
    bool chosen;
    // Update is called once per frame
    void Update()
    {
        //are the players ready and did anyone hit pause? If so then advance the scene.
        for (int i = 0; i < GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().GetNumPlayers(); i++)
        {
            if (Input.GetButtonDown("Pause" + (i + 1)) && !chosen)
            {
                necro = i;
                chosen = true;
                AdvanceLevel();
            }
        }
    }
    void AdvanceLevel()
    {
        for (int i = 0; i < GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().GetNumPlayers(); i++)
        {
            if (i != necro)
            {
                GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().ChooseSpirit();
            }
            else
            {
                GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>().ChooseNecromancer();
            }
        }
        GameObject.Find("Mask").GetComponent<ScreenFader>().setFadeTime(2.0f);
        GameObject.Find("Mask").GetComponent<ScreenFader>().fadeIn = false;
        if (GameObject.Find("CharacterSelectPanel"))
        {
            GameObject.Find("CharacterSelectPanel").SetActive(false);
        }
        GetComponent<FadeIntoLevel>().Invoke("advanceScene", 3.0f);
    }
}
