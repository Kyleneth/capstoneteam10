﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class TrackPlayerHealth : MonoBehaviour {
    //I think this was deprecated, but it tracks the players health and pauses the game and restarts it if they do.
    public GameObject restart;
	public void Died()
    {
        Time.timeScale = 0;
        restart.SetActive(true);
    }
}
