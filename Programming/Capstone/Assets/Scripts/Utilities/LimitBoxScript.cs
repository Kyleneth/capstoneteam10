﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class LimitBoxScript : MonoBehaviour {
    GameObject center;
    public float maxDist;

    //#region ZOOMING
    //private float startSize = 4f;
    //private float endSize = 5.5f;

    //private EaseType type = EaseType.CubeOut;
    //private CoroutineData data;

    //[HideInInspector]
    //public bool transitioning = false;
    //#endregion

    void Start()
    {
        //Sets the necromancer as the main object of focus
        center = GameObject.Find("Necromancer");

        //data.duration = 1.2f;
        //data.target = null;
        //data.easeType = type;
    }
	//This update ensures that if someones distance isn't too far away.
	void Update () {
        float dst = Vector3.Distance(center.transform.position, transform.position);
        if (dst > maxDist)
        {
            Vector3 distance = center.transform.position - transform.position;
            distance.Normalize();
            distance *= (dst - maxDist);
            // distance.y = 1;
            transform.position += distance;

            //StartCoroutine(Coroutines.Zoom(startSize, endSize, data));
        }
        //else StartCoroutine(Coroutines.Zoom(endSize, startSize, data));
        
	}
}
