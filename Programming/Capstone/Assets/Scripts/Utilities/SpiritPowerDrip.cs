﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiritPowerDrip : MonoBehaviour
{
    [Tooltip("The blob that falls from the spirit power every so often.")]
    public GameObject blob;

    [Tooltip("How long between drip spawns.")]
    public float interval;

    private float timer = 0.0f;

    private GameObject currentDrip;
    private GameObject spawnOrigin;

    // Use this for initialization
    void Start ()
    {
        spawnOrigin = GameObject.Find("SpawnOrigin");
    }

    // Update is called once per frame
    void Update ()
    {
        timer += Time.deltaTime;
        if (timer >= interval)
        {
            if (currentDrip) Destroy(currentDrip);
            currentDrip = Instantiate(blob);
            currentDrip.transform.parent = spawnOrigin.transform;
            currentDrip.transform.localPosition = GetRandomPosInCircle();

            timer = 0.0f;
        }
    }

    private Vector3 GetRandomPosInCircle()
    {
        float angle = Random.Range(0f, 1f) * Mathf.PI * 2f;

        float rad = Mathf.Sqrt(Random.Range(0f, 1f) * 0.3f);
        float x = spawnOrigin.transform.localPosition.x + rad * Mathf.Cos(angle);
        float y = spawnOrigin.transform.localPosition.y + rad * Mathf.Sin(angle);

        return new Vector3(x, 0, y);
    }
}
