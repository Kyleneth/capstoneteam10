﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class EndLevel : MonoBehaviour {
    public GameObject endLevel;
	//Don't think this is used, but it should end the level.
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (other.name == "Necromancer")
            {
                Time.timeScale = 0;
                endLevel.SetActive(true);
            }
        }
    }
}
