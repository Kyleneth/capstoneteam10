﻿
#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DropItems))]
public class ItemPickupEditor : Editor
{


    private bool SpiritPowerDrop = false;
    private bool TimeDrop = false;
    private bool PageDrop = false;
    private DropItems _evCtrl = null;

    void OnEnable()
    {
        _evCtrl = (DropItems)target;
        SpiritPowerDrop = _evCtrl.SpiritPowerDrop;
        TimeDrop = _evCtrl.TimeDrop;
        PageDrop = _evCtrl.PageDrop;

    }
    public override void OnInspectorGUI()
    {
        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        GUILayout.Label("SpiritPower", GUILayout.Width(70));
        SpiritPowerDrop = EditorGUILayout.Toggle(SpiritPowerDrop);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Time", GUILayout.Width(70));
        TimeDrop = EditorGUILayout.Toggle(TimeDrop);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Page", GUILayout.Width(70));
        PageDrop = EditorGUILayout.Toggle(PageDrop);
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
        if (SpiritPowerDrop)
        {
            _evCtrl.SpiritPowerDrop = true;
            _evCtrl.TimeDrop = false;
            _evCtrl.PageDrop = false;

            TimeDrop = false;
            PageDrop = false;
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();
            GUILayout.Label("SpiritPowerAmount", GUILayout.Width(100));
            _evCtrl.SpiritDropAmount = EditorGUILayout.IntField(_evCtrl.SpiritDropAmount);
            GUILayout.Label("SpiritPercentChance", GUILayout.Width(100));
            _evCtrl.SpiritPer = EditorGUILayout.FloatField(_evCtrl.SpiritPer);
        }
        if (TimeDrop)
        {

            SpiritPowerDrop = false;
            PageDrop = false;
            _evCtrl.SpiritPowerDrop = false;
            _evCtrl.TimeDrop = true;
            _evCtrl.PageDrop = false;
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();
            GUILayout.Label("TimeAmount", GUILayout.Width(100));
            _evCtrl.TimeDropAmount = EditorGUILayout.IntField(_evCtrl.TimeDropAmount);
            GUILayout.Label("TimePercentChance", GUILayout.Width(100));
            _evCtrl.TimePer = EditorGUILayout.FloatField(_evCtrl.TimePer);
        }
        if (PageDrop)
        {
            SpiritPowerDrop = false;
            TimeDrop = false;
            _evCtrl.SpiritPowerDrop = false;
            _evCtrl.TimeDrop = false;
            _evCtrl.PageDrop = true;
        }
    }

}
#endif
