﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class Firsttime : MonoBehaviour {
    public GameObject handler,pHandler;
	// This runs during the first time running to either create or find the necessary items
	void Start () {
        if (!GameObject.Find("AudioHandler"))
        {
            GameObject temp =Instantiate(handler) as GameObject;
            temp.name = "AudioHandler";
        }
        if (!GameObject.Find("PlayerHandler"))
        {
            GameObject temp = Instantiate(pHandler) as GameObject;
            temp.name = "PlayerHandler";
        }
        else
        {
            Destroy(GameObject.Find("PlayerHandler"));
            GameObject temp = Instantiate(pHandler) as GameObject;
            temp.name = "PlayerHandler";
        }
    }
	
}
