﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class PossessedControl : MonoBehaviour {
    [Header("Damage")]
    public float Damage = 3;
    public float NormalDamage = 3;
    [Header("AttackSpeed")]
    public float AttackSpeed = 1.5f;
    public float abilityTwoSpeed = 2.0f;
    public float abilityThreeSpeed = 5.0f;
    public float NormalAttackSpeed = 1.5f;
    public float MaxAttackSpeed = 0.0f;
    private float attackTimer = 0;
    private float abilityTwoTimer = 0;
    private float abilityThreeTimer = 0;

    public Color ogCol = Color.white;
    private Color hurtCol = Color.red;
    private float colTime = 0.0f;
    private bool hurt = false;
    private bool unhurt = false;
    private Color yumCol = Color.green;
    private bool healed = false;
    private bool unhealed = false;

    public GameObject sword,consecration;
    public float consecrationDuration = 5.0f;
    protected GameObject[] players;
    public GameObject blood;
    public GameObject Shield1;
    public GameObject Shield2;
	public GameObject RightBumper;
    public float attackRange;
    public bool attacking= false;

    public float amountOfDodge = 0.0f;
    public float buffDur = 0.0f;
    public float a2MaxCD = 10.0f;
    public bool invincible = false;

    ToggleUISwaps tog;
    AudioReference audioRef;
    //allow attacking. and another ability(?)
    void Start () 
	{

        blood = Resources.Load("BloodSplat", typeof(GameObject)) as GameObject;
        consecration = Resources.Load("ConsecretionStay", typeof(GameObject)) as GameObject;
        if (GameObject.Find("SpiritUI" + (GetComponent<PlayerScript>().spiritID)))
        {
            tog = GameObject.Find("SpiritUI" + (GetComponent<PlayerScript>().spiritID)).GetComponent<ToggleUISwaps>();

        }
        if (GameObject.Find("AudioHandler"))
        {
            audioRef = GameObject.Find("AudioHandler").GetComponent<AudioReference>();
        }
        
    }
	
	void Update () {
        //Allows for the basic attack when possessing
        attackTimer -= Time.deltaTime;
        abilityTwoTimer -= Time.deltaTime;
        abilityThreeTimer -= Time.deltaTime;
        if (tog)
        {
            tog.tCD1.fillAmount = attackTimer / AttackSpeed;
            tog.tCD2.fillAmount = abilityTwoTimer / abilityTwoSpeed;
            tog.tCD3.fillAmount = abilityThreeTimer / abilityThreeSpeed;
        }
        

        if (Input.GetAxis("BasicAbilityAlt" + GetComponent<PlayerScript>().playerID) < 0 && attackTimer <= 0)
        {
            Attack();
            attackTimer = AttackSpeed;
        }
        if (Input.GetButtonDown("SecondaryAbility" + GetComponent<PlayerScript>().playerID) && abilityTwoTimer <= 0)
        {
            AbilityTwo();
            abilityTwoTimer = abilityTwoSpeed;
        }
        if (Input.GetButtonDown("ThirdAbility" + GetComponent<PlayerScript>().playerID) && abilityThreeTimer <= 0)
        {
            AbilityThree();
            abilityThreeTimer = abilityThreeSpeed;
        }
        
        buffDur -= Time.deltaTime;

        if (buffDur <= 0)
        {
            Shield2.SetActive(false);
            Shield1.SetActive(false);
            amountOfDodge = 0;
        }
        if (hurt)
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                r.material.color = Color.Lerp(ogCol, hurtCol, colTime);
            }
            colTime += Time.deltaTime * 3.0f;
            if (colTime >= 1.0f)
            {
                colTime = 0.0f;
                unhurt = true;
                hurt = false;
            }
        }
        if (unhurt)
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                r.material.color = Color.Lerp(hurtCol, ogCol, colTime);
            }
            colTime += Time.deltaTime * 3.0f;
            if (colTime >= 1.0f)
            {
                colTime = 0.0f;
                unhurt = false;
            }
        }
        if (healed)
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                r.material.color = Color.Lerp(ogCol, yumCol, colTime);
            }
            colTime += Time.deltaTime * 3.0f;
            if (colTime >= 1.0f)
            {
                colTime = 0.0f;
                unhealed = true;
                healed = false;
            }
        }
        if (unhealed)
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                r.material.color = Color.Lerp(yumCol, ogCol, colTime);
            }
            colTime += Time.deltaTime * 3.0f;
            if (colTime >= 1.0f)
            {
                colTime = 0.0f;
                unhealed = false;
            }
        }
    }
    void AbilityTwo()
    {

        audioRef.PlayOneshot(audioRef.templarShields);
        buffDur = 4;
        amountOfDodge = 2;
        Shield1.SetActive(true);
        Shield2.SetActive(true);
    }
    void AbilityThree()
    {
        audioRef.PlayOneshot(audioRef.templarConsecration);
        GetComponent<AnimReference>().TriggerUltimate();
        GameObject temp = Instantiate(consecration, transform.position + new Vector3(0,0.1f,0), Quaternion.Euler(90,0,0)) as GameObject;
        Destroy(temp, consecrationDuration);
        //consecration
    }
    void Attack()
    {

        audioRef.PlayOneshot(audioRef.templarBasic);
        attacking = true;
        sword.GetComponent<DamageOnCollision>().ColliderOn = true;
        //trigger animation and then create a sword particle.
        GetComponent<AnimReference>().TriggerAttack();
        Invoke("EndAttack", 1.29f);
    }
    private void EndAttack()
    {
        attacking = false;
        sword.GetComponent<DamageOnCollision>().ColliderOn = false;
    }
    public void TakeDamage(float dmg)
    {
        
        if (dmg <= 0)
        {
            if (GetComponent<EnemyHealth>().Health - dmg <= GetComponent<EnemyHealth>().MaxHealth)
            {
                healed = true;
                GetComponent<EnemyHealth>().ChangeHealth(dmg);
            }
            else
            {
                GetComponent<EnemyHealth>().Health = GetComponent<EnemyHealth>().MaxHealth;
            }
        }
       else
        {
            if (!invincible)
            {
                if (amountOfDodge <= 0)
                {
                    hurt = true;
                    audioRef.PlayOneshot(audioRef.necroGrunt2);
                    GetComponent<EnemyHealth>().ChangeHealth(dmg);
                }
                if (amountOfDodge >= 1)
                {

                    audioRef.PlayOneshot(audioRef.templarShieldDeplete);
                    Shield1.SetActive(false);
                    amountOfDodge--;
                }
                if (amountOfDodge == 0)
                {
                    Shield2.SetActive(false);
                }
            }
            
        }
        if (GetComponent<EnemyHealth>().Health <= 0) //if enemy isn't dead and they should be then 
        {

            GameObject temp = Instantiate(blood, transform.position, transform.rotation) as GameObject; //make them bleed.
            Destroy(temp, .4f); //this destroys the particle after a time so that it doesn't stay.
            GetComponent<AnimReference>().TriggerDeath();
            if (GetComponent<BasicSeek>())
            {
                Destroy(GetComponent<BasicSeek>());
            }
            else if (GetComponent<BasicWanderSteering>())
            {
                Destroy(GetComponent<BasicWanderSteering>());
            }
            if (GetComponent<UnityEngine.AI.NavMeshAgent>())
            {
                //GetComponent<UnityEngine.AI.NavMeshAgent>().ResetPath();
                Destroy(GetComponent<UnityEngine.AI.NavMeshAgent>());
            }
            Destroy(GetComponent<Rigidbody>());
            Destroy(GetComponent<CapsuleCollider>());
            GameObject.Find("Spirit" + GetComponent<PlayerScript>().playerID).GetComponent<SpiritScript>().UnPossess();

            //this plays the death animation
            GetComponent<AnimReference>().TriggerDeath();
           
            Invoke("Kill", 2f); //this just gives it enough time for the animation to play
        }
    }

    void Kill()
    {
        Destroy(gameObject); //bye bye
    }
}
