﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//has the possession code in it
public class SpiritScript : MonoBehaviour
{
    [Header("Possession")] //the following is all for possession
    public GameObject possessTarget;
    public bool isPossessing;

    [Header("Known Abilities")]
    public bool knowsPossession = false;

    private float internalCoolDownForPossession = 2.0f;

    public SkinnedMeshRenderer mesh;
    public Animator anim;
    GameObject possParticles, particles;

    private ToggleUISwaps tog;
    private PlayerScript pScript;
    private GameObject pStats;
    private AudioReference audioRef;
    // Update is called once per frame
    void Start()
    {
        pScript = GetComponent<PlayerScript>();
        if (GameObject.Find("SpiritUI" + pScript.spiritID))
        {
            tog = GameObject.Find("SpiritUI" + pScript.spiritID).GetComponent<ToggleUISwaps>();
        }
        pStats = GameObject.Find("PlayerStats");
        if (GameObject.Find("AudioHandler"))
        {
            audioRef = GameObject.Find("AudioHandler").GetComponent<AudioReference>();
        }
        
    }
    void Update()
    {
        #region Possession Script
        internalCoolDownForPossession -= Time.deltaTime;
            
        if (isPossessing)
        {
            if (particles)
            {
                particles.transform.position = transform.position;
            }
            
            pStats.GetComponent<PlayerStats>().addTimePossessing(pScript.spiritID, Time.deltaTime);
            
            if (possessTarget.GetComponent<PriestControl>())
            {
                if (tog)
                {
                    tog.PriestHealth.value = possessTarget.GetComponent<EnemyHealth>().Health;
                }
                
            }
            else if (possessTarget.GetComponent<CleaveControl>())
            {
                if (tog)
                {
                    tog.CleaverHealth.value = possessTarget.GetComponent<EnemyHealth>().Health;
                }
            }
            else if (possessTarget.GetComponent<PossessedControl>())
            {
                if (tog)
                {
                    tog.TempHealth.value = possessTarget.GetComponent<EnemyHealth>().Health;
                }
            }

            if (Input.GetButtonDown("LBumper" + pScript.playerID) && internalCoolDownForPossession <= 0)
            {
                if (audioRef)
                {
                    audioRef.PlayOneshot(audioRef.spiritUnposs);
                }
                
                internalCoolDownForPossession = 2.0f;
                mesh.enabled = true;
                anim.SetTrigger("Exiting");
                if (tog)
                {
                    tog.SpiritBase.SetActive(true);
                    tog.TempBase.SetActive(false);
                    tog.PriestBase.SetActive(false);
                    tog.CleaverBase.SetActive(false);
                }
                Destroy(particles);

                if (possessTarget.GetComponent<PossessedControl>())
                {
                    possessTarget.AddComponent<BaseEnemyScript>();
					possessTarget.GetComponent<BaseEnemyScript>().RightBumper=possessTarget.GetComponent<PossessedControl>().RightBumper;
					possessTarget.GetComponent<BaseEnemyScript> ().TurnOnRB();
                    possessTarget.GetComponent<BaseEnemyScript>().sword = possessTarget.GetComponent<PossessedControl>().sword;

                    possessTarget.GetComponent<BaseEnemyScript>().Shield1 = possessTarget.GetComponent<PossessedControl>().Shield1;
                    possessTarget.GetComponent<BaseEnemyScript>().Shield2 = possessTarget.GetComponent<PossessedControl>().Shield2;
                    possessTarget.GetComponent<BaseEnemyScript>().sword.GetComponent<DamageOnCollision>().Possessing = false;
                    Destroy(possessTarget.GetComponent<PossessedControl>());
                }
                else if (possessTarget.GetComponent<PriestControl>())
                {
                    possessTarget.AddComponent<PriestScript>();
					possessTarget.GetComponent<PriestScript>().RightBumper=possessTarget.GetComponent<PriestControl>().RightBumper;
					possessTarget.GetComponent<BaseEnemyScript> ().TurnOnRB();
                    possessTarget.GetComponent<PriestScript>().aura = possessTarget.GetComponent<PriestControl>().aura;
                    possessTarget.GetComponent<PriestScript>().aura.GetComponent<HolyAuraScript>().possessing = false;
                    Destroy(possessTarget.GetComponent<PriestControl>());
                    GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                    foreach (GameObject player in players)
                    {
                        if (player.GetComponent<NecroScript>())
                        {
                            player.GetComponent<NecroScript>().invincible = false;
                        }
                        else if (player.GetComponent<PossessedControl>())
                        {
                            player.GetComponent<PossessedControl>().invincible = false;
                        }
                        else if (player.GetComponent<PriestControl>())
                        {
                            player.GetComponent<PriestControl>().invincible = false;
                        }
                        else if (player.GetComponent<CleaveControl>())
                        {
                            player.GetComponent<CleaveControl>().invincible = false;
                        }
                    }
                }
                else if (possessTarget.GetComponent<CleaveControl>())
                {
                    possessTarget.AddComponent<CleaveEnemy>();
					possessTarget.GetComponent<CleaveEnemy>().RightBumper=possessTarget.GetComponent<CleaveControl>().RightBumper;
					possessTarget.GetComponent<BaseEnemyScript> ().TurnOnRB();
                    possessTarget.GetComponent<CleaveEnemy>().sword = possessTarget.GetComponent<CleaveControl>().sword;
                    possessTarget.GetComponent<CleaveEnemy>().sword.GetComponent<DamageOnCollision>().Possessing = false;
                    Destroy(possessTarget.GetComponent<CleaveControl>());
                }

                
                if (GameObject.Find("EnemySpawning"))
                {
                    GameObject.Find("EnemySpawning").GetComponent<NewEnemySpwnScript>().currEnemies++;
                }
                Destroy(possessTarget.GetComponent<PlayerScript>());
                Destroy(possessTarget.GetComponent<IsoMovement>());
                possessTarget.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
                possessTarget.AddComponent<BasicWanderSteering>();
                possessTarget.GetComponent<BasicWanderSteering>().shouldSeek = possessTarget.transform.GetChild(1).gameObject;
                Destroy(possessTarget.GetComponent<LimitBoxScript>());
                possessTarget.tag = "Enemy";
                possessTarget = null;
                isPossessing = false;
            }
            if (possessTarget)
            {
                //****
                transform.position = possessTarget.transform.position;
            }
        }
        else
        {
            if (tog)
            {
                tog.AB1.fillAmount = internalCoolDownForPossession / 2.0f;
            }
           
        }

        #endregion
    }
    void turnOffMesh()
    {
        mesh.enabled = false;
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            #region Possess Start
            if (Input.GetButtonDown("RBumper" + pScript.playerID) && !isPossessing && knowsPossession && other.gameObject.GetComponent<BaseEnemyScript>() && internalCoolDownForPossession <=0)//possession 
                //hoping checking if they have an enemy script will stop other people from possessing already possessed ones
            {
                if (audioRef)
                {
                    audioRef.PlayOneshot(audioRef.spiritPoss);
                }
                other.GetComponent<BaseEnemyScript> ().TurnOffRB();
                switch (pScript.spiritID)
                {
                    case 1:
                        possParticles = Resources.Load("Spirit1PossessGraphics", typeof(GameObject)) as GameObject;
                        break;
                    case 2:
                        possParticles = Resources.Load("Spirit2PossessGraphics", typeof(GameObject)) as GameObject;
                        break;
                    case 3:
                        possParticles = Resources.Load("Spirit3PossessGraphics", typeof(GameObject)) as GameObject;
                        break;
                    default:
                        break;
                }
                if (possParticles)
                {
                    particles = Instantiate(possParticles, transform.position, Quaternion.Euler(-90, 0, 0)) as GameObject;
                }

                internalCoolDownForPossession = 2.0f;
                anim.SetTrigger("Possessing");
                Invoke("turnOffMesh", 1.4f);
                isPossessing = true;
                possessTarget = other.gameObject;


                if (tog)
                {
                    tog.SpiritBase.SetActive(false);
                }

                //super messy, but this is for the priest enemy. 
                if (possessTarget.GetComponent<PriestScript>())
                {
                    if (tog)
                    {
                        tog.PriestBase.SetActive(true);
                        tog.TempBase.SetActive(false);
                        tog.CleaverBase.SetActive(false);
                    }
                    possessTarget.AddComponent<PriestControl>();
                    possessTarget.GetComponent<PriestControl>().aura = possessTarget.GetComponent<PriestScript>().aura;
					possessTarget.GetComponent<PriestControl>().RightBumper=possessTarget.GetComponent<PriestScript>().RightBumper;
                    possessTarget.GetComponent<PriestControl>().aura.GetComponent<HolyAuraScript>().possessing = true;
                    possessTarget.GetComponent<PriestControl>().aura.GetComponent<HolyAuraScript>().spiritID = pScript.spiritID;
                    Destroy(possessTarget.GetComponent<PriestScript>());

                }
                else if (possessTarget.GetComponent<CleaveEnemy>())
                {
                    if (tog)
                    {
                        tog.CleaverBase.SetActive(true);
                        tog.PriestBase.SetActive(false);
                        tog.TempBase.SetActive(false);
                    }

                    possessTarget.AddComponent<CleaveControl>();
                    possessTarget.GetComponent<CleaveControl>().sword = possessTarget.GetComponent<CleaveEnemy>().sword;
					possessTarget.GetComponent<CleaveControl>().RightBumper=possessTarget.GetComponent<CleaveEnemy>().RightBumper;
                    possessTarget.GetComponent<CleaveControl>().sword.GetComponent<DamageOnCollision>().Possessing = true;
                    possessTarget.GetComponent<CleaveControl>().sword.GetComponent<DamageOnCollision>().spiritID = pScript.spiritID;
                    Destroy(possessTarget.GetComponent<CleaveEnemy>());

                }
                else
                {
                    if (tog)
                    {
                        tog.TempBase.SetActive(true);
                        tog.CleaverBase.SetActive(false);
                        tog.PriestBase.SetActive(false);
                    }
                    

                    possessTarget.AddComponent<PossessedControl>();
                    possessTarget.GetComponent<PossessedControl>().sword = possessTarget.GetComponent<BaseEnemyScript>().sword;
					possessTarget.GetComponent<PossessedControl>().RightBumper=possessTarget.GetComponent<BaseEnemyScript>().RightBumper;
                    possessTarget.GetComponent<PossessedControl>().Shield1 = possessTarget.GetComponent<BaseEnemyScript>().Shield1;
                    possessTarget.GetComponent<PossessedControl>().Shield2 = possessTarget.GetComponent<BaseEnemyScript>().Shield2;
                    possessTarget.GetComponent<PossessedControl>().sword.GetComponent<DamageOnCollision>().Possessing = true;
                    possessTarget.GetComponent<PossessedControl>().sword.GetComponent<DamageOnCollision>().spiritID = pScript.spiritID;
                    Destroy(possessTarget.GetComponent<BaseEnemyScript>());

                }

                if (possessTarget.GetComponent<BasicSeek>())
                {
                    Destroy(possessTarget.GetComponent<BasicSeek>());
                }
                if (possessTarget.GetComponent<BasicWanderSteering>())
                {
                    Destroy(possessTarget.GetComponent<BasicWanderSteering>());
                }

                pStats.GetComponent<PlayerStats>().addPossEnemySpirit(pScript.spiritID);
                if (GameObject.Find("EnemySpawning"))
                {
                    GameObject.Find("EnemySpawning").GetComponent<NewEnemySpwnScript>().currEnemies--;
                }
               
                possessTarget.AddComponent<LimitBoxScript>();
                possessTarget.GetComponent<LimitBoxScript>().maxDist = GetComponent<LimitBoxScript>().maxDist;
                possessTarget.tag = "Player";
                possessTarget.AddComponent<IsoMovement>();
                possessTarget.GetComponent<IsoMovement>().speed = possessTarget.GetComponent<EnemySpeed>().Speed;
                possessTarget.AddComponent<PlayerScript>();
                possessTarget.GetComponent<PlayerScript>().playerID = pScript.playerID;
                possessTarget.GetComponent<PlayerScript>().spiritID = pScript.spiritID;
                possessTarget.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
            }
            #endregion

        }

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
			other.GetComponent<BaseEnemyScript> ().TurnOnRB();
            Light lit = GetComponentInChildren<Light>();
            Color mine = lit.color;
            if (mine.r > 0)
            {
                mine.b = 0.4f;
                mine.g = 0.4f;
            }
            else if (mine.b > 0)
            {
                mine.r = 0.4f;
                mine.g = 0.4f;
            }
            else if (mine.g > 0)
            {
                mine.r = 0.4f;
                mine.b = 0.4f;
            }

            lit.color = Color.Lerp(lit.color, mine, 0.5f);
        }


    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Enemy")
        {
			other.GetComponent<BaseEnemyScript> ().TurnOffRB();
            Light lit = GetComponentInChildren<Light>();
            Color mine = lit.color;
            if (mine.r > 0.5f)
            {
                mine.b = 0f;
                mine.g = 0f;
            }
            else if (mine.b > 0.5f)
            {
                mine.r = 0f;
                mine.g = 0f;
            }
            else if (mine.g > 0.5f)
            {
                mine.r = 0f;
                mine.b = 0f;
            }
            lit.color = Color.Lerp(lit.color, mine, 0.5f);
        }
    }
    public void UnPossess()
    {
        if (audioRef)
        {
            audioRef.PlayOneshot(audioRef.spiritUnposs);
        }
       
        Destroy(particles);
        if (possessTarget.GetComponent<PossessedControl>())
        {
             Destroy(possessTarget.GetComponent<PossessedControl>());
        }
        else if (possessTarget.GetComponent<PriestControl>())
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                if (player.GetComponent<NecroScript>())
                {
                    player.GetComponent<NecroScript>().invincible = false;
                }
                else if (player.GetComponent<PossessedControl>())
                {
                    player.GetComponent<PossessedControl>().invincible = false;
                }
                else if (player.GetComponent<PriestControl>())
                {
                    player.GetComponent<PriestControl>().invincible = false;
                }
                else if (player.GetComponent<CleaveControl>())
                {
                    player.GetComponent<CleaveControl>().invincible = false;
                }
            }
            Destroy(possessTarget.GetComponent<PriestControl>());
        }
        else if (possessTarget.GetComponent<CleaveControl>())
        {
            Destroy(possessTarget.GetComponent<CleaveControl>());
        }
        if (tog)
        {
            tog.SpiritBase.SetActive(true);

            tog.TempBase.SetActive(false);
            tog.PriestBase.SetActive(false);
            tog.CleaverBase.SetActive(false);
        }
        if (GameObject.Find("EnemySpawning"))
        {
            GameObject.Find("EnemySpawning").GetComponent<NewEnemySpwnScript>().currEnemies++;
        }
        Destroy(possessTarget.GetComponent<LimitBoxScript>());
        mesh.enabled = true;
        anim.SetTrigger("Exiting");
        Destroy(possessTarget.GetComponent<PlayerScript>());
        Destroy(possessTarget.GetComponent<IsoMovement>());
        possessTarget.tag = "Enemy";
        if (possessTarget.GetComponent<EnemyHealth>().Health <= 0)
        {
            Destroy(possessTarget);
        }
        possessTarget = null;
        isPossessing = false;
    }
}
