﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriestControl : MonoBehaviour
{
    [Header("Damage")]
    public float Damage = 3;
    public float NormalDamage = 3;
    [Header("AttackSpeed")]
    public float AttackSpeed = 1f;
    public float abilityTwoSpeed = 2.0f;
    public float abilityThreeSpeed = 5.0f;
    public float NormalAttackSpeed = 1f;
    public float MaxAttackSpeed = 0.0f;
    private float attackTimer = 0;
    private float abilityTwoTimer = 0;
    private float abilityThreeTimer = 0;

    public Color ogCol = Color.white;
    private Color hurtCol = Color.red;
    private float colTime = 0.0f;
    private bool hurt = false;
    private bool unhurt = false;
    private Color yumCol = Color.green;
    private bool healed = false;
    private bool unhealed = false;

    private float ampItUpTimer = 3.0f;
    private bool ampingItUp = false;
    public bool canMove = true;

    private float heroesNeverDieTimer = 2.0f;
    private bool heroesNeverDie = false;

    public bool invincible = false;
    public GameObject bolt;
    public GameObject aura;
    public GameObject blood;
    public GameObject tether;
	public GameObject RightBumper;
    public float attackRange;

    private HolyTether holyTetherRef;
    ToggleUISwaps tog;
    AudioReference audioRef;
    void Start()
    {
        bolt = Resources.Load("EnergyBolt", typeof(GameObject)) as GameObject;
        blood = Resources.Load("BloodSplat", typeof(GameObject)) as GameObject;
        tether = Resources.Load("HolyTether", typeof(GameObject)) as GameObject;
        if (GameObject.Find("SpiritUI" + (GetComponent<PlayerScript>().spiritID)))
        {
            tog = GameObject.Find("SpiritUI" + (GetComponent<PlayerScript>().spiritID)).GetComponent<ToggleUISwaps>();
        }
        if (GameObject.Find("AudioHandler"))
        {
            audioRef = GameObject.Find("AudioHandler").GetComponent<AudioReference>();
        }
        
    }
    void Update()
    {
        //Allows for the basic attack when possessing
        attackTimer -= Time.deltaTime;
        abilityTwoTimer -= Time.deltaTime;
        abilityThreeTimer -= Time.deltaTime;

        if (tog)
        {
            tog.pCD1.fillAmount = attackTimer / AttackSpeed;
            tog.pCD2.fillAmount = abilityTwoTimer / abilityTwoSpeed;
            tog.pCD3.fillAmount = abilityThreeTimer / abilityThreeSpeed;
        }
        

        if (Input.GetAxis("BasicAbilityAlt" + GetComponent<PlayerScript>().playerID) < 0 && attackTimer <= 0)
        {
            Attack();
            attackTimer = AttackSpeed;
        }
        if (Input.GetButtonDown("SecondaryAbility" + GetComponent<PlayerScript>().playerID) && abilityTwoTimer <= 0)
        {
            AbilityTwo();
            abilityTwoTimer = abilityTwoSpeed;
        }
        if (Input.GetButtonDown("ThirdAbility" + GetComponent<PlayerScript>().playerID) && abilityThreeTimer <= 0)
        {
            AbilityThree();

            // Set the cooldown for tether to the timer plus the amount of time it takes to complete the ability
            abilityThreeTimer = abilityThreeSpeed + heroesNeverDieTimer;
        }

        if (ampingItUp)
        {
            //pulse ability look here. like lucio
            ampItUpTimer -= Time.deltaTime;
            if (ampItUpTimer <= 0)
            {
                ampingItUp = false;
                ampItUpTimer = 3.0f;

                aura.GetComponent<HolyAuraScript>().healAmount -= 1;
            }
        }
        if (heroesNeverDie)
        {
            heroesNeverDieTimer -= Time.deltaTime;
            tether.GetComponent<HolyTether>().UpdateOrigin(transform);
            if (heroesNeverDieTimer <= 0)
            {
                heroesNeverDie = false;
                heroesNeverDieTimer = 2.0f;

                GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
                foreach (GameObject player in players)
                {
                    if (player.GetComponent<NecroScript>())
                    {
                        player.GetComponent<NecroScript>().invincible = false;
                    }
                    else if (player.GetComponent<PossessedControl>())
                    {
                        player.GetComponent<PossessedControl>().invincible = false;
                    }
                    else if (player.GetComponent<PriestControl>())
                    {
                        player.GetComponent<PriestControl>().invincible = false;
                    }
                    else if (player.GetComponent<CleaveControl>())
                    {
                        player.GetComponent<CleaveControl>().invincible = false;
                    }
                }

                Destroy(tether);
            }
        }
        if (hurt)
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                r.material.color = Color.Lerp(ogCol, hurtCol, colTime);
            }
            colTime += Time.deltaTime * 3.0f;
            if (colTime >= 1.0f)
            {
                colTime = 0.0f;
                unhurt = true;
                hurt = false;
            }
        }
        if (unhurt)
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                r.material.color = Color.Lerp(hurtCol, ogCol, colTime);
            }
            colTime += Time.deltaTime * 3.0f;
            if (colTime >= 1.0f)
            {
                colTime = 0.0f;
                unhurt = false;
            }
        }
        if (healed)
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                r.material.color = Color.Lerp(ogCol, yumCol, colTime);
            }
            colTime += Time.deltaTime * 3.0f;
            if (colTime >= 1.0f)
            {
                colTime = 0.0f;
                unhealed = true;
                healed = false;
            }
        }
        if (unhealed)
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                r.material.color = Color.Lerp(yumCol, ogCol, colTime);
            }
            colTime += Time.deltaTime * 3.0f;
            if (colTime >= 1.0f)
            {
                colTime = 0.0f;
                unhealed = false;
            }
        }
    }

    void AbilityTwo()
    {
        ampingItUp = true;
        aura.GetComponent<HolyAuraScript>().healAmount += 1;
    }
    void AbilityThree()
    {
        GetComponent<AnimReference>().TriggerUltimate();

        heroesNeverDie = true;
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        //tether to friends, do something similar to amp it up timer, but after its up remove invincibility.
        if (!tether) tether = Resources.Load("HolyTether", typeof(GameObject)) as GameObject;
        tether = Instantiate(tether);

        if (!holyTetherRef) holyTetherRef = tether.GetComponent<HolyTether>();
        holyTetherRef.FindPlayers(players, transform);

        foreach (GameObject player in players)
        {
            if (player.GetComponent<NecroScript>())
            {
                player.GetComponent<NecroScript>().invincible = true;
            }
            else if (player.GetComponent<PossessedControl>())
            {
                player.GetComponent<PossessedControl>().invincible = true;
            }
            else if (player.GetComponent<PriestControl>())
            {
                player.GetComponent<PriestControl>().invincible = true;
            }
            else if (player.GetComponent<CleaveControl>())
            {
                player.GetComponent<CleaveControl>().invincible = true;
            }
        }
    }
    void Attack()
    {
        GetComponent<AnimReference>().TriggerAttack();

        GameObject temp = Instantiate(bolt, transform.position + new Vector3(0.0f, 0.5f, 0.0f),
                                      transform.rotation) as GameObject;
        temp.GetComponent<HolyBolt>().enemy = "Enemy";
        temp.GetComponent<HolyBolt>().ally = "Player";
        temp.GetComponent<HolyBolt>().spiritID = aura.GetComponent<HolyAuraScript>().spiritID;
        temp.GetComponent<Rigidbody>().AddForce(transform.forward * 8, ForceMode.Impulse);

    }
    public void TakeDamage(float dmg)
    {
        if (dmg <= 0)
        {
            if (GetComponent<EnemyHealth>().Health - dmg <= GetComponent<EnemyHealth>().MaxHealth)
            {
                healed = true;
                GetComponent<EnemyHealth>().ChangeHealth(dmg);
            }
            else
            {
                GetComponent<EnemyHealth>().Health = GetComponent<EnemyHealth>().MaxHealth;
            }
        }
        else
        {
            if (!invincible)
            {
                hurt = true;
                audioRef.PlayOneshot(audioRef.priestDamage);
                GetComponent<EnemyHealth>().ChangeHealth(dmg);
            }
        }


        if (GetComponent<EnemyHealth>().Health <= 0) //if enemy isn't dead and they should be then
        {

            GameObject temp = Instantiate(blood, transform.position, transform.rotation) as GameObject; //make them bleed.
            Destroy(temp, .4f); //this destroys the particle after a time so that it doesn't stay.
            GetComponent<AnimReference>().TriggerDeath();
            if (GetComponent<BasicSeek>())
            {
                Destroy(GetComponent<BasicSeek>());
            }
            else if (GetComponent<BasicWanderSteering>())
            {
                Destroy(GetComponent<BasicWanderSteering>());
            }
            if (GetComponent<UnityEngine.AI.NavMeshAgent>())
            {
                //GetComponent<UnityEngine.AI.NavMeshAgent>().ResetPath();
                Destroy(GetComponent<UnityEngine.AI.NavMeshAgent>());
            }
            Destroy(GetComponent<Rigidbody>());
            Destroy(GetComponent<CapsuleCollider>());
            GetComponent<AnimReference>().TriggerDeath();
            GameObject.Find("Spirit" + GetComponent<PlayerScript>().playerID).GetComponent<SpiritScript>().UnPossess();
            Invoke("Kill", 2f); //this just gives it enough time for the animation to play
        }
    }

    void Kill()
    {
        Destroy(gameObject); //bye bye
    }
}
