﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleaveControl : MonoBehaviour
{
    [Header("Damage")]
    public float Damage = 3;
    public float NormalDamage = 3;
    [Header("AttackSpeed")]
    public float AttackSpeed = 1.0f;
    public float abilityTwoSpeed = 2.0f;
    public float abilityThreeSpeed = 5.0f;
    private float attackTimer = 0;
    private float abilityTwoTimer = 0;
    private float abilityThreeTimer = 0;

    public Color ogCol = Color.white;
    private Color hurtCol = Color.red;
    private float colTime = 0.0f;
    private bool hurt = false;
    private bool unhurt = false;
    private Color yumCol = Color.green;
    private bool healed = false;
    private bool unhealed = false;

    public GameObject blood;
    public GameObject sword;
	public GameObject RightBumper;
    private bool isCharging = false;

    public bool canMove = true;//should be false when spinning and when about to charge.

    public bool invincible = false;
    public bool spinToWin;
    private float fuckTim = 1.233f;
    private bool ulting;
    private float NormalSpeed;
    ToggleUISwaps tog;
    AudioReference audioRef;
    void Start()
    {
        blood = Resources.Load("BloodSplat", typeof(GameObject)) as GameObject;
        NormalSpeed = GetComponent<IsoMovement>().speed;
        if (GameObject.Find("AudioHandler"))
        {
            audioRef = GameObject.Find("AudioHandler").GetComponent<AudioReference>();
        }
        
        if (GameObject.Find("SpiritUI" + GetComponent<PlayerScript>().spiritID))
        {
            tog = GameObject.Find("SpiritUI" + GetComponent<PlayerScript>().spiritID).GetComponent<ToggleUISwaps>();
        }
        
    }
    void Update()
    {
        if (transform.position.y <= -5)
        {
            if (GetComponent<BasicSeek>())
            {
                Destroy(GetComponent<BasicSeek>());
            }
            else if (GetComponent<BasicWanderSteering>())
            {
                Destroy(GetComponent<BasicWanderSteering>());
            }
            if (GetComponent<UnityEngine.AI.NavMeshAgent>())
            {
                Destroy(GetComponent<UnityEngine.AI.NavMeshAgent>());
            }
            Destroy(GetComponent<Rigidbody>());
            Destroy(GetComponent<CapsuleCollider>());
            GameObject.Find("Spirit" + GetComponent<PlayerScript>().playerID).GetComponent<SpiritScript>().UnPossess();
            Invoke("Kill", 2f); //this just gives it enough time for the animation to play
        }
        //Allows for the basic attack when possessing
        attackTimer -= Time.deltaTime;
        abilityTwoTimer -= Time.deltaTime;
        abilityThreeTimer -= Time.deltaTime;
        if (tog)
        {
            tog.cCD1.fillAmount = attackTimer / AttackSpeed;
            tog.cCD2.fillAmount = abilityTwoTimer / abilityTwoSpeed;
            tog.cCD3.fillAmount = abilityThreeTimer / abilityThreeSpeed;
        }

        if (Input.GetAxis("BasicAbilityAlt" + GetComponent<PlayerScript>().playerID) < 0 && !isCharging && !ulting )
        {
            Attack();
        }
        else if(!isCharging && !ulting)
        {
            spinToWin = false;
            TurnOffSwordCollider();
            GetComponent<AnimReference>().SetAttacking(false);
        }
        if (Input.GetButtonDown("SecondaryAbility" + GetComponent<PlayerScript>().playerID) && abilityTwoTimer <= 0 && !ulting && !spinToWin && !isCharging)
        {
            AbilityTwo();
            abilityTwoTimer = abilityTwoSpeed;
        }
        if (Input.GetButtonDown("ThirdAbility" + GetComponent<PlayerScript>().playerID) && abilityThreeTimer <= 0 && !spinToWin && !isCharging)
        {
            AbilityThree();
            abilityThreeTimer = abilityThreeSpeed;
        }

        if (isCharging && !spinToWin)
        {
            GetComponent<IsoMovement>().speed = NormalSpeed * 1.5f;
            Invoke("FinishCharge", fuckTim);
        }
        if (hurt)
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                r.material.color = Color.Lerp(ogCol, hurtCol, colTime);
            }
            colTime += Time.deltaTime * 3.0f;
            if (colTime >= 1.0f)
            {
                colTime = 0.0f;
                unhurt = true;
                hurt = false;
            }
        }
        if (unhurt)
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                r.material.color = Color.Lerp(hurtCol, ogCol, colTime);
            }
            colTime += Time.deltaTime * 3.0f;
            if (colTime >= 1.0f)
            {
                colTime = 0.0f;
                unhurt = false;
            }
        }
        if (healed)
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                r.material.color = Color.Lerp(ogCol, yumCol, colTime);
            }
            colTime += Time.deltaTime * 3.0f;
            if (colTime >= 1.0f)
            {
                colTime = 0.0f;
                unhealed = true;
                healed = false;
            }
        }
        if (unhealed)
        {
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
            {
                r.material.color = Color.Lerp(yumCol, ogCol, colTime);
            }
            colTime += Time.deltaTime * 3.0f;
            if (colTime >= 1.0f)
            {
                colTime = 0.0f;
                unhealed = false;
            }
        }
    }
    void EndUlt()
    {
        ulting = false;
        sword.GetComponent<DamageOnCollision>().damage = 2.0f;
    }
    void AbilityTwo()
    {

        GetComponent<AnimReference>().TriggerChannel();
        
        Invoke("Charge", 1.467f);
    }
    void Charge()
    {
        sword.GetComponent<DamageOnCollision>().ColliderOn = true;
        Invoke("TurnOffSwordCollider", 1.233f);
        isCharging = true;
    }
    void AbilityThree()
    {
        ulting = true;
        GetComponent<AnimReference>().TriggerUltimate();
        sword.GetComponent<DamageOnCollision>().ColliderOn = true;
        Invoke("TurnOffSwordCollider", 1.467f);
        sword.GetComponent<DamageOnCollision>().damage = 500.0f;
        Invoke("EndUlt", 1.467f);
        //super leap
    }
    void Attack()
    {
        if (!isCharging)
        {
            spinToWin = true;
            sword.GetComponent<DamageOnCollision>().ColliderOn = true;
            GetComponent<AnimReference>().SetAttacking(true);
        }
    }
    void turnOffSpin()
    {
        spinToWin = false;
    }
    protected void TurnOffSwordCollider()
    {
        sword.GetComponent<DamageOnCollision>().ColliderOn = false;
    }
    private void FinishCharge()
    {
        isCharging = false;
        GetComponent<IsoMovement>().speed = NormalSpeed;
    }
    public void TakeDamage(float dmg)
    {
        if (dmg <= 0)
        {
            if (GetComponent<EnemyHealth>().Health - dmg <= GetComponent<EnemyHealth>().MaxHealth)
            {
                healed = true;
                GetComponent<EnemyHealth>().ChangeHealth(dmg);
            }
            else
            {
                GetComponent<EnemyHealth>().Health = GetComponent<EnemyHealth>().MaxHealth;
            }
        }
        else
        {
            if (!invincible)
            {
                hurt = true;
                audioRef.PlayOneshot(audioRef.necroGrunt1);
                GetComponent<EnemyHealth>().ChangeHealth(dmg);
            }
        }
        if (GetComponent<EnemyHealth>().Health <= 0) //if enemy isn't dead and they should be then 
        {

            GameObject temp = Instantiate(blood, transform.position, transform.rotation) as GameObject; //make them bleed.
            Destroy(temp, .4f); //this destroys the particle after a time so that it doesn't stay.
            GetComponent<AnimReference>().TriggerDeath();
            if (GetComponent<BasicSeek>())
            {
                Destroy(GetComponent<BasicSeek>());
            }
            else if (GetComponent<BasicWanderSteering>())
            {
                Destroy(GetComponent<BasicWanderSteering>());
            }
            if (GetComponent<UnityEngine.AI.NavMeshAgent>())
            {
                //GetComponent<UnityEngine.AI.NavMeshAgent>().ResetPath();
                Destroy(GetComponent<UnityEngine.AI.NavMeshAgent>());
            }
            Destroy(GetComponent<Rigidbody>());
            Destroy(GetComponent<CapsuleCollider>());
            GameObject.Find("Spirit" + GetComponent<PlayerScript>().playerID).GetComponent<SpiritScript>().UnPossess();
            Invoke("Kill", 2f); //this just gives it enough time for the animation to play
        }
    }

    void Kill()
    {

        Destroy(gameObject); //bye bye
    }
}
