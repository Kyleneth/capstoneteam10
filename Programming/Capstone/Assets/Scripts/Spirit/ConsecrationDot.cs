﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsecrationDot : MonoBehaviour {

    public float dot = 1.0f;
    public float timer = 1.0f;
    public bool timeToDuel;
    public int spiritID = 0;

    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timeToDuel = true;
        }
    }
    void OnTriggerStay(Collider other)
    {
        if (timeToDuel)
        {
            if (other.tag == "Enemy")
            {
                if (other.GetComponent<BaseEnemyScript>())
                {
                    other.GetComponent<BaseEnemyScript>().TakeDamage(dot, spiritID);
                }
            }
            timeToDuel = false;
            timer = 1.0f;
        }
    }
}
