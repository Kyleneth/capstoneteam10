﻿//AUTHOR: Kyle Netherland

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AOEAbility : MonoBehaviour {
    [Header("ColdSnap")] //the following is all for coldsnap
    [Tooltip("Enter slow amounts as a value of 0-1")]
    public float csSlowAmount;
    [Tooltip("Enter slow amounts as a value of 0-1")]
    public float csAsSlowAmount;
    [Tooltip("Duration in seconds\nFor normal ColdSnap")]
    public float csDuration;
    [Tooltip("For normal ColdSnap")]
    public float csRadius;
    [Tooltip("For normal ColdSnap")]
    public int csDamage;
    private float timer = 0.0f;
    private float gCD = 0.5f;
    [Header("ColdSnapObjects")]
    public GameObject nSnap;

    public bool knowsColdSnap = false;

    private Animator animController;

    ToggleUISwaps tog;
    // Use this for initialization
    void Start () {
        setAnim();
        if (GameObject.Find("SpiritUI" + GetComponent<PlayerScript>().spiritID))
        {
            tog = GameObject.Find("SpiritUI" + GetComponent<PlayerScript>().spiritID).GetComponent<ToggleUISwaps>();
        }
    }
    public void setAnim()
    {
        animController = GetComponent<SpiritScript>().anim;
    }
    // Update is called once per frame
    void Update () {
        #region Cold Snap Script

        
        timer -= Time.deltaTime;
        if (tog)
        {
            tog.AB2.fillAmount = timer / gCD;
        }
        if (timer <= 0.0f)
        {
            if (Input.GetButtonDown("BasicAbility" + gameObject.GetComponent<PlayerScript>().playerID) && !GetComponent<SpiritScript>().isPossessing)//cold snap
            {
                animController.SetTrigger("Attacking");

                GameObject temp = Instantiate(nSnap, transform.position, Quaternion.identity) as GameObject;
                temp.GetComponent<ColdSnap>().SetValues(csSlowAmount, csDuration, csDamage);
                temp.GetComponent<SphereCollider>().radius = csRadius;
                ParticleSystem sys = temp.GetComponent<ParticleSystem>();
                ParticleSystem.ShapeModule shMod = sys.shape;
                shMod.radius = csRadius;

                timer = gCD;
            }
        }
        #endregion
    }
}
