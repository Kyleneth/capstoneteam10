﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class EndPossess : StateMachineBehaviour {
    
    //When the animation is over, find all spirits possessing and disable their sprite renderer.
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        for (int i = 0; i < 5; i++)
        {
            GameObject temp = GameObject.Find("Spirit" + i);
            if (temp)
            {
                if (temp.GetComponent<SpiritScript>().isPossessing)
                {
                    GameObject temp2 = GameObject.Find("Spirit" + i);
                    temp2.GetComponent<SpiritScript>().mesh.enabled = false;
                }
            }
        }
	}
}
