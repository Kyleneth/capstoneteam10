﻿//AUTHOR: Kyle Netherland

using UnityEngine;
using System.Collections;

public class ColdSnap : MonoBehaviour {
    private float slowAmount, duration, damage; 
    float i = 1; //timer
    public GameObject coldStamp;//decal effect
    public int spiritID;
	// sets values correctly
	public void SetValues (float slow,float dur,float dmg) {
        slowAmount = slow;
        duration = dur;
        damage = dmg;

        Instantiate(coldStamp, transform.position - new Vector3(0, 0.4f, 0), Quaternion.Euler(90, 0, 0));
    }
	
	//dies after certain time
	void Update () {
        i -= Time.deltaTime;
        if (i < 0)
        {
            Destroy(gameObject);
        }
	}
    void OnTriggerEnter(Collider other) //if it hits an enemy, slow them and deal damage to them.
    {
        if (other.tag == "Enemy")
        {
            other.gameObject.GetComponent<EnemySpeed>().Slow(slowAmount,duration);
            other.gameObject.GetComponent<BaseEnemyScript>().TakeDamage(damage, spiritID);
        }
    }
}
