﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerateButton : MonoBehaviour {

    private DungeonGeneration generation;
    private Button button;
	
    // Use this for initialization
	void Start () {
        generation = GameObject.Find("DungeonGenerator").GetComponent<DungeonGeneration>();
        button = transform.GetComponent<Button>();

        button.onClick.AddListener(() => {
            generation.Generate();
        });
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
