﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RoomCache
{
    [Tooltip("This list of ints are the number of each type of difficulty you want - (e.g 0, 1, 1, 2 would spawn one level 0, two level 1s, one level 2.)")]
    public List<int> LevelMakeup;

    [Space(5)] [Tooltip("Pool of difficulty 0 rooms.")]
    public List<GameObject> Difficulty0 = new List<GameObject>();

    [Space(5)] [Tooltip("Pool of difficulty 1 rooms.")]
    public List<GameObject> Difficulty1 = new List<GameObject>();

    [Space(5)] [Tooltip("Pool of difficulty 2 rooms.")]
    public List<GameObject> Difficulty2 = new List<GameObject>();

    [Space(5)] [Tooltip("Pool of difficulty 3 rooms.")]
    public List<GameObject> Difficulty3 = new List<GameObject>();
}

public class DungeonGeneration : MonoBehaviour {

    [Tooltip("Collection of varius level data for designer customization!")]
    public RoomCache RoomCache;

    [Space(15)]

    [SerializeField] [Tooltip("Unless you have a good reason to, it's better to not touch this. Affects how far apart the rooms spawn before separating.")]
    private float radius = 200.0f;
    [Tooltip("Unless you have a good reason to, it's better to not touch this. Affects how fast the rooms decelerate when separating.")]
    public float decay;
    [Tooltip("Unless you have a good reason to, it's better to not touch this. Affects how fast the rooms accelerate when separating")]
    public float maxAccel;

    [HideInInspector]
    public List<GameObject> Rooms = new List<GameObject>();

    [HideInInspector]
    public int completionCount = 0;

    private GameObject dungeonRoot;

    void Update() {
        if (completionCount == Rooms.Count)
        {
            FinalizeRoomPositions();
        }
    }

    // The trigger for generation; called by the generate button
    public void Generate()
    {
        Clear();

        // Instantiate the DungeonRoot object so that the level can be saved out to a prefab
        dungeonRoot = new GameObject("DungeonRoot");

        StartCoroutine(RoomPlacementRoutine(OnComplete => { }));
    }

    public void Clear()
    {
        foreach (var go in Rooms)
        {
            Destroy(go);
        }

        Destroy(dungeonRoot);
        Rooms.Clear();
        completionCount = 0;
    }

    // TODO: I need to implement some sort of way of maintaining a weight of difficulty;
    // rather, the designers need to be able to throw rooms into a pool, and it needs to 
    // pick from that pool with a bias
    // For now, just using a random index.
    #region PLACEMENT
    private IEnumerator RoomPlacementRoutine(System.Action<bool> OnComplete)
    {
        // Grab the rooms we'll be spawning from the pool, based on the makeup
        // There is probably a more clever way of doing this - I will figure it out later.
        // for now, let there be if statements!
        for (int i = 0; i < RoomCache.LevelMakeup.Count; i++)
        {
            GameObject go = null;
            if (RoomCache.LevelMakeup[i] == 0) go = RoomCache.Difficulty0.PickRandom();
            if (RoomCache.LevelMakeup[i] == 1) go = RoomCache.Difficulty1.PickRandom();
            if (RoomCache.LevelMakeup[i] == 2) go = RoomCache.Difficulty2.PickRandom();
            if (RoomCache.LevelMakeup[i] == 3) go = RoomCache.Difficulty3.PickRandom();

            Vector3 pos = GetRandomPosInCircle();

            go = Instantiate(go);
            go.transform.localPosition = pos;
            int rand = Random.Range(1, 3);
           
            if(rand==1)
            {
                Quaternion rot = Quaternion.Euler(new Vector3(0,90,0));
                go.transform.localRotation = rot;

            }
            if (rand == 2)
            {
                Quaternion rot = Quaternion.Euler(new Vector3(0, 0, 0));
                go.transform.localRotation = rot;

            }
            go.transform.parent = dungeonRoot.transform;

            Rooms.Add(go);

            yield return null;
        }

        EventHandler.TriggerEvent("ProcEVT_PlacementFinished");
    }
    #endregion

    #region HALLWAYS
    private void FinalizeRoomPositions()
    {
        // Snap all of the rooms to an integer position
        for (int i = 0; i < Rooms.Count; ++i)
        {
            Vector3 pos1 = Rooms[i].transform.localPosition;
            pos1.x = (int)pos1.x;
            pos1.z = (int)pos1.z;
            Rooms[i].transform.localPosition = new Vector3((int)pos1.x, (int)0, (int)pos1.z);
        }
    }
    #endregion

    #region UTILS
    private Vector3 GetRandomPosInCircle()
    {
        float angle = Random.Range(0f, 1f) * Mathf.PI * 2f;

        float rad = Mathf.Sqrt(Random.Range(0f, 1f) * radius);
        float x = this.transform.localPosition.x + rad * Mathf.Cos(angle);
        float y = this.transform.localPosition.y + rad * Mathf.Sin(angle);

        return new Vector3(x, 0, y);
    }
    #endregion
}
