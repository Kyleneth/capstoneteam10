﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (RoomSeparation))]
public class Room : MonoBehaviour {

    private RoomSeparation separation;
    private DungeonGeneration generator;

	private bool separating = true;
    private bool complete = false;

	// Use this for initialization
	void Start () {
        separation = GetComponent<RoomSeparation>();
        generator = GameObject.Find("DungeonGenerator").GetComponent<DungeonGeneration>();

        separation.decay = generator.decay;
        separation.maxAccel = generator.maxAccel;
    }
	
	// Update is called once per frame
	void Update () {
        if (separating && !complete)
        {
            Vector3 velocity = separation.Linear();
            transform.position += velocity * Time.deltaTime;

            if (velocity.magnitude == 0)
            {
                complete = true;
                separating = false;
                generator.completionCount++;
            }
        }
    }

    void BeginSeparation()
    {
        separating = true;
    }

    void Awake()
    {
        EventHandler.StartListening("ProcEVT_PlacementFinished", BeginSeparation);
    }

    void OnDestroy()
    {
        EventHandler.StopListening("ProcEVT_PlacementFinished", BeginSeparation);
    }
}
