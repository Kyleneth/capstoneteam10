﻿using System;
using UnityEngine;

// I don't know if I want to base this on a per-room basis yet or not.
public class RoomSeparation : MonoBehaviour
{
    public float separationThreshold = 25.0f;

    [HideInInspector]
    public float decay;
    [HideInInspector]
    public float maxAccel;

    private DungeonGeneration generator;

    void Start()
    {
        generator = GameObject.Find("DungeonGenerator").GetComponent<DungeonGeneration>();
    }

    public Vector3 Linear()
    {
        Vector3 linear = Vector3.zero;

        foreach (GameObject go in generator.Rooms)
        {
            Vector3 direction = transform.position - go.transform.position;
            float distSquared = direction.sqrMagnitude;

            if (distSquared < (separationThreshold * separationThreshold))
            {
                float strength = Mathf.Min(decay / distSquared, maxAccel);

                direction.Normalize();
                linear += strength * direction;
            }
        }

        return linear;
    }
}
