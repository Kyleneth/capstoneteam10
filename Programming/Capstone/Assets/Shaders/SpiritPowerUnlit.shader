﻿Shader "Unlit/SpiritPowerUnlit"
{
	Properties
	{
		_Color("Color", Color) = (0.5, 0.5, 1.0, 1.0)
		_Frequency("Frequency", Float) = 5.0
		_Amplitude("Amplitude", Float) = 0.1
		_Speed("Speed", Float) = 1.0
	}
	SubShader
	{
		//ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		Tags {"Queue" = "Transparent" "RenderType"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
			};

			fixed4 _Color;
			float _Frequency;
			float _Amplitude;
			float _Speed;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.vertex.z += sin(_Time.y * _Speed + o.vertex.x * _Frequency) * _Amplitude;
				o.vertex.y += sin(_Time.y * _Speed + o.vertex.z * _Frequency) * _Amplitude;
				o.vertex.x += sin(_Time.y * _Speed + o.vertex.y * _Frequency) * _Amplitude;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = _Color;
				return col;
			}
			ENDCG
		}
	}
}
