﻿Shader "Vertex Modifier" {
	Properties{
		_Color("Color", Color) = (0.5, 0.5, 1.0, 1.0)
		_Frequency("Frequency", Float) = 5.0
		_Amplitude("Amplitude", Float) = 0.1
		_Speed("Speed", Float) = 1.0
	}
	SubShader{
		Tags{ "RenderType" = "Opaque" }
		CGPROGRAM
	#pragma surface surf Lambert vertex:vert
	struct Input {
		float2 uv_MainTex;
	};

	// Access the shaderlab properties
	fixed4 _Color;
	float _Frequency;
	float _Amplitude;
	float _Speed;

	// Vertex modifier function
	void vert(inout appdata_full v) {
		v.vertex.z += sin(_Time.y * _Speed + v.vertex.x * _Frequency) * _Amplitude;
		v.vertex.y += sin(_Time.y * _Speed + v.vertex.z * _Frequency) * _Amplitude;
	}

	// Surface shader function
	void surf(Input IN, inout SurfaceOutput o) {
		//o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
		o.Albedo = _Color;
	}
	ENDCG
	}
}