﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeToClear : MonoBehaviour {
    float t = 0.0f;
    bool fade = false;
    void Start()
    {
        fade = true;
    }
	// Update is called once per frame
	void Update () {
        if (fade)
        {
            if (GetComponent<Image>())
            {
                GetComponent<Image>().color = new Color(GetComponent<Image>().color.r,
                                                                        GetComponent<Image>().color.g,
                                                                        GetComponent<Image>().color.b,
                                                                        Mathf.Lerp(1.0f, 0.0f, t));
            }
            else if (GetComponent<Text>())
            {
                GetComponent<Text>().color = new Color(GetComponent<Text>().color.r,
                                                                        GetComponent<Text>().color.g,
                                                                        GetComponent<Text>().color.b,
                                                                        Mathf.Lerp(1.0f, 0.0f, t));
            }

            t += Time.deltaTime * 0.7f;
            if (t > 1.0f)
            {
                Destroy(gameObject);
            }
        }
    }
}
