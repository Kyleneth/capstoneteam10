﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMusic : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (GameObject.Find("AudioHandler"))
        {
            GameObject.Find("AudioHandler").GetComponent<AdjustVolume>().OnSceneLoad();
        }
        
    }
	
}
