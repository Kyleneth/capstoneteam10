﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialFollow : MonoBehaviour {

    private int TutorialIndex;

    bool[] pMoved = {false,false,false,false };
    bool[] pLooked = { false, false, false, false };
    bool[] pPossessed = { false, false, false, false };

    bool[] ptAbilityOne = { false, false, false, false };
    bool[] ptAbilityTwo = { false, false, false, false };
    bool[] ptAbilityThree = { false, false, false, false };
    public SmoothTextWrite[] tutorialMessages;
    GameObject[] players;
    GameObject[] spirits;
    int numPlayers, numSpirits;
    GameObject[] people;
	public GameObject Wall1;
	public GameObject Wall2;
	public GameObject Wall3;

    private void Start()
    {
        TutorialIndex = 0;
        players = GameObject.FindGameObjectsWithTag("Player");
        numPlayers = players.Length;
        spirits = GameObject.FindGameObjectsWithTag("Spirit");
        numPlayers += spirits.Length;
        numSpirits = spirits.Length;
        people = new GameObject[numPlayers];
        people[0] = players[0];
        for (int i = 0; i < numSpirits; i++)
        {
            people[i + 1] = spirits[i];
        }
    }
    // Update is called once per frame
    void Update () {

    
        switch (TutorialIndex)
        {
            case 0:
                {
                    if (numPlayers > 0)
                    {
                        for (int i = 0; i < numPlayers; i++)
                        {
                            float x = Input.GetAxis("Horizontal" + people[i].GetComponent<PlayerScript>().playerID);
                            float y = 0;
                            float z = Input.GetAxis("Vertical" + people[i].GetComponent<PlayerScript>().playerID);
                            if (new Vector3(x, y, z).magnitude > 0)
                            {
                                pMoved[i] = true;
                            }
                        }
                        int donePlayers = 0;
                        for (int i = 0; i < numPlayers; i++)
                        {
                            if (pMoved[i] == true)
                            {
                                donePlayers++;
                            }
                        }
                        if (donePlayers == numPlayers)
                        {
                            //switch canvas here, 
                            tutorialMessages[TutorialIndex].TutorialFinished();
                            TutorialIndex++;
                        }
                    }
                    else
                    {
                        players = GameObject.FindGameObjectsWithTag("Player");
                        numPlayers = players.Length;
                        spirits = GameObject.FindGameObjectsWithTag("Spirit");
                        numPlayers += spirits.Length;
                        numSpirits = spirits.Length;
                        people = new GameObject[numPlayers];
                        people[0] = players[0];
                        for (int i = 0; i < numSpirits; i++)
                        {
                            people[i + 1] = spirits[i];
                        }
                    }
                }
                break;
            case 1:
                {
                    for (int i = 0; i < numPlayers; i++)
                    {
                        float x = Input.GetAxis("HorizontalLook" + people[i].GetComponent<PlayerScript>().playerID);
                        float y = 0;
                        float z = Input.GetAxis("VerticalLook" + people[i].GetComponent<PlayerScript>().playerID);
                        if (new Vector3(x, y, z).magnitude > 0)
                        {
                            pLooked[i] = true;
                        }
                    }
                    int donePlayers = 0;
                    for (int i = 0; i < numPlayers; i++)
                    {
                        if (pLooked[i] == true)
                        {
                            donePlayers++;
                        }
                    }
                    if (donePlayers == numPlayers)
                    {
                        tutorialMessages[TutorialIndex].TutorialFinished();
                        TutorialIndex++;
                    }
                }
                break;
            case 2:
                {
                    if (Input.GetAxis("BasicAbilityAlt" + players[0].GetComponent<PlayerScript>().playerID) < 0)
                    {
                        tutorialMessages[TutorialIndex].TutorialFinished();
                        TutorialIndex++;
                    }
                }
                break;
            case 3:
                {
					Destroy (Wall3);
                    if (Input.GetButtonDown("Submit" + players[0].GetComponent<PlayerScript>().playerID))
                    {
						Destroy (Wall2);
                        tutorialMessages[TutorialIndex].TutorialFinished();
                        TutorialIndex++;
                    }
                }
                break;
            case 4:
                {
                    if (Input.GetButtonDown("Submit" + players[0].GetComponent<PlayerScript>().playerID))
                    {
                        tutorialMessages[TutorialIndex].TutorialFinished();
                        TutorialIndex++;
                    }
                }
                break;
            case 5:
                {
                    
                    if (Input.GetAxis("RBumper" + players[0].GetComponent<PlayerScript>().playerID) > 0)
                    {
                        tutorialMessages[TutorialIndex].TutorialFinished();
                        TutorialIndex++;
                    }
                }
                break;
            case 6:
                {
                   
                    if (Input.GetAxis("LBumper" + players[0].GetComponent<PlayerScript>().playerID) > 0)
                    {
                        tutorialMessages[TutorialIndex].TutorialFinished();
                        TutorialIndex++;
                    }
                }
                break;
            case 7:
                {
					Destroy (Wall2);
                    for (int i = 0; i < numSpirits; i++)
                    {
                        if (spirits[i].GetComponent<SpiritScript>().isPossessing)
                        {
                            pPossessed[i] = true;
                        }
                    }
                    int donePlayers = 0;
                    for (int i = 0; i < numSpirits; i++)
                    {
                        if (pPossessed[i] == true)
                        {
                            donePlayers++;
                        }
                    }
                    if (donePlayers == numSpirits)
                    {
                        tutorialMessages[TutorialIndex].TutorialFinished();
                        TutorialIndex++;
                    }
                }
                break;
            case 8:
                {
                    for (int i = 0; i < numSpirits; i++)
                    {
                        if (Input.GetAxis("BasicAbilityAlt" + spirits[i].GetComponent<PlayerScript>().playerID) < 0)
                        {
                            ptAbilityOne[i] = true;
                        }
                    }
                    int donePlayers = 0;
                    for (int i = 0; i < numSpirits; i++)
                    {
                        if (ptAbilityOne[i] == true)
                        {
                            donePlayers++;
                        }
                    }
                    if (donePlayers == numSpirits)
                    {
                        tutorialMessages[TutorialIndex].TutorialFinished();
                        TutorialIndex++;
                    }
                }
                break;
            case 9:
                {
                    for (int i = 0; i < numSpirits; i++)
                    {
                        if (Input.GetButtonDown("SecondaryAbility" + spirits[i].GetComponent<PlayerScript>().playerID))
                        {
                            ptAbilityTwo[i] = true;
                        }
                    }
                    int donePlayers = 0;
                    for (int i = 0; i < numSpirits; i++)
                    {
                        if (ptAbilityTwo[i] == true)
                        {
                            donePlayers++;
                        }
                    }
                    if (donePlayers == numSpirits)
                    {
                        tutorialMessages[TutorialIndex].TutorialFinished();
                        TutorialIndex++;
                    }
                }
                break;
            case 10:
                {
                    for (int i = 0; i < numSpirits; i++)
                    {
                        if (Input.GetButtonDown("ThirdAbility" + spirits[i].GetComponent<PlayerScript>().playerID))
                        {
                            ptAbilityThree[i] = true;
                        }
                    }
                    int donePlayers = 0;
                    for (int i = 0; i < numSpirits; i++)
                    {
                        if (ptAbilityThree[i] == true)
                        {
                            donePlayers++;
                        }
                    }
                    if (donePlayers == numSpirits)
                    {
                        tutorialMessages[TutorialIndex].TutorialFinished();
                        TutorialIndex++;
                    }
                }
                break;
            case 11:
                {
                    for (int i = 0; i < numSpirits; i++)
                    {
                        if (!spirits[i].GetComponent<SpiritScript>().isPossessing)
                        {
                            pPossessed[i] = false;
                        }
                    }
                    int donePlayers = 0;
                    for (int i = 0; i < numSpirits; i++)
                    {
                        if (pPossessed[i] == false)
                        {
                            donePlayers++;
                        }
                    }
                    if (donePlayers == numSpirits)
                    {
                        tutorialMessages[TutorialIndex].TutorialFinished();
                        TutorialIndex++;
                    }
                }
                break;
		case 12:
			{
				if (GameObject.FindGameObjectsWithTag ("Enemy").Length > 0 && GameObject.FindGameObjectsWithTag("Player").Length ==1) {
					Destroy (Wall1);
				}
			}
			break;
            default:
                break;
        }
    }


}
