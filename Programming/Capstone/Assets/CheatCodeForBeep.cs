﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatCodeForBeep : MonoBehaviour {
    //GameObject camera;
    public bool beepCode = false;
    float timer = 2.5f;
    int codeLevel = 0;
	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(gameObject);
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 0)
        {
            beepCode = false;
            codeLevel = 0;
            timer = 2.5f;
        }
        GameObject temp = GameObject.Find("BeepBeep");
        if (temp != gameObject)
        {
            Destroy(temp);
        }
        //camera = GameObject.Find("MainCamera");
	}
	
	// Update is called once per frame
	void Update () {
        if (!beepCode)
        {
            switch (codeLevel)
            {
                case 0:
                    {
                        for (int i = 1; i < 5; i++)
                        {
                            if (Input.GetButtonDown("SecondaryAbility" + i))
                            {
                                codeLevel++;
                            }
                        }

                    }
                    break;
                case 1:
                    {
                        for (int i = 1; i < 5; i++)
                        {
                            if (Input.GetButtonDown("Submit" + i))
                            {
                                timer = 2.5f;
                                codeLevel++;
                            }
                        }
                        timer -= Time.deltaTime;
                        if (timer <= 0)
                        {
                            timer = 2.5f;
                            codeLevel = 0;
                        }
                    }
                    break;
                case 2:
                    {
                        for (int i = 1; i < 5; i++)
                        {
                            if (Input.GetButtonDown("Submit" + i))
                            {
                                timer = 2.5f;
                                codeLevel++;
                            }
                        }
                        timer -= Time.deltaTime;
                        if (timer <= 0)
                        {
                            timer = 2.5f;
                            codeLevel = 0;
                        }
                    }
                    break;
                case 3:
                    {
                        for (int i = 1; i < 5; i++)
                        {
                            if (Input.GetButtonDown("Submit" + i))
                            {
                                timer = 2.5f;
                                codeLevel++;
                            }
                        }
                        timer -= Time.deltaTime;
                        if (timer <= 0)
                        {
                            timer = 2.5f;
                            codeLevel = 0;
                        }
                    }
                    break;
                case 4:
                    {
                        beepCode = true;
                    }
                    break;
                default:
                    break;
            }
        }
        if (beepCode)
        {
            //change camera here
        }
    }
}
