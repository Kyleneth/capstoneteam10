﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GoToNextLevel : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (SceneManager.GetActiveScene().buildIndex == 5)
        {
            if (other.tag == "Player")
            {
                GameObject.Find("PlayerStats").GetComponent<Death>().deathScreen.GetComponent<Text>().text = "You've escaped the Crypt!";
                GameObject.Find("PlayerStats").GetComponent<Death>().Died();
            }
        }
        else
        {
            if (other.tag == "Player")
            {
                Time.timeScale = 1;
                SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }
        
    }
}
