﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ToggleUISwaps : MonoBehaviour {

    public GameObject SpiritBase, TempBase, PriestBase, CleaverBase;
    public Slider TempHealth, PriestHealth, CleaverHealth;
    public Image tCD1, tCD2, tCD3, pCD1, pCD2, pCD3, cCD1, cCD2, cCD3;
    public Image AB1, AB2, AB3;
}
