﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioReference : MonoBehaviour {
    private AudioSource sfx;
    public AudioClip almostDead;
    public AudioClip priestDamage;
    public AudioClip cantUse;
    public AudioClip upgradePoint;
    public AudioClip upgradePoint2;
    public AudioClip necroBB;
    public AudioClip necroBB2;
    public AudioClip necroBP1;
    public AudioClip necroBP2;
    public AudioClip necroBP3;
    public AudioClip necroGrunt1;
    public AudioClip necroGrunt2;
    public AudioClip necroGrunt3;
    public AudioClip necroGrunt4;
    public AudioClip necroGrunt5;
    public AudioClip necroPN;
    public AudioClip pickUpTime;
    public AudioClip pickUpTime2;
    public AudioClip cleaverWW;
    public AudioClip cleaverWW2;
    public AudioClip cleaverWW3;
    public AudioClip spiritPoss;
    public AudioClip spiritUnposs;
    public AudioClip priestHB;
    public AudioClip priestHB2;
    public AudioClip priestHB3;
    public AudioClip templarBasic;
    public AudioClip templarBasic1;
    public AudioClip templarConsecration;
    public AudioClip templarShields;
    public AudioClip templarShieldDeplete;
    public AudioClip blowUp;

    public float sfxVolume = 0.15f;
    void Start()
    {
        sfx = GetComponents<AudioSource>()[0];    
    }
    public void SetSFXVolume()
    {
        //set value of volume to the value of the slider as its changed.
        float val = GameObject.Find("SoundFXSlider").GetComponent<Slider>().value;
        sfxVolume = val;
        sfx.volume = sfxVolume;
    }
    public void OnLoad()
    {
        //on load, get the current volume and set the sliders value to it.
        sfxVolume = sfx.volume;
        GameObject.Find("SoundFXSlider").GetComponent<Slider>().value = sfxVolume;
    }
    public void PlayOneshot(AudioClip fx)
    {
        sfx.PlayOneShot(fx, sfxVolume);
    }
    
}
