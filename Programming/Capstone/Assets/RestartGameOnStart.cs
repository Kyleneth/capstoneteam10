﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartGameOnStart : MonoBehaviour {
    public bool dead;
    private PlayerHandler pHandler;
    void Start()
    {
        pHandler = GameObject.Find("PlayerHandler").GetComponent<PlayerHandler>();
    }
	// Update is called once per frame
	void Update () {
        if (dead)
        {
            for (int i = 0; i < pHandler.GetNumPlayers(); i++)
            {
                if (Input.GetButtonDown("Pause" + (i + 1)))
                {
                    Time.timeScale = 1.0f;
                    SceneManager.LoadSceneAsync(0);
                }
            }
        }
    }
}
