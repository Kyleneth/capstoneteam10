﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Death : MonoBehaviour {

    public GameObject deathScreen;
    public GameObject background;
    private bool dead;
    float timer = 0.0f;
    private bool timeToMoveOn;
    public void Died()
    {
        GameObject.Find("PauseHandler").GetComponent<PauseGame>().gameOn = false;
        deathScreen.SetActive(true);
        //deathScreen.GetComponent<RestartGameOnStart>().dead = true;
        background.SetActive(true);
        dead = true;
    }

    void Update()
    {
        if (dead)
        {
            deathScreen.GetComponent<Text>().color = new Color(deathScreen.GetComponent<Text>().color.r,
                                                                    deathScreen.GetComponent<Text>().color.g,
                                                                    deathScreen.GetComponent<Text>().color.b,
                                                                    Mathf.Lerp(0.0f, 1.0f, timer));

            background.GetComponent<Image>().color = new Color(background.GetComponent<Image>().color.r,
                                                                    background.GetComponent<Image>().color.g,
                                                                    background.GetComponent<Image>().color.b, 
                                                                    Mathf.Lerp(0.0f, 1.0f, timer));
            timer += 0.02f;
            if (timer > 1.0f)
            {
                timeToMoveOn = true;
                dead = false;
            }
        }
        if (timeToMoveOn)
        {
            deathScreen.GetComponent<Text>().color = new Color(deathScreen.GetComponent<Text>().color.r,
                                                                    deathScreen.GetComponent<Text>().color.g,
                                                                    deathScreen.GetComponent<Text>().color.b,
                                                                    Mathf.Lerp(1.0f, 0.0f, 1.0f-timer));
            timer -= 0.02f;
            if (timer <= 0.0f)
            {
                Time.timeScale = 1.0f;
                SceneManager.LoadSceneAsync(6);
            }
        }
    }
}
